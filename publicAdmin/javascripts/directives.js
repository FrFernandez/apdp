'use strict'; 

/* Directives */


app.directive('appVersion', function() {
    return function(scope, elm, attrs) {
      elm.text("hola");
    };
  });
  
app.directive('onFinishRender', function ($timeout) {
    return {
        restrict: 'A',
        link: function (scope, element, attr) {
            if (scope.$last === true) {
                $timeout(function () {
                    scope.$emit(attr.onFinishRender);
                });
            }
        }
    }
}); 
  
app.directive('stringToNumber', function() {
  return {
    require: 'ngModel',
    link: function(scope, element, attrs, ngModel) {
      ngModel.$parsers.push(function(value) {
        return '' + value;
      });
      ngModel.$formatters.push(function(value) {
        return parseFloat(value);
      });
    }
  };
});

app.directive('numbersOnly', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {
            function fromUser(text) {
                if (text) {
                    var transformedInput = text.replace(/[^0-9]/g, '');

                    if (transformedInput !== text) {
                        ngModelCtrl.$setViewValue(transformedInput);
                        ngModelCtrl.$render();
                    }
                    return transformedInput;
                }
                return undefined;
            }            
            ngModelCtrl.$parsers.push(fromUser);
        }
    };
});

app.directive('localityCreator', ['Locality', function(Locality){
 return {
  restrict: 'E',
  templateUrl: 'views/directiveLocalityCreator',
  scope: {
   model: "=",
   str : "=?",
   change : "&?"
  },
  link: function(scope, element, attr){
   
   function loop(i){
    i = parseInt(i);
    scope.Locality.getChilds(scope.model[i], function(success, childs){
     if(success){
      scope.con[i+1] = childs;
      
      for(var key in scope.con[i])
       if(scope.con[i][key]._id == scope.model[i]){
        scope.str[i] = scope.con[i][key].name;
        break;
       }
      
     }
    });
   };
   
   
   function build(){
    scope.Locality = angular.copy(Locality);
    if(scope.Locality.items.length == 0)
     scope.Locality.s.parent = "false";
   
    scope.con = [];
    scope.str = [];
   
    scope.Locality.get(function(){
     scope.con[0] = scope.Locality.items;
     if(Array.isArray(scope.model)){
      for(var i = 0; i < scope.model.length; i++)
       loop(i);
     }else
      scope.model = [];
    });
    
    
    
    scope.childOf = function(index){
     index = parseInt(index);
     if(scope.change) scope.change();
     
     scope.Locality.getChilds(scope.model[index], function(success, items){
      
      for(var key in scope.con[index])
       if(scope.con[index][key]._id == scope.model[index]){
        scope.str[index] = scope.con[index][key].name;
        break;
       }
      
      if(success)

       scope.con[index+1] = angular.copy(items);
      
      for(var i = 3; i > index+1; i--){
       delete scope.con[i];
       delete scope.str[i];
      }
     });
    };
   
    scope.$watch('model', function(nv,ov){
     if(Array.isArray(scope.model)){
      for(var i = 0; i < scope.model.length; i++){
       loop(i);
      }
     }
    });
   }
   
   
   scope.$on('newLoc', function(){
    build();
   });
   build();
  }
 }
}]);

/*
app.directive('localityCreator', ['Locality', function(Locality){ 
	return {
		restrict: 'E',
		templateUrl: 'views/directiveLocalityCreator',
		scope: {
			model: "=",
			str : "=?",
			change : "&?"
		},
		link: function(scope, element, attr){
			
			function loop(i){
				i = parseInt(i);
				scope.Locality.getChilds(scope.model[i], function(success, childs){
					if(success){
						scope.con[i+1] = childs;
						
						for(var key in scope.con[i])
							if(scope.con[i][key]._id == scope.model[i]){
								scope.str[i] = scope.con[i][key].name;
								break;
							}
						
					}
				});
			};
			
			
			
			scope.Locality = angular.copy(Locality);
			if(scope.Locality.items.length == 0)
				scope.Locality.s.parent = "false";
			
			scope.con = [];
			scope.str = [];
		
			scope.Locality.get(function(){
				scope.con[0] = scope.Locality.items;
				if(Array.isArray(scope.model)){
					for(var i = 0; i < scope.model.length; i++)
						loop(i);
				}else
					scope.model = [];
			});
			
			
			
			scope.childOf = function(index){
				index = parseInt(index);
				if(scope.change) scope.change();
				
				scope.Locality.getChilds(scope.model[index], function(success, items){
					
					for(var key in scope.con[index])
						if(scope.con[index][key]._id == scope.model[index]){
							scope.str[index] = scope.con[index][key].name;
							break;
						}
					
					if(success)
						scope.con[index+1] = angular.copy(items);
					
					for(var i = 2; i > index+1; i--){
						delete scope.con[i];
						delete scope.str[i];
					}
				});
			};
		
			scope.$watch('model', function(nv,ov){
				if(Array.isArray(scope.model)){
					for(var i = 0; i < scope.model.length; i++){
						loop(i);
					}
				}
			});
		}
	}
}]);
*/

app.directive('localityPicker', ['Locality', function(Locality){
 return {
  restrict: 'E',
  templateUrl: 'views/directiveLocalityPicker',
  scope: {
   model: "=",
   str : "=?",
   change : "&?"
  },
  link: function(scope, element, attr){
   
   function loop(i){
    i = parseInt(i);
    scope.Locality.getChilds(scope.model[i], function(success, childs){
     if(success){
      scope.con[i+1] = childs;
      
      for(var key in scope.con[i])
       if(scope.con[i][key]._id == scope.model[i]){
        scope.str[i] = scope.con[i][key].name;
        break;
       }
      
     }
    });
   };
      
   scope.Locality = angular.copy(Locality);
   if(scope.Locality.items.length == 0)
    scope.Locality.s.parent = "false";
   
   scope.con = [];
   scope.str = [];
  
   scope.Locality.get(function(){
    scope.con[0] = scope.Locality.items;
    if(Array.isArray(scope.model)){
     for(var i = 0; i < scope.model.length; i++)
      loop(i);
    }else
     scope.model = [];
   });
     
   scope.childOf = function(index){
    index = parseInt(index);
    if(scope.change) scope.change();
    
    scope.Locality.getChilds(scope.model[index], function(success, items){
     
	 scope.con.splice(index+1);
	 scope.str.splice(index+1);
	 scope.model.splice(index+1);
     for(var key in scope.con[index])
      if(scope.con[index][key]._id == scope.model[index]){
       scope.str[index] = scope.con[index][key].name;
       break;
      }
     
     if(success)
      scope.con[index+1] = angular.copy(items);
     
    });
   };
  
   scope.$watch('model', function(nv,ov){
    if(Array.isArray(scope.model)){
     for(var i = 0; i < scope.model.length; i++){
      loop(i);
     }
    }
   });
  }
 }
}]);

/*
app.directive('localityPicker', ['Locality', function(Locality){
 return {
  restrict: 'E',
  templateUrl: 'views/directiveLocalityPicker',
  scope: {
   model: "=",
   str : "=?",
   change : "&?"
  },
  link: function(scope, element, attr){
   
   function loop(i){
    i = parseInt(i);
    scope.Locality.getChilds(scope.model[i], function(success, childs){
     if(success){
      scope.con[i+1] = childs;
      
      for(var key in scope.con[i])
       if(scope.con[i][key]._id == scope.model[i]){
        scope.str[i] = scope.con[i][key].name;
        break;
       }
      
     }
    });
   };
      
   scope.Locality = angular.copy(Locality);
   if(scope.Locality.items.length == 0)
    scope.Locality.s.parent = "false";
   
   scope.con = [];
   scope.str = [];
  
   scope.Locality.get(function(){
    scope.con[0] = scope.Locality.items;
    if(Array.isArray(scope.model)){
     for(var i = 0; i < scope.model.length; i++)
      loop(i);
    }else
     scope.model = [];
   });
     
   scope.childOf = function(index){
    index = parseInt(index);
    if(scope.change) scope.change();
    
    scope.Locality.getChilds(scope.model[index], function(success, items){
     
     for(var key in scope.con[index])
      if(scope.con[index][key]._id == scope.model[index]){
       scope.str[index] = scope.con[index][key].name;
       break;
      }
     
     if(success)
      scope.con[index+1] = angular.copy(items);
     
	 scope.con.splice(index+1);
	 scope.str.splice(index+1);
	 scope.model.splice(index+1);
    });
   };
  
   scope.$watch('model', function(nv,ov){
    if(Array.isArray(scope.model)){
     for(var i = 0; i < scope.model.length; i++){
      loop(i);
     }
    }
   });
  }
 }
}]);
*/

app.directive('categoryCreator', ['Category', function(Category){ 
	return {
		restrict: 'E',
		templateUrl: 'views/directiveCategoryCreator',
		scope: {
			model: "=",
			str : "=?",
			change : "&?"
		},
		link: function(scope, element, attr){
			
			function loop(i){
				i = parseInt(i);
				scope.Category.getChilds(scope.model[i], function(success, childs){
					if(success){
						scope.con[i+1] = childs;
						
						for(var key in scope.con[i])
							if(scope.con[i][key]._id == scope.model[i]){
								scope.str[i] = scope.con[i][key].name;
								break;
							}
						
					}
				});
			};
			
			
			
			scope.Category = angular.copy(Category);
			if(scope.Category.items.length == 0)
				scope.Category.s.parent = "false";
			
			scope.con = [];
			scope.str = [];
		
			scope.Category.get(function(){
				scope.con[0] = scope.Category.items;
				if(Array.isArray(scope.model)){
					for(var i = 0; i < scope.model.length; i++)
						loop(i);
				}else
					scope.model = [];
			});
			
			
			
			scope.childOf = function(index){
				index = parseInt(index);
				if(scope.change) scope.change();
				
				scope.Category.getChilds(scope.model[index], function(success, items){
					
					for(var key in scope.con[index])
						if(scope.con[index][key]._id == scope.model[index]){
							scope.str[index] = scope.con[index][key].name;
							break;
						}
					
					if(success)
						scope.con[index+1] = angular.copy(items);
					
					for(var i = 2; i > index+1; i--){
						delete scope.con[i];
						delete scope.str[i];
					}
				});
			};
		
			scope.$watch('model', function(nv,ov){
				if(Array.isArray(scope.model)){
					for(var i = 0; i < scope.model.length; i++){
						loop(i);
					}
				}
			});
		}
	}
}]);

app.directive('categoryPicker', ['Category', function(Category){
 return {
  restrict: 'E',
  templateUrl: 'views/directiveCategoryPicker',
  scope: {
   model: "=",
   str : "=?",
   change : "&?"
  },
  link: function(scope, element, attr){
   
   function loop(i){
    i = parseInt(i);
    scope.Category.getChilds(scope.model[i], function(success, childs){
     if(success){
      scope.con[i+1] = childs;
      
      for(var key in scope.con[i])
       if(scope.con[i][key]._id == scope.model[i]){
        scope.str[i] = scope.con[i][key].name;
        break;
       }
      
     }
    });
   };
      
   scope.Category = angular.copy(Category);
   if(scope.Category.items.length == 0)
    scope.Category.s.parent = "false";
   
   scope.con = [];
   scope.str = [];
  
   scope.Category.get(function(){
    scope.con[0] = scope.Category.items;
    if(Array.isArray(scope.model)){
     for(var i = 0; i < scope.model.length; i++)
      loop(i);
    }else
     scope.model = [];
   });
     
   scope.childOf = function(index){
    index = parseInt(index);
    if(scope.change) scope.change();
    
    scope.Category.getChilds(scope.model[index], function(success, items){
     
	 scope.con.splice(index+1);
	 scope.str.splice(index+1);
	 scope.model.splice(index+1);
     for(var key in scope.con[index])
      if(scope.con[index][key]._id == scope.model[index]){
       scope.str[index] = scope.con[index][key].name;
       break;
      }
     
     if(success)
      scope.con[index+1] = angular.copy(items);
     
    });
   };
  
   scope.$watch('model', function(nv,ov){
    if(Array.isArray(scope.model)){
     for(var i = 0; i < scope.model.length; i++){
      loop(i);
     }
    }
   });
  }
 }
}]);

/*
app.directive('categoryPicker', ['Category', function(Category){
 return {
  restrict: 'E',
  templateUrl: 'views/directiveCategoryPicker',
  scope: {
   model: "=",
   str : "=?",
   change : "&?"
  },
  link: function(scope, element, attr){
   
   function loop(i){
    i = parseInt(i);
    scope.Category.getChilds(scope.model[i], function(success, childs){
     if(success){
      scope.con[i+1] = childs;
      
      for(var key in scope.con[i])
       if(scope.con[i][key]._id == scope.model[i]){
        scope.str[i] = scope.con[i][key].name;
        break;
       }
      
     }
    });
   };
      
   scope.Category = angular.copy(Category);
   if(scope.Category.items.length == 0)
    scope.Category.s.parent = "false";
   
   scope.con = [];
   scope.str = [];
  
   scope.Category.get(function(){
    scope.con[0] = scope.Category.items;
    if(Array.isArray(scope.model)){
     for(var i = 0; i < scope.model.length; i++)
      loop(i);
    }else
     scope.model = [];
   });
     
   scope.childOf = function(index){
    index = parseInt(index);
    if(scope.change) scope.change();
    
    scope.Category.getChilds(scope.model[index], function(success, items){
     
     for(var key in scope.con[index])
      if(scope.con[index][key]._id == scope.model[index]){
       scope.str[index] = scope.con[index][key].name;
       break;
      }
     
     if(success)
      scope.con[index+1] = angular.copy(items);
     
	 scope.con.splice(index+1);
	 scope.str.splice(index+1);
	 scope.model.splice(index+1);
    });
   };
  
   scope.$watch('model', function(nv,ov){
    if(Array.isArray(scope.model)){
     for(var i = 0; i < scope.model.length; i++){
      loop(i);
     }
    }
   });
  }
 }
}]);*/

app.directive('categoryfaqsCreator', ['Categoryfaqs', function(Categoryfaqs){ 
	return {
		restrict: 'E',
		templateUrl: 'views/directiveCategoryFaqsCreator',
		scope: {
			model: "=",
			str : "=?",
			change : "&?"
		},
		link: function(scope, element, attr){
			
			function loop(i){
				i = parseInt(i);
				scope.Categoryfaqs.getChilds(scope.model[i], function(success, childs){
					if(success){
						scope.con[i+1] = childs;
						
						for(var key in scope.con[i])
							if(scope.con[i][key]._id == scope.model[i]){
								scope.str[i] = scope.con[i][key].name;
								break;
							}
						
					}
				});
			};
			
			scope.Categoryfaqs = angular.copy(Categoryfaqs);
			if(scope.Categoryfaqs.items.length == 0)
				scope.Categoryfaqs.s.parent = "false";
			
			scope.con = [];
			scope.str = [];
		
			scope.Categoryfaqs.get(function(){
				scope.con[0] = scope.Categoryfaqs.items;
				if(Array.isArray(scope.model)){
					for(var i = 0; i < scope.model.length; i++)
						loop(i);
				}else
					scope.model = [];
			});
			
			
			
			scope.childOf = function(index){
				index = parseInt(index);
				if(scope.change) scope.change();
				
				scope.Categoryfaqs.getChilds(scope.model[index], function(success, items){
					
					for(var key in scope.con[index])
						if(scope.con[index][key]._id == scope.model[index]){
							scope.str[index] = scope.con[index][key].name;
							break;
						}
					
					if(success)
						scope.con[index+1] = angular.copy(items);
					
					for(var i = 2; i > index+1; i--){
						delete scope.con[i];
						delete scope.str[i];
					}
				});
			};
		
			scope.$watch('model', function(nv,ov){
				if(Array.isArray(scope.model)){
					for(var i = 0; i < scope.model.length; i++){
						loop(i);
					}
				}
			});
		}
	}
}]);

app.directive('categoryfaqsPicker', ['Categoryfaqs', function(Categoryfaqs){
 return {
  restrict: 'E',
  templateUrl: 'views/directiveCategoryFaqsPicker',
  scope: {
   model: "=",
   str : "=?",
   change : "&?"
  },
  link: function(scope, element, attr){
   
   function loop(i){
    i = parseInt(i);
    scope.Categoryfaqs.getChilds(scope.model[i], function(success, childs){
     if(success){
      scope.con[i+1] = childs;
      
      for(var key in scope.con[i])
       if(scope.con[i][key]._id == scope.model[i]){
        scope.str[i] = scope.con[i][key].name;
        break;
       }
      
     }
    });
   };
      
   scope.Categoryfaqs = angular.copy(Categoryfaqs);
   if(scope.Categoryfaqs.items.length == 0)
    scope.Categoryfaqs.s.parent = "false";
   
   scope.con = [];
   scope.str = [];
  
   scope.Categoryfaqs.get(function(){
    scope.con[0] = scope.Categoryfaqs.items;
    if(Array.isArray(scope.model)){
     for(var i = 0; i < scope.model.length; i++)
      loop(i);
    }else
     scope.model = [];
   });
     
   scope.childOf = function(index){
    index = parseInt(index);
    if(scope.change) scope.change();
    
    scope.Categoryfaqs.getChilds(scope.model[index], function(success, items){
     
	 scope.con.splice(index+1);
	 scope.str.splice(index+1);
	 scope.model.splice(index+1);
     for(var key in scope.con[index])
      if(scope.con[index][key]._id == scope.model[index]){
       scope.str[index] = scope.con[index][key].name;
       break;
      }
     
     if(success)
      scope.con[index+1] = angular.copy(items);
     
    });
   };
  
   scope.$watch('model', function(nv,ov){
    if(Array.isArray(scope.model)){
     for(var i = 0; i < scope.model.length; i++){
      loop(i);
     }
    }
   });
  }
 }
}]);

/*
app.directive('categoryfaqsPicker', ['Categoryfaqs', function(Categoryfaqs){
 return {
  restrict: 'E',
  templateUrl: 'views/directiveCategoryFaqsPicker',
  scope: {
   model: "=",
   str : "=?",
   change : "&?"
  },
  link: function(scope, element, attr){
   
   function loop(i){
    i = parseInt(i);
    scope.Categoryfaqs.getChilds(scope.model[i], function(success, childs){
     if(success){
      scope.con[i+1] = childs;
      
      for(var key in scope.con[i])
       if(scope.con[i][key]._id == scope.model[i]){
        scope.str[i] = scope.con[i][key].name;
        break;
       }
      
     }
    });
   };
      
   scope.Categoryfaqs = angular.copy(Categoryfaqs);
   if(scope.Categoryfaqs.items.length == 0)
    scope.Categoryfaqs.s.parent = "false";
   
   scope.con = [];
   scope.str = [];
  
   scope.Categoryfaqs.get(function(){
    scope.con[0] = scope.Categoryfaqs.items;
    if(Array.isArray(scope.model)){
     for(var i = 0; i < scope.model.length; i++)
      loop(i);
    }else
     scope.model = [];
   });
     
   scope.childOf = function(index){
    index = parseInt(index);
    if(scope.change) scope.change();
    
    scope.Categoryfaqs.getChilds(scope.model[index], function(success, items){
     
     for(var key in scope.con[index])
      if(scope.con[index][key]._id == scope.model[index]){
       scope.str[index] = scope.con[index][key].name;
       break;
      }
     
     if(success)
      scope.con[index+1] = angular.copy(items);
     
     for(var i = 3; i > index+1; i--){
      delete scope.con[i];
      delete scope.str[i];
      delete scope.model[i];
     }
    });
   };
  
   scope.$watch('model', function(nv,ov){
    if(Array.isArray(scope.model)){
     for(var i = 0; i < scope.model.length; i++){
      loop(i);
     }
    }
   });
  }
 }
}]);
*/

app.directive('doubleScroll', function () {
    
        return {
            restrict: 'A',
            scope: {
                wait: "="
            },
            link: function (scope, el, attrs) {
				scope.$watch('wait', function(newValue, oldValue) {
					if(newValue)
						$(el).doubleScroll();
				}, true);
            }
      };
  });
  
app.directive('stickyHeader', function () {
    
        return {
            restrict: 'A',
            scope: {
                wait: "="
            },
            link: function (scope, el, attrs) {
				scope.$watch('wait', function(newValue, oldValue) {
					if(newValue)
						$(el).stickyTableHeaders();
				}, true);
            }
      };
  });  
  
app.directive('btCarousel', function () {
        return {
            restrict: 'A',
            link: function (scope, el, attrs) {
				$(el).carousel("pause").removeData();
				$(el).carousel(0);
            }
      };
  }); 

app.directive('timePicker', function () {
    
        var options = {clear:"Limpiar",
        format: 'HH:i',
        container: 'body'};
    
        return {
            restrict: 'A',
            scope: {
                tpMax: "=",
                tpMin: "=",
				wait: "="
            },
            link: function (scope, el, attrs) {
			
				var make = function(){
					if(scope.tpMin)
						options.min = [scope.tpMin,0];
					if(scope.tpMax)
						options.max = [scope.tpMax,59];
					
					if(scope.wait){
						if(scope.tpMin && scope.tpMax)
							$(el).pickatime(options);
					}else
						$(el).pickatime(options);
				};
			
				scope.$watch('tpMax', function(newValue, oldValue) {
					make();
				}, true);
				
				scope.$watch('tpMin', function(newValue, oldValue) {
					make();
				}, true);
				
				if(!scope.wait)
					make();
            }
      };
  });

app.directive('datePicker', function () {
    
        var options = {monthsFull: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthsShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
        weekdaysFull: ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'],
        weekdaysShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
        selectYears: 100,
        selectMonths: true,
        today: 'Hoy',
        clear: 'Limpiar',
        close: 'Cerrar',
        format:'dd-mm-yyyy',
        container: 'body'};
    
        return {
            restrict: 'A',
            scope: {
                max: "=",
                min: "="
            },
            link: function (scope, el, attrs) {
                var today = Date.now();
                if(scope.min)
                    options.min = new Date(today+parseInt(scope.min));
                if(scope.max)
                    options.max = new Date(today+parseInt(scope.max));
					
                $(el).pickadate(options);
            }
      };
  });