//Login
app.controller('loginController', [ 'Auth','$state','$scope','$rootScope', function(Auth, $state, $scope, $rootScope){
	
	$rootScope.title = $state.current.title;
	if(Auth.isLogged()){
		return $state.go('main');
	}
	
		$scope.Auth = Auth;
		$scope.form = {};
		$scope.forgot = {};
		
		$scope.forgot = function(){
			Auth.forgotPassword($scope.forgot,function(success){
				if(success)
					$('#closeForgotModal').click();
			});
		};
			
		$scope.send = function(){
			$scope.showSpinner = true;
			
			Auth.login($scope.form,function(success){
				$scope.showSpinner=false;
				if(success){
					toastr.success('Ingreso exitoso','', {progressBar:true});
					return $state.go('main');
				}
			});
		};
}]);

//Recuperar clave
app.controller('restarPasswordController', [function($scope, Users, $state){
	
	$scope.Users = angular.copy(Users);
		
		$scope.send = function(){
			$scope.Users.p.token = $state.params.token;
			$scope.Users.p.tokensecret = $state.params.tokensecret;
			$scope.Users.putSelf(function(success){
				$state.go('/');
			});
		};
}]);

//Menu principal
app.controller('mainController', [ '$location','toastr','Auth','$state','$scope','$rootScope','Users', function($location, toastr, Auth, $state, $scope, $rootScope, Users){
	
	$rootScope.title = $state.current.title;
	if(!Auth.isLogged()){
		return $state.go('/');
	}
	$scope.Auth = Auth;
	
	/*if(!Auth.User){
		Auth.getUser;		
	}*/
	
	$scope.Users = angular.copy(Users);
	$scope.Users.getSelf(function(success){
		if(success){
			$scope.Users.p = $scope.Users.item;
			for(var key in $scope.Users.p.localities)
				$scope.Users.p.localities[key] = $scope.Users.p.localities[key]._id;
		}
	});
	 
	$scope.put = function(){
		
		delete $scope.Users.p.priv;		
		$scope.Users.putSelf($scope.Users.p,function(success){
			if(success){
				$scope.Users.p = {};
				$scope.Users.getSelf(function(success){
					if(success){
						$scope.Users.p = $scope.Users.item;
						for(var key in $scope.Users.p.localities)
							$scope.Users.p.localities[key] = $scope.Users.p.localities[key]._id;
					}
				});
				toastr.success('Datos editados exitosamente','',{progressBar:true});
			}
		})
	};
	
	$scope.logout = function(){
		$scope.Auth.logout();
		//$location.path('/');
	};	
	
}]);

//Notificaciones
app.controller('MyNotis', [ function(toastr, $rootScope, $state, $scope, $http, $location, $stateParams, $cookies, $cookieStore, $document, $window){
	
	$scope.notis = [];
	$scope.audio = new Audio("/assets/audio/solemn.ogg");
	$scope.audio.type= 'audio/ogg';
    $scope.audio.src= "/assets/audio/solemn.ogg";
	
	 
	$scope.clicknotis = function(){
		$http.get('http://50.21.179.35:8082/notifications',{
			ignoreLoadingBar: true,
			params:{ 
				token:$cookies.get('tokena'),
				tokensecret:$cookies.get('tokenSecreta'),
				start:0,
				max:5
			}}).then(function(response){
				if(response.data.e===0){
					$scope.notis = response.data.items;
				}
			},function(){});
	};
	
	if(typeof(EventSource) !== "undefined") {
		if($cookies.get('tokena') && $cookies.get('tokenSecreta')){
			var source = new EventSource('events/'+$cookies.get('tokena')+'/'+$cookies.get('tokenSecreta'));
			source.onmessage = function (event) {
				var data = JSON.parse(event.data);
				if(data.logout){
					$cookies.remove("tokena", {path:'/'});
					$cookies.remove("tokenSecreta", {path:'/'});
					$window.location.href='/';
				}else{
					$scope.audio.play();
					toastr.success(data.message, data.subject, {
						progressBar:true,
						timeOut: 0,
						extendedTimeOut: 5000,
						closeButton: true
					});
					$scope.notis.unshift(data);
					if($scope.notis.length>=5)
						$scope.notis.slice(0,5);
				}
            };
		}
		
	}
}]); 

//Editor de terminos y condiciones
app.controller('editorController', [ 'Valuepair', '$rootScope','$http','$scope','$cookies','toastr','$state', function( Valuepair, $rootScope, $http, $scope, $cookies, toastr, $state){
	
	$rootScope.title = $state.current.title;
	
	$scope.Valuepair = angular.copy(Valuepair);
	
	$http.get("http://50.21.179.35:8082/valuepairs/id/"+$state.current.term)
	.then(function(response) {
		if(response.data.e===0){
			$scope.value=response.data.value.value;
		}else{
			toastr.error(response.data.mes,'', {progressBar:true});
		}
	});
	
	if($cookies.get('token') && $cookies.get('tokenSecret')){
		$scope.form = {};
		$scope.subtitle = $state.current.subtitle;
		
		$scope.send = function(){
			$http.put("http://50.21.179.35:8082/valuepairs/id/"+$state.current.term,{
				token:$cookies.get('token'),
				tokensecret:$cookies.get('tokenSecret'),
				value: $scope.value
			})
			.then(function(response) {
				if(response.data.e===0){
					toastr.success("Contenido actualizado",'', {progressBar:true});
				}else{
					toastr.error(response.data.mes,'', {progressBar:true});
				}
			});
		};
		
	}
}]);

//Users
app.controller('usersController', [ 'Users','toastr','GeoPosition','Upload','Auth','$scope','$rootScope','$state','$stateParams', function( Users, toastr, GeoPosition, Upload, Auth, $scope, $rootScope, $state, $stateParams){
	
	$rootScope.title = $state.current.title;
	if(!Auth.isLogged())
		return $state.go('/');
		
	$scope.Users = angular.copy(Users);
	$scope.Users.s = {
		start: 0,
		limit: 20,
		status:1,
		currentPage: 1
	};
	
	$scope.priv = ['','Administrador','Moderador','Usuario'];
		
	$scope.Users.get();	
		
	$scope.validate = function(item){
		//$scope.Users.setPut(item);
		$scope.Users.p = {
			_id: item._id
		};
		$scope.Users.validate(function(success){
			if(success){
				$scope.Users.get();
			}
		});
	};
	
	$scope.post = function(){
		$scope.Users.a.datasource = "regular";
		//$scope.Users.a.valid = "true";
		
		if(!$scope.Users.a.photo.name)
			return toastr.error("Debes enviar una foto", '', {progressBar: true});
		
		if(!$scope.Users.a.cover.name)
			return toastr.error("Debes enviar un Cover", '', {progressBar: true});
				
		$scope.Users.post(function(success){
			if(success){
				toastr.success('Datos Agredados','',{progressBar:true});	
				$scope.Users.get();
				$scope.Users.a = {};
			}
		});
	};
	
	$scope.put = function(){	
				
		if($scope.Users.p.password){
			if($scope.Users.p.password.length < 8){
				return toastr.error('La contraseña debe tener mínimo 8 caracteres',{progressBar:true});
			}
		}else
			delete $scope.Users.p.password;
		
		$scope.Users.put(function(success){
			if(success){
				toastr.success('Datos Editados','',{progressBar:true});		
				$('#modalCerrar').click();	
				$scope.Users.p = {};
				$scope.Users.get();
			}
		});
			
	};
	
}]);

//banks 
app.controller('banksController', [ 'Banks', 'Auth','$scope','$rootScope','$state','$stateParams','toastr', function( Banks, Auth, $scope, $rootScope, $state, $stateParams, toastr){
	
	$rootScope.title = $state.current.title;
	if(!Auth.isLogged())
		return $state.go('/');
	
	$scope.Banks = angular.copy(Banks);
	$scope.Banks.s = {
		start: 0,
		limit: 20,
		currentPage: 1
	};
	
	$scope.Banks.get(); 
	
	$scope.post = function(){
		$scope.Banks.post(function(success){
			if(success){
				toastr.success('Datos Agredados','',{progressBar:true});	
				$scope.Banks.get();
				$scope.Banks.a = {};
			}
		});
	};
	
	$scope.put = function(){
		$scope.Banks.put(function(success){
			if(success){
				toastr.success('Datos Editados','',{progressBar:true});		
				$scope.Banks.get();
				$('#cerrarModal').click();
			}
		});
	}
}]);

//Banners
app.controller('bannerController', [ 'Banners', 'Auth','$scope','$rootScope','$state','$stateParams','toastr', function( Banners, Auth, $scope, $rootScope, $state, $stateParams, toastr){
	
	$rootScope.title = $state.current.title;
	if(!Auth.isLogged()){
		return $state.go('/');
	}
	
	$scope.Banners = angular.copy(Banners);
	$scope.Banners.s = {
		start: 0,
		limit: 20,
		currentPage: 1
	};
	
	$scope.Banners.get();
	 
	$scope.post = function(){				
		$scope.Banners.post(function(success){
			if(success){
				toastr.success('Datos Agredados','',{progressBar:true});	
				$scope.Banners.get();
				$scope.Banners.a = {};
			}
		});
	};

	$scope.put = function(){
		$scope.Banners.put(function(success){
			if(success){
				toastr.success('Datos Editados','',{progressBar:true});	
				$scope.Banners.get();
				$('#cerrarModal').click();				
			}	
		});
	};
}]);

//categories
app.controller('categoriesController', [ 'Category', '$timeout', 'NgMap', 'GeoPosition', 'toastr','Auth', '$scope', '$rootScope', '$state', '$stateParams', function( Category, $timeout, NgMap, GeoPosition, toastr, Auth, $scope, $rootScope, $state, $stateParams){
	
	$rootScope.title = $state.current.title;	
	
	if(!Auth.isLogged()){
		return $state.go('/');
	}	
	
	$scope.Category = angular.copy(Category);
	$scope.Category.s = {
		start: 0,
		limit: 20,
		currentPage: 1
	};
	
	$scope.Category.get();
	
	$scope.get = function(){
		$scope.Category.s.parent = false;
		if($scope.Category.s.categories.length > 0)
			$scope.Category.s.parent = $scope.Category.s.categories[$scope.Category.s.categories.length - 1];
		$scope.Category.get();
	};
	
	$scope.post = function(){	
		/*if(Array.isArray($scope.Category.a.precategories)){
			$scope.Category.a.categories = [];
			for(var i = 0; i < $scope.Category.a.precategories.length; i++){
				$scope.Category.a.categories[i] = $scope.Category.a.precategories[i]._id;
			}
		}*/
		
		if($scope.Category.a.categories.length > 0)
			$scope.Category.a.parent = $scope.Category.a.categories[$scope.Category.a.categories.length - 1];
		
		$scope.Category.post(function(success){
			if(success){ 
				toastr.success('Datos Agredados','',{progressBar:true});	
				$scope.Category.get(); 
				$scope.Category.a = {
					categories : []
				};				
			}
		});
	};

	$scope.put = function(){
		$scope.Category.put(function(success){
			if(success){
				toastr.success('Datos Editados','',{progressBar:true});		
				$scope.Category.get(); 
				$scope.Category.p = {
					categories : []
				};
				$('#cerrarModal').click();				
			}	
		});
	};
	
	
}]);

//faqs
app.controller('faqsController', [ 'Faqs', 'Auth', '$scope', '$rootScope', '$state', '$stateParams','toastr', function( Faqs, Auth, $scope, $rootScope, $state, $stateParams, toastr){
	
	$rootScope.title = $state.current.title;
	if(!Auth.isLogged())
		return $state.go('/');
	
	$scope.Faqs = angular.copy(Faqs);
	$scope.Faqs.s = {
		start: 0,
		limit: 20,
		currentPage: 1
	};
	
	$scope.Faqs.get();
	
	$scope.get = function(){
		$scope.Faqs.s.parent = false;
		if($scope.Faqs.s.categoriesfaq.length > 0)
			$scope.Faqs.s.parent = $scope.Faqs.s.categoriesfaq[$scope.Faqs.s.categoriesfaq.length - 1];
		$scope.Faqs.get();
	};
	
	$scope.post = function(){
		if(Array.isArray($scope.Faqs.a.precategoriesfaqs)){
			$scope.Faqs.a.categories = [];
			for(var i = 0; i < $scope.Faqs.a.precategoriesfaqs.length; i++){
				$scope.Faqs.a.categories[i] = $scope.Faqs.a.precategoriesfaqs[i]._id;
			}
		}
		
		$scope.Faqs.post(function(success){
			if(success){
				toastr.success('Datos Agredados','',{progressBar:true});
				$scope.Faqs.get();
				$scope.Faqs.a = {};
			}
		});
	};
	
	$scope.put = function(){
		$scope.Faqs.put(function(success){
			if(success){
				toastr.success('Datos Editados','',{progressBar:true});
				$scope.Faqs.get();
				$('#cerrarModal').click();
			}
		});
	}
}]);

 //categoryfaqs
app.controller('categoryfaqsController', [ 'Categoryfaqs', 'Auth', '$scope', '$rootScope', '$state', '$stateParams','toastr', function( Categoryfaqs, Auth, $scope, $rootScope, $state, $stateParams, toastr){
	
	$rootScope.title = $state.current.title;
	if(!Auth.isLogged())
		return $state.go('/');
	
	$scope.Categoryfaqs = angular.copy(Categoryfaqs);
	$scope.Categoryfaqs.s = {
		start: 0,
		limit: 20,
		currentPage: 1
	};
	
	$scope.Categoryfaqs.get();
	
	$scope.get = function(){
		$scope.Categoryfaqs.s.parent = false;
		if($scope.Categoryfaqs.s.categories.length > 0)
			$scope.Categoryfaqs.s.parent = $scope.Categoryfaqs.s.categories[$scope.Categoryfaqs.s.categories.length - 1];
		$scope.Categoryfaqs .get();
	};
	
	$scope.post = function(){
		if($scope.Categoryfaqs.a.categories.length > 0)
			$scope.Categoryfaqs.a.parent = $scope.Categoryfaqs.a.categories[$scope.Categoryfaqs.a.categories.length - 1];
		
		$scope.Categoryfaqs.post(function(success){
			if(success){
				toastr.success('Datos Agredados','',{progressBar:true});	
				$scope.Categoryfaqs.get();
				$scope.Categoryfaqs.a = {};
			}
		});
	};
	
	$scope.put = function(){
		$scope.Categoryfaqs.put(function(success){
			if(success){
				toastr.success('Datos Editados','',{progressBar:true});	
				$scope.Categoryfaqs.get();
				$('#cerrarModal').click();
			}
		});
	}
}]);

//Locality
app.controller('localityController', [ 'Locality', '$timeout', 'NgMap', 'GeoPosition', 'toastr','Auth', '$scope', '$rootScope', '$state', '$stateParams', function( Locality, $timeout, NgMap, GeoPosition, toastr, Auth, $scope, $rootScope, $state, $stateParams){
	
	$rootScope.title = $state.current.title;
	
	$scope.GeoPosition = GeoPosition;
	if(!GeoPosition.position)
		GeoPosition.request();
	
	if(!Auth.isLogged()){
		return $state.go('/');
	}
	
	var myMap, paths = [], polygon;
	var smyMap, spaths = [], spolygon;
	
	
	$scope.load = function(){
		if($scope.loaded)
			setSPolygon();
		else
			$timeout(function(){
				$scope.loaded = true;
				setSecondMap();
			}, 500);
	};
	
	function setSPolygon(){
		console.log("spol");
		if(spolygon){
			spaths = [];
			spolygon.setMap(null);
		}
		
		var newPaths = [];
		$scope.Locality.p.geometry.coordinates[0].pop();
		
		for(var i = 0; i < $scope.Locality.p.geometry.coordinates[0].length ; i++){
			newPaths.push({lat: $scope.Locality.p.geometry.coordinates[0][i][1], lng: $scope.Locality.p.geometry.coordinates[0][i][0]});
		}
		spaths = newPaths;
		spolygon = new google.maps.Polygon({
		  paths: spaths,
		  strokeColor: '#FF0000',
		  strokeOpacity: 0.8,
		  strokeWeight: 3,
		  editable: true,
		  fillColor: '#FF0000',
		  fillOpacity: 0.35,
		  autoclose: true
		});
		spolygon.setMap(smyMap);
		console.log(spolygon);
		spolygon.addListener('dblclick', function(e) {
		  if (e.vertex !== undefined) {
			spaths = spolygon.getPath().b;
			spaths.splice(e.vertex, 1);
			spolygon.setPath(spaths);
		  }
		});
	}
		
	function setPolygon(){
		if(polygon){
			paths = [];
			polygon.setMap(null);
		}
		polygon = new google.maps.Polygon({
		  paths: paths,
		  strokeColor: '#FF0000',
		  strokeOpacity: 0.8,
		  strokeWeight: 3,
		  editable: true,
		  fillColor: '#FF0000',
		  fillOpacity: 0.35,
		  autoclose: true
		});
		polygon.setMap(myMap);
		
		polygon.addListener('dblclick', function(e) {
		  if (e.vertex !== undefined) {
			paths = polygon.getPath().b;
			paths.splice(e.vertex, 1);
			polygon.setPath(paths);
		  }
		});
	}
		
	function setSecondMap(){
		NgMap.getMap("second").then(function(map) {
			console.log("second map");
			smyMap = map;
			
			smyMap.addListener('click', function(e) {
			  spaths = spolygon.getPath();
			  if (spaths !== undefined)
				spaths.push(e.latLng);
			  else
				spaths = [e.latLng];
			  spolygon.setPath(spaths);
			});
			
			setSPolygon();
		});
	}
	
	NgMap.getMap("first").then(function(map) {
		myMap = map;
		
		myMap.addListener('click', function(e) {
		  paths = polygon.getPath();
		  if (paths !== undefined)
			paths.push(e.latLng);
		  else
			paths = [e.latLng];
		  polygon.setPath(paths);
		});
		
		setPolygon();
	});
	
	$scope.Locality = angular.copy(Locality);
	$scope.Locality.s = {
		start: 0,
		limit: 20,
		currentPage: 1
	};
	
	$scope.Locality.get(); 
	
	$scope.get = function(){
		$scope.Locality.s.parent = false;
		if($scope.Locality.s.localities.length > 0)
			$scope.Locality.s.parent = $scope.Locality.s.localities[$scope.Locality.s.localities.length - 1];
		$scope.Locality.get();
	};
	
	$scope.post = function(){		
		if($scope.Locality.a.localities.length > 0)
			$scope.Locality.a.parent = $scope.Locality.a.localities[$scope.Locality.a.localities.length - 1];
		 
		$scope.Locality.a.coordinates = polygon.getPath().b; 
		
		$scope.Locality.post(function(success){
			if(success){ 
				toastr.success('Datos Agredados','',{progressBar:true});	
				$scope.Locality.get();
				$scope.Locality.a = {
					localities : []
				};
				setPolygon();
				$scope.$emit('newLoc', true);
			}
		});
	};

	$scope.put = function(){
		$scope.Locality.put(function(success){
			if(success){
				toastr.success('Datos Editados','',{progressBar:true});	
				$scope.Locality.get();
				$scope.Locality.p = {
					localities : []
				};
				$('#cerrarModal').click();				
			}	
		});
	};

}]);

//publications 
app.controller('publicationsController', [ 'Publications', 'Locality', 'Category', '$timeout', 'GeoPosition','Auth', '$scope', '$rootScope', '$state', '$stateParams','toastr', function( Publications, Locality, Category, $timeout, GeoPosition, Auth, $scope, $rootScope, $state, $stateParams,toastr){
	
	$rootScope.title = $state.current.title;
	if(!Auth.isLogged())
		return $state.go('/');
	
	$scope.Publications = angular.copy(Publications);
	
	if(GeoPosition.position){
		$scope.Publications.a.lat = angular.copy(GeoPosition.position.lat);
		$scope.Publications.a.lng = angular.copy(GeoPosition.position.lng);
	}else{
		GeoPosition.request(function(success){
			if(success){
				$scope.Publications.a.lat = angular.copy(GeoPosition.position.lat);
				$scope.Publications.a.lng = angular.copy(GeoPosition.position.lng);
			}
		});
	}
	
	$scope.Publications.s = {
		start: 0,
		limit: 20,
		currentPage: 1
	};
		
	$scope.get = function(){
		$scope.Publications.s.parent = false;
		if($scope.Publications.s.localities.length > 0)
			$scope.Publications.s.parent = $scope.Publications.s.localities[$scope.Publications.s.localities.length - 1];
		$scope.Publications.get();
	};
	
	$scope.Publications.get();
	
	$scope.post = function(){		
					
		if(!$scope.Publications.a.files)
			return toastr.error("Debes enviar una imagen", '', {progressBar: true});		
		
		if(!$scope.Publications.a.lat)
			return toastr.error("Debes marcar la ubicación en el mapa", '', {progressBar: true});
		
		$scope.Publications.post(function(success){
			if(success){
				toastr.success('Datos Agredados','',{progressBar:true});		
				$scope.Publications.get();
				$scope.Publications.a = {};
				$scope.$emit('newLoc', true);
			}
		});
	};

	$scope.put = function(){
		$scope.Publications.put(function(success){
			if(success){
				toastr.success('Datos Editados','',{progressBar:true});
				$scope.Publications.get();
				$scope.Publications.p = {};
				$('#cerrarModal').click();				
			}	
		});
	};
	
	/*$(document).ready(function(){
	  $('.date').mask('00/00/0000');
	  $('.stock').mask('000.000.000.000.000', {reverse:true});
	  $('.time').mask('00:00:00');
	  $('.date_time').mask('00/00/0000 00:00:00');
	  $('.cep').mask('00000-000');
	  $('.phone').mask('0000-0000');
	  $('.phone_with_ddd').mask('(00) 0000-0000');
	  $('.phone_us').mask('(000) 000-0000');
	  $('.mixed').mask('AAA 000-S0S');
	  $('.cpf').mask('000.000.000-00', {reverse: true});
	  $('.cnpj').mask('00.000.000/0000-00', {reverse: true});
	  $('.money').mask('000.000.000.000.000,00', {reverse: true});
	  $('.money2').mask("#.##0,00", {reverse: true});
	  $('.ip_address').mask('0ZZ.0ZZ.0ZZ.0ZZ', {
		translation: {
		  'Z': {
			pattern: /[0-9]/, optional: true
		  }
		}
	  });
	  $('.ip_address').mask('099.099.099.099');
	  $('.percent').mask('##0,00%', {reverse: true});
	  $('.clear-if-not-match').mask("00/00/0000", {clearIfNotMatch: true});
	  $('.placeholder').mask("00/00/0000", {placeholder: "__/__/____"});
	  $('.fallback').mask("00r00r0000", {
		  translation: {
			'r': {
			  pattern: /[\/]/,
			  fallback: '/'
			},
			placeholder: "__/__/____"
		  }
		});
	  $('.selectonfocus').mask("00/00/0000", {selectOnFocus: true});
	});*/
	
	$scope.mapClick = function(event){
		$scope.Publications.a.lat = event.latLng.lat();
		$scope.Publications.a.lng = event.latLng.lng();
	};
}]);

//reportComments
app.controller('reportCommentsController', [ 'ReportComments', 'toastr','Auth', '$scope', '$rootScope', '$state', '$stateParams', function( ReportComments, toastr, Auth, $scope, $rootScope, $state, $stateParams){
	
	$rootScope.title = $state.current.title;
	if(!Auth.isLogged())
		return $state.go('/');
	
	$scope.ReportComments = angular.copy(ReportComments);
	$scope.ReportComments.s = {
		start: 0,
		limit: 20,
		currentPage: 1
	};
	
	$scope.ReportComments.get();
	
	$scope.get = function(){
		$scope.ReportComments.s.parent = false;
		if($scope.ReportComments.s.categoriesfaq.length > 0)
			$scope.ReportComments.s.parent = $scope.ReportComments.s.categories[$scope.ReportComments.s.categories.length - 1];
		$scope.ReportComments.get();
	};	
	
}]);

 //reports
app.controller('reportsController', [ 'Reports','ReportComments', 'Auth', '$scope', '$state', '$stateParams','$rootScope', 'toastr', function( Reports,ReportComments, Auth, $scope, $state, $stateParams,$rootScope, toastr){
	
	$rootScope.title = $state.current.title;
	if(!Auth.isLogged())
		return $state.go('/');
	
	$scope.Reports = angular.copy(Reports);
	$scope.Reports.s = {
		start: 0,
		limit: 20,
		currentPage: 1
	};
	$scope.Reports.get();
	
	$scope.ReportComments = angular.copy(ReportComments);
	$scope.ReportComments.get();
	
	
}]);

 //reviews
app.controller('reviewsController', [ 'Reviews', 'Auth', '$scope', '$state', '$stateParams','$rootScope', 'toastr', function( Reviews, Auth, $scope, $state, $stateParams,$rootScope, toastr){
	
	$rootScope.title = $state.current.title;
	if(!Auth.isLogged())
		return $state.go('/');
	
	$scope.Reviews = angular.copy(Reviews);
	$scope.Reviews.s = {
		start: 0,
		limit: 20,
		currentPage: 1
	};
	
	$scope.Reviews.get();

}]);

 //transactions
app.controller('transactionsController', [ 'Transactions', 'Auth', '$scope', '$state', '$stateParams','$rootScope', 'toastr', function( Transactions, Auth, $scope, $state, $stateParams,$rootScope, toastr){
	
	$rootScope.title = $state.current.title;
	if(!Auth.isLogged())
		return $state.go('/');
	
	$scope.Transactions = angular.copy(Transactions);
	$scope.Transactions.s = {
		start: 0,
		limit: 20,
		currentPage: 1
	};
	
	$scope.Transactions.get();

}]);

//Spots
app.controller('spotsController', [ 'Spots', 'Auth','$scope','$rootScope','$state','$stateParams','toastr', function( Spots, Auth, $scope, $rootScope, $state, $stateParams,toastr){
	
	$rootScope.title = $state.current.title;
	if(!Auth.isLogged()){
		return $state.go('/');
	}
	
	$scope.Spots = angular.copy(Spots);
	$scope.Spots.s = {
		start: 0,
		limit: 20,
		currentPage: 1
	};
	
	$scope.Spots.get();
	 
	$scope.post = function(){				
		$scope.Spots.post(function(success){
			if(success){ 
				toastr.success('Datos Agregados','',{progressBar:true});	
				$scope.Spots.get();
				$scope.Spots.a = {};
			}
		});
	};

	$scope.put = function(){
		$scope.Spots.put(function(success){
			if(success){
				toastr.success('Datos Editados','',{progressBar:true});	
				$scope.Spots.get();
				$('#cerrarModal').click();				
			}	
		});
	};
}]);

