var express = require('express'),
	router = express.Router(),
	ObjectID = require('mongoskin').ObjectID,
	Chat = require('../objects/chat'),
	User = require('../objects/user');

	User = new User();
	Chat = new Chat();
	
	
router.route('/')
	.get(function(req,res){
        var body = req.query,
			skip = body.start || 0,
			limit = body.max || 20,
            id = req.params.id;
			
		limit = parseInt(limit);
		skip = parseInt(skip);
		
		var orderBy = {};
		if(body.indexU)
			orderBy.lastActive = -1;
		
		if(req.user){
			var find = {users: new ObjectID(req.user._id)};
			
			Chat.getMultiFull(find, {lastActive:-1}, skip, limit, function(success, notis){
				if(success){
					Chat.count(find, function(success, count){
						if(success){
							res.json({e:0, items:notis, total:count});
						}else
							res.json({e:2, mes:count});
					});
				}else
					res.json({e:1, mes: notis});
			});
		}else{
			return res.json({e:1,mes:"Token invalido"});
		}
	});
	
	
module.exports = router;