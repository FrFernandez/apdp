var express = require('express'),
	router = express.Router(),
	fileUtils = require('../utils/fileUtils'),
	multer = require('multer'),
	upload = multer({ dest: './temp/' }),
	ObjectID = require('mongoskin').ObjectID,
	sanetize = require('../middlewares/sanetize'),
	User = require('../objects/user'),
	Spot = require('../objects/spot');

	User = new User();
	Spot = new Spot();

	
router.use(sanetize());	
	
router.route('/search')
	.get(function(req,res){
		var body = req.query,
			start = body.start || 0,
			limit = body.limit || 20;
			
		start = parseInt(start);
		limit = parseInt(limit);
		
		
		return Spot.count({}, function(success, count){
			if(success)
				if(count>0)
					return Spot.find({},{name:1, mail: 1}, start, limit, function(success, users){
						if(success){
							return res.json({e:0, items:users, total: count});
						}else
							return res.json({e:1,mes:users});
					});
				else
					return res.json({e:0, items: [], total: 0});
			else
				return res.json({e:1, mes:count});
		});
	});
	
	
router.route('/id/:id')
	.get(function(req,res){
		var token = req.query.token,
			tokenSecret = req.query.tokensecret,
			id = req.params.id;
			
		if(ObjectID.isValid(id)){
			id = new ObjectID(id);
			
			Spot.findOne({_id: id}, function(success, mes){
				if(success){
					return res.json({e:0, user: mes});
				}else
					return res.json({e:2, mes: mes});
			});
		}else
			return res.json({e:1, mes: "ID invalido"});
		
	})
	.put(function(req,res){
		
		var body = req.body,
			id = req.params.id;
		
		if(ObjectID.isValid(id)){
			id = new ObjectID(id);
			
			if(req.user && User.isAdmin(req.user)){
				return Spot.findOne({_id:id}, function(success, spot){
					if(success){
						var data = {};
						
						if(body.title)
							data.title = body.title;
						
						if(body.description)
							data.description = body.description;
						
						if(body.href)
							data.href = body.href;
						
						
						return Spot.update({_id:id}, {"$set": data}, {}, function(success, mes){
							if(success){
								return res.json({e:0});
							}else{
								return res.json({e:3, mes:mes});
							}
						});
					}else{
						return res.json({e:3, mes:spot});
					}
				});
			}else
				return res.json({e:1,mes:"Token invalido"});
		}else
			return res.json({e:1, mes: "ID Invalido"});
	})
	.delete(function(req,res){
		var token = req.query.token,
			tokenSecret = req.query.tokensecret,
			id = req.params.id;
		
		if(ObjectID.isValid(id)){
			id = new ObjectID(id);
			if(req.user && User.isAdmin(req.user))
				return Spot.remove({_id:id}, function(success, mes){
					if(success){
						return res.json({e:0});
					}else
						return res.json({e:2, mes: mes});
				});
			else
				return res.json({e:1,mes:"Token invalido"});
		}else
			res.json({e:1, mes: "ID invalido"});
	});
	

router.route('/')
	.post(function(req,res){
		var body = req.body,
			token = body.token,
			tokenSecret = body.tokensecret;
			
		if(req.user && User.isAdmin(req.user)){
			
			Spot.insert(body, function(success, mes){
				if(success)
					res.json({e:0});
				else
					res.json({e:1, mes: mes});
			});
			
		}else	
			return res.json({e:1, mes: "Token invalido"});
	});
	 
module.exports = router;