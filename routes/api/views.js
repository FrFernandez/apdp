var express = require('express'),
	router = express.Router(),
	ObjectID = require('mongoskin').ObjectID,
	User = require('../objects/user'),
	View = require('../objects/view');

	User = new User();
	View = new View();



router.route('/search')
	.get(function(req,res){
		var body = req.query,
			start = body.start || false,
			limit = body.limit || false,
			find = {};
			
		start = parseInt(start);
		limit = parseInt(limit);
		
		if(body.gender != undefined)
			find.gender = parseInt(body.gender);
		
		if(body.minAge !== undefined && body.maxAge !== undefined){
			find.age = {"$gte": parseInt(body.minAge), "$lte": parseInt(body.maxAge)};
		}
		
		if(body.minTime){
			find.time = {};
			find.time["$gte"] = View.dateToTime(body.minTime);
		}
		
		if(body.maxTime){
			if(!find.time) find.time = {};
			find.time["$lte"] = View.dateToTime(body.maxTime);
		}
		
		if(ObjectID.isValid(body.house))
			find.house = new ObjectID(body.house);
		
		
		return View.count(find, function(success, count){
			if(success)
				if(count>0)
					return View.find(find, {time:1}, start, limit, function(success, users){
						if(success){
							return res.json({e:0,items:users, total: count});
						}else
							return res.json({e:1,mes:users});
					});
				else
					return res.json({e:0, items: [], total: 0});
			else
				return res.json({e:1, mes:count});
		});
	});
		
router.route('/')
	.get(function(req,res){
		var body = req.query,
			start = body.start || 0,
			limit = body.limit || 20,
			token = body.token,
			tokenSecret = body.tokensecret;
		
		return User.checkHash(token, tokenSecret, function(user){
			if(req.user && User.isHouse(user)){
			
				start = parseInt(start);
				limit = parseInt(limit);
				
				if(body.gender != undefined)
					find.gender = parseInt(body.gender);
				
				if(body.minAge !== undefined && body.maxAge !== undefined){
					find.age = {"$gte": parseInt(body.minAge), "$lte": parseInt(body.maxAge)};
				}
				
				if(body.minTime){
					find.time = {};
					find.time["$gte"] = View.dateToTime(body.minTime);
				}
				
				if(body.maxTime){
					if(!find.time) find.time = {};
					find.time["$lte"] = View.dateToTime(body.maxTime);
				}
			
				return View.count(find, function(success, count){
					if(success)
						if(count>0)
							return View.find(find, {time:-1}, start, limit, function(success, users){
								if(success){
									return res.json({e:0,items:users, total: count});
								}else
									return res.json({e:1,mes:users});
							});
						else
							return res.json({e:0, items: [], total: 0});
					else
						return res.json({e:1, mes:count});
				});	
			}else	
				res.json({e:1, mes: "Token invalido"});
		});
	});
	 
module.exports = router;