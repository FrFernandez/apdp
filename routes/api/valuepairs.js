var express = require('express'),
	router = express.Router(),
	multer = require('multer'),
	fileUtils = require('../utils/fileUtils'),
	upload = multer({ dest: './temp/' }),
	ObjectID = require('mongoskin').ObjectID,
	fs = require('fs'),
	Valuepair = require('../objects/valuepair'),
	User = require('../objects/user');
	
	
	User = new User();
	Valuepair = new Valuepair();
	
router.route('/id/:key')
	.get(function(req,res){
		console.log({key:req.params.key});
		return Valuepair.findOne({key:req.params.key}, function(success, value){
			if(success) 
				res.json({e:0, value: value});
			else
				res.json({e:2, mes: value});
		});
	})
	.put(function(req,res){
		var body = req.body,
			token = body.token,
			tokenSecret = body.tokensecret,
			key = req.params.key;
			
		return User.checkHash(token, tokenSecret, function(user){
			if(req.user && User.isAdmin(req.user)){
				return Valuepair.findOne({key:key}, function(success, cat){
					if(success){
						var data = {};
							
						if(body.value){
							data.value = body.value;
						}
							
						if(Object.keys(data).length > 0){
							Valuepair.update({key:key},{"$set":data}, function(success, mes){
								if(success)
									res.json({e:0});
								else
									res.json({e:3,mes:mes});
								return;
							});
							return;
						}else
							return res.json({e:2,mes:"Debes enviar al menos un campo para actualizar"});
							
					}else
						res.json({e:3, mes:cat});
					return;
				});
			}else
				return res.json({e:2,mes:"Token invalido"});
		});
	})
	.delete(function(req,res){
		var body = req.query,
			token = body.token,
			tokenSecret = body.tokensecret,
			key = req.params.key;
			
		return User.checkHash(token, tokenSecret, function(user){
			if(req.user && User.isAdmin(req.user)){
				Valuepair.remove({key:key}, function(success, mes){
					if(success){
						return res.json({e:0});
					}else return res.json({e:3, mes:mes});
				});
			}else
				return res.json({e:2,mes:"Token invalido"});
		});
	});
	
router.route('/')
	.post(function(req,res){
		var body = req.body,
			token = body.token,
			tokenSecret = body.tokensecret;
		
		if(!body.key ||
			!body.value){
				return res.json({
					e:7,
					mes:"Debes enviar todos los campos"
				});
			}
		
		User.checkHash(token, tokenSecret, function(user){
			if(req.user && User.isAdmin(req.user)){
				var data = {
					key : body.key,
					value : body.value
				};
					
				return Valuepair.insert(data, function(success, result){
					if(success)
						return res.json({e:0,id:result});
					else
						return res.json({e:3,mes:result});
				});
			}else
				return res.json({e:1,mes:"Token invalido"});
		});
	});	
	 
module.exports = router;