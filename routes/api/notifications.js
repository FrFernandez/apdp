var express = require('express'),
	router = express.Router(),
	ObjectID = require('mongoskin').ObjectID,
	Notification = require('../objects/notification'),
	User = require('../objects/user');

	User = new User();
	Notification = new Notification();
	
	
router.route('/')
	.get(function(req,res){
        var token = req.query.token,
			skip = req.query.start || 0,
			limit = req.query.max || 10,
            tokenSecret = req.query.tokensecret;
			
		limit = parseInt(limit);
		skip = parseInt(skip);
		User.checkHash(token, tokenSecret, function(user){
            if (user) {
				var find = {user:new ObjectID(user._id)};
				
				Notification.find(find,{time:-1}, skip, limit, function(success, notis){
					if(success){
						Notification.count(find, function(success, count){
							if(success){
								res.json({e:0,items:notis,total:count});
								Notification.update({user:new ObjectID(user._id)},{"$set":{status:2}},function(){return;},{multi: true});
							}else
								res.json({e:2, mes:count});
						});
					}else
						res.json({e:1, mes: notis});
				});
            }else{
                res.json({e:1,mes:"Token invalido"});
            }
        });
	
	
	});
module.exports = router;