var express = require('express'),
	router = express.Router(),
	fileUtils = require('../utils/fileUtils'),
	multer = require('multer'),
	request = require('request'),
	upload = multer({ dest: './temp/' }),
	ObjectID = require('mongoskin').ObjectID,
	sanetize = require('../middlewares/sanetize'),
	User = require('../objects/user'),
	Publication = require('../objects/publication'),
	Transaction = require('../objects/transaction'),
	View = require('../objects/view');

	User = new User();
	Publication = new Publication();
	Transaction = new Transaction();
	View = new View();
	
function makeView(user, publication){
	var data = {
		publication: new ObjectID(publication._id)
	};
	
	if(user){
		data.gender = user.gender;
		data.age = user.age;
		data.user = new ObjectID(user._id);
	}else{
		data.gender = 0;
		data.age = 0;
	}
	
	View.insert(data);
}	
	
router.use(sanetize({
	allowedTags: ['h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'center', 'h6', 'blockquote', 'p', 'ul', 'ol', 'font', 
		'nl', 'li', 'b', 'i', 'strong', 'em', 'strike', 'code', 'hr', 'br', 'div',
		'table', 'thead', 'caption', 'tbody', 'tr', 'th', 'td', 'pre', 'img'],
	allowedAttributes: {
		h1: ['style'],
		h2: ['style'],
		h3: ['style'],
		h4: ['style'],
		h5: ['style'],
		h6: ['style'],
		center: ['style'],
		h6: ['style'],
		blockquote: ['style'],
		p: ['style'],
		a: ['style'],
		ul: ['style'],
		ol: ['style'],
		font: ['style'],
		nl: ['style'],
		li: ['style'],
		b: ['style'],
		i: ['style'],
		strong: ['style'],
		em: ['style'],
		strike: ['style'],
		code: ['style'],
		hr: ['style'],
		br: ['style'],
		div: ['style'],
		table: ['style'],
		thead: ['style'],
		caption: ['style'],
		tbody: ['style'],
		tr: ['style'],
		th: ['style'],
		td: ['style'],
		pre: ['style'],
		img: ['src', 'alt', 'style']
	}
 }));	
	
router.route('/search')
	.get(function(req,res){
		var body = req.query,
			token = body.token,
			order = {},
			tokenSecret = body.tokensecret,
			start = body.start || 0,
			limit = body.limit || 20;
			
		start = parseInt(start);
		limit = parseInt(limit);
		
		if(req.user && body.byMe)
			body.user = new ObjectID(req.user._id);
		
		
		if(body.orderSales){
			order.sales = parseInt(body.orderSales);
		}
		
		
		if(body.orderDate){
			order.added = parseInt(body.orderDate);
		}
		
		return Publication.count(body, function(success, count){
			if(success)
				if(count>0){
					return Publication.getMultiFull(body, order, start, limit, function(success, users){
						if(success){
							return res.json({e:0, items:users, total: count});
						}else
							return res.json({e:1, mes:users});
					});
				}else
					return res.json({e:0, items: [], total: 0});
			else
				return res.json({e:1, mes:count});
		});
	});
	
router.route('/predictiveSearch')
	.get(function(req, res){
		
		var body = req.query;
		
		return Publication.count(body, function(success, count){
			if(success)
				if(count>0){
					return Publication.getMultiFull(body, function(success, items){
						if(success){
							
							let filters = {
								categories: [],
								localities: [],
								states: []
							}
							
							for(let key in items){
								let state;
								if(items[key].localities.length >= 2)
									state = items[key].localities[1];
								else
									state = items[key].localities[0];
								let sfound = false;
								for(let skey in filters.localities){
									if(filters.localities[skey]._id.equals(new ObjectID(state._id))){
										sfound = true;
										filters.localities[skey].sum++;
									}
								}
								if(!sfound && state)
									filters.localities.push({
										_id: new ObjectID(state._id),
										name: state.name,
										sum: 1
									});
									
								
								let category = items[key].categories[items[key].categories.length - 1];
								let cfound = false;
								for(let ckey in filters.categories){
									if(filters.categories[ckey]._id.equals(new ObjectID(category._id))){
										cfound = true;
										filters.categories[ckey].sum++;
									}
								}
								if(!cfound)
									filters.categories.push({
										_id: new ObjectID(category._id),
										name: category.name,
										sum: 1
									});	
									
								
							}
							
							
							return res.json({e:0, items:filters});
						}else
							return res.json({e:1, mes:users});
					});
				}else
					return res.json({e:0, items: [], total: 0});
			else
				return res.json({e:1, mes:count});
		});
	});
	
router.route('/id/:id')
	.get(function(req,res){
		var id = req.params.id;
			
			
		return Publication.getFull(id, function(success, mes){
			if(success){
				if(req.user){
					if(!new ObjectID(req.user._id).equals(mes.user._id))
						makeView(req.user, mes);
				}else
					makeView(false, mes);
				return res.json({e:0, item: mes});
			}else
				return res.json({e:2, mes: mes});
		});	
	})
	.put(upload.array('files',6),function(req,res,next){
		
		if(!req.files || req.files.length < 1)
			return next();
		
		var body = req.body,
			id = req.params.id;
			
		if(req.user){
			if(ObjectID.isValid(req.params.id)){
				id = new ObjectID(id);
				var find = {_id: id};
				
				if(User.isUser(req.user))
					find.user = new ObjectID(req.user._id);
				
				return Publication.findOne(find, function(success, publication){
					if(success){
						fileUtils.saveFile(req.files,"image",function(files){
							if(files){
								body.photos = files;
								
								return Publication.doUpdate(req.user, body, publication, function(success, mes){
									if(success)
										return res.json({e:0});
									else
										return res.json({e:1,mes:mes});
								});
							}else
								return res.json({e:3, mes:"Error al guardar archivo"});
						});
					}else
						return res.json({e:2, mes: publication});
				});
			}else 
				return res.json({e:1, mes: "ID Invalido"});
		}else
			return res.json({e:1,mes:"Token invalido"});
	})
	.put(function(req,res){
		var body = req.body;
		delete body.photos;
		if(req.user){
			if(ObjectID.isValid(req.params.id)){
				id = new ObjectID(req.params.id);
				var find = {_id: id};
				
				if(User.isUser(req.user))
					find.user = new ObjectID(req.user._id);
				
				Publication.findOne(find, function(success, publication){
					if(success){
						return Publication.doUpdate(req.user, body, publication, function(success, mes){
							if(success)
								return res.json({e:0});
							else
								return res.json({e:1,mes:mes});
						});
					}else
						return res.json({e:2, mes: publication});
				});
			}else
				return res.json({e:1, mes: "ID Invalido"});
		}else
			return res.json({e:1,mes:"Token invalido"});
	})
	.delete(function(req,res){
		var id = req.params.id;
		
		if(ObjectID.isValid(id)){
			id = new ObjectID(id);
			
			if(req.user && (User.isAdmin(req.user) || User.isMod(req.user)))
				return Publication.remove({_id:id}, function(success, mes){
					if(success){
						return res.json({e:0});
					}else
						return res.json({e:2, mes: mes});
				});
			else
				return res.json({e:1,mes:"Token invalido"});
		}else
			res.json({e:1, mes: "ID invalido"});
	});
	
router.route('/')
	.post(upload.array('files',6), function(req, res, next){
		
		var body = req.body;
	
		if(!req.files || req.files.length < 1)
			return next();
	
		if(req.user){
			body.user = req.user._id;
			body.localities = req.user.localities;
			return fileUtils.saveFile(req.files,"image",function(files){
				if(files){
					body.photos = files;
					
					return Publication.insert(body, function(success, mes){
						if(success)
							return res.json({e:0, id: mes});
						else
							return res.json({e:1, mes: mes});
					});
				}else
					return res.json({e:3, mes:"Error al guardar archivo"});
			});
		}else
			return res.json({e:1,mes:"Token invalido"});
	})
	.post(function(req,res){
		
		var body = req.body;
		if(req.user){
			if(!Array.isArray(req.body.files64) || req.body.files64.length < 1)
				return res.json({e:1,mes:"Debes enviar las fotos de la publicación"});
			
			body.user = req.user._id;
			body.localities = req.user.localities;
			
			return fileUtils.saveBase64Files(body.files64, function(files){
				if(files){
					body.photos = files;
					
					return Publication.insert(body, function(success, mes){
						if(success)
							return res.json({e:0, id: mes});
						else
							return res.json({e:1, mes: mes});
					});
				}else
					return res.json({e:3, mes:"Error al guardar archivo"});
			});
		}else
			return res.json({e:1,mes:"Token invalido"});
	});
	 
module.exports = router;