var express = require('express'),
	router = express.Router(),
	fileUtils = require('../utils/fileUtils'),
	multer = require('multer'),
	upload = multer({ dest: './temp/' }),
	ObjectID = require('mongoskin').ObjectID,
	sanetize = require('../middlewares/sanetize'),
	Category = require('../objects/category'),
	User = require('../objects/user');
	
	
	User = new User();
	Category = new Category();
	
router.use(sanetize());	
	
router.route('/search')
	.get(function(req,res){
		var body = req.query,
			start = body.start || 0,
			limit = body.limit || 999999,
			term = body.term && body.term.length > 0 ? body.term : false ;
			
		start = parseInt(start);
		limit = parseInt(limit);
		var find = {};
		if(term)
			find.name = {"$regex":new RegExp(term,'i')};
		if(body.parent)
			find.parent = ObjectID.isValid(body.parent) ? new ObjectID(body.parent) : false;
			
		Category.find(find,{searchName:1}, start, limit, function(success, categories){
			if(success){
				return res.json({e:0,items:categories});
			}else
				return res.json({e:1,mes:categories});
		});
		return;
	});
	
router.route('/id/:id')
	.get(function(req,res){
		if(ObjectID.isValid(req.params.id)){
			Category.findOne({_id: new ObjectID(req.params.id)}, function(success, category){
				if(success) 
					res.json({e:0, item: category});
				else
					res.json({e:2, mes: category});
			});
		}else
			res.json({e:1, mes:"ID invalido"});
		return;
	})
	.put(upload.single('file'), function(req,res, next){
		var body = req.body,
			id = req.params.id;
		
		if(!req.file)
			return next();
			
		if(req.user && User.isAdmin(req.user)){
			if(ObjectID.isValid(id)){
				id = new ObjectID(id);
				return fileUtils.saveFile([req.file], "image", function(files){
					if(files){
						body.picture = files[0];
						return Category.findOne({_id:id}, function(success, cat){
							if(success){
								return Category.doUpdate(body, cat, function(success, result){
									if(success)
										return res.json({e:0,id:result});
									else
										return res.json({e:3,mes:result});
								});	
							}else
								return res.json({e:3, mes:cat});
						});
					}else
						return res.json({e:3, mes:"Error al guardar archivo"});
				});
			}else
				return res.json({e:1,mes:"ID invalido"});	
		}else	
			return res.json({e:1, mes: "Token invalido"});
	})
	.put(function(req,res){
		var body = req.body,
			id = req.params.id;
			
		if(req.user && User.isAdmin(req.user)){
			if(ObjectID.isValid(id)){
				id = new ObjectID(id);
				
				return Category.findOne({_id:id}, function(success, cat){
					if(success){
						return Category.doUpdate(body, cat, function(success, result){
							if(success)
								return res.json({e:0,id:result});
							else
								return res.json({e:3,mes:result});
						});	
					}else
						return res.json({e:3, mes:cat});
				});
			}else
				return res.json({e:1,mes:"ID invalido"});	
		}else
			return res.json({e:2,mes:"Token invalido"});
	})
	.delete(function(req,res){
		var body = req.query,
			token = body.token,
			tokenSecret = body.tokensecret,
			id = req.params.id;
	
		if(ObjectID.isValid(id)){
			if(req.user && User.isAdmin(req.user)){
				Category.remove({_id:new ObjectID(id)}, function(success, mes){
					if(success){
						return res.json({e:0});
					}else return res.json({e:3, mes:mes});
				
				});
			}else
				return res.json({e:2,mes:"Token invalido"});
		}else
			return res.json({e:1,mes:"ID invalido"});
	});
	
router.route('/')
	.post(upload.single('file'), function(req, res, next){
		var body = req.body;
		
		if(!req.file)
			return next();
			
		if(req.user && User.isAdmin(req.user)){
			return fileUtils.saveFile([req.file], "image", function(files){
				if(files){
					body.picture = files[0];
					return Category.insert(body, function(success, result){
						if(success)
							return res.json({e:0,id:result});
						else
							return res.json({e:3,mes:result});
					});
				}else
					return res.json({e:3, mes:"Error al guardar archivo"});
			});
		}else	
			return res.json({e:1, mes: "Token invalido"});
	})
	.post(function(req,res){
		var body = req.body;
		
		if(!body.name){
			return res.json({
				e:7,
				mes:"Debes enviar los campos requeridos"
			});
		}
		
		if(req.user && User.isAdmin(req.user)){
			return Category.insert(body, function(success, result){
				if(success)
					return res.json({e:0,id:result});
				else
					return res.json({e:3,mes:result});
			});
		}else
			return res.json({e:1,mes:"Token invalido"});
	});	
	 
module.exports = router;