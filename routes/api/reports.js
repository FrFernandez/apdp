var express = require('express'),
	router = express.Router(),
	fileUtils = require('../utils/fileUtils'),
	multer = require('multer'),
	request = require('request'),
	upload = multer({ dest: './temp/' }),
	ObjectID = require('mongoskin').ObjectID,
	sanetize = require('../middlewares/sanetize'),
	User = require('../objects/user'),
	Report = require('../objects/report');

	User = new User();
	Report = new Report();

router.use(sanetize());	
	
router.route('/search')
	.get(function(req,res){
		var body = req.query,
			start = body.start || 0,
			limit = body.limit || 20;
			
		start = parseInt(start);
		limit = parseInt(limit);
		
		if(req.user && (User.isAdmin(req.user) || User.isMod(req.user))){
			return Report.count(body, function(success, count){
				if(success)
					if(count>0){
						return Report.getMultiFull(body,{}, start, limit, function(success, users){
							if(success){
								return res.json({e:0, items:users, total: count});
							}else
								return res.json({e:1, mes:users});
						});
					}else
						return res.json({e:0, items: [], total: 0});
				else
					return res.json({e:1, mes:count});
			});
		}else
			return res.json({e:1, mes:"Token invalido"});
		
	});
	
	
router.route('/forme')
	.get(function(req, res){
		var body = req.query,
			start = body.start || 0,
			limit = body.limit || 20;
			
		start = parseInt(start);
		limit = parseInt(limit);
		
		if(req.user){
			body.openfor = new ObjectID(req.user._id)
			return Report.count(body, function(success, count){
				if(success)
					if(count>0){
						return Report.getMultiFull(body,{}, start, limit, function(success, users){
							if(success){
								return res.json({e:0, items:users, total: count});
							}else
								return res.json({e:1, mes:users});
						});
					}else
						return res.json({e:0, items: [], total: 0});
				else
					return res.json({e:1, mes:count});
			});
		}else
			return res.json({e:1, mes:"Token invalido"});
	
	});
	
	
router.route('/id/:id')
	.put(function(req, res){
		var body = req.body;
		
		if(req.user && (User.isAdmin(req.user) || User.isMod(req.user))){
			return Report.update(body, function(success, mes){
				if(success){
					return res.json({e:0});
				}else
					return res.json({e:1, mes:mes});
			});
		}else
			return res.json({e:1, mes:"Token invalido"});
	
	});
	
router.route('/byme')
	.get(function(req, res){
		var body = req.query,
			start = body.start || 0,
			limit = body.limit || 20;
			
		start = parseInt(start);
		limit = parseInt(limit);
		
		if(req.user){
			body.openby = new ObjectID(req.user._id)
			return Report.count(body, function(success, count){
				if(success)
					if(count>0){
						return Report.getMultiFull(body,{}, start, limit, function(success, users){
							if(success){
								return res.json({e:0, items:users, total: count});
							}else
								return res.json({e:1, mes:users});
						});
					}else
						return res.json({e:0, items: [], total: 0});
				else
					return res.json({e:1, mes:count});
			});
		}else
			return res.json({e:1, mes:"Token invalido"});
	
	});	
	
router.route('/open/:transaction')
	.post(function(req,res){
		var body = req.body;
		if(req.user){
			if(ObjectID.isValid(req.params.transaction)){
				body.transaction = req.params.transaction;
				body.by = req.user._id;
				return Report.insert(body, function(success, mes){
					if(success){
						return res.json({e:0, id: mes});
					}else
						return res.json({e:2, mes: mes});
				});
			}else
				return res.json({e:1, mes: "ID Invalido"});
		}else
			return res.json({e:1,mes:"Token invalido"});
	});

 
module.exports = router;