var express = require('express'),
	router = express.Router(),
	ObjectID = require('mongoskin').ObjectID,
	sanetize = require('../middlewares/sanetize'),
	User = require('../objects/user'),
	Favorite = require('../objects/favorite'),
	Publication = require('../objects/publication');

	User = new User();
	Favorite = new Favorite();
	Publication = new Publication();
		
	
router.use(sanetize());	
	
router.route('/search')
	.get(function(req,res){
		var body = req.query,
			token = body.token,
			tokenSecret = body.tokensecret,
			start = body.start || 0,
			limit = body.limit || 20,
			find = {};
			
		start = parseInt(start);
		limit = parseInt(limit);
		
		if(body.publication && ObjectID.isValid(body.publication))
			find.publication = new ObjectID(body.publication);
		
		if(body.user && ObjectID.isValid(body.user))
			find.user = new ObjectID(body.user);
		
		if(body.byMe && req.user)
			find.user = new ObjectID(req.user._id);
		
		return Favorite.count(find, function(success, count){
			if(success)
				if(count>0){
					return Favorite.getMultiFull(find,{ time:-1}, start, limit, function(success, users){
						if(success){
							return res.json({e:0, items:users, total: count});
						}else
							return res.json({e:1, mes:users});
					});
				}else
					return res.json({e:0, items: [], total: 0});
			else
				return res.json({e:1, mes:count});
		});
	});
	
router.route('/id/:id')
	.delete(function(req,res){
		var id = req.params.id;
		
		if(ObjectID.isValid(id)){
		
			id = new ObjectID(id);
			
			if(req.user)
				return Favorite.remove({_id:id, user: new ObjectID(req.user._id)}, function(success, mes){
					if(success){
						return res.json({e:0});
					}else
						return res.json({e:2, mes: mes});
				});
			else
				return res.json({e:1,mes:"Token invalido"});
		}else
			res.json({e:1, mes: "ID invalido"});
	});

router.route('/')
	.post(function(req,res){
		var body = req.body;

		if(req.user){
			body.user = req.user._id;
			return Favorite.insert(body, function(success, mes){
				if(success)
					return res.json({e:0, id: mes});
				else
					return res.json({e:1, mes: mes});
			});
		}else
			return res.json({e:1,mes:"Token invalido"});
	});
	 
module.exports = router;