var express = require('express'),
	router = express.Router(),
	fileUtils = require('../utils/fileUtils'),
	multer = require('multer'),
	upload = multer({ dest: './temp/' }),
	ObjectID = require('mongoskin').ObjectID,
	sanetize = require('../middlewares/sanetize'),
	User = require('../objects/user'),
	Banner = require('../objects/banner');

	User = new User();
	Banner = new Banner();

	
router.use(sanetize());	
	
router.route('/search')
	.get(function(req,res){
		var body = req.query,
			start = body.start || 0,
			limit = body.limit || 20;
			
		start = parseInt(start);
		limit = parseInt(limit);
		
		
		return Banner.count({}, function(success, count){
			if(success)
				if(count>0)
					return Banner.find({},{name:1, mail: 1}, start, limit, function(success, users){
						if(success){
							return res.json({e:0, items:users, total: count});
						}else
							return res.json({e:1,mes:users});
					});
				else
					return res.json({e:0, items: [], total: 0});
			else
				return res.json({e:1, mes:count});
		});
	});
	
	
router.route('/id/:id')
	.get(function(req,res){
		var token = req.query.token,
			tokenSecret = req.query.tokensecret,
			id = req.params.id;
			
		if(ObjectID.isValid(id)){
			id = new ObjectID(id);
			
			Banner.findOne({_id: id}, function(success, mes){
				if(success){
					return res.json({e:0, user: mes});
				}else
					return res.json({e:2, mes: mes});
			});
		}else
			return res.json({e:1, mes: "ID invalido"});
		
	})
	.put(upload.single('file'),function(req,res){
		
		var body = req.body,
			token = body.token,
			tokenSecret = body.tokensecret,
			id = req.params.id;
		
		if(ObjectID.isValid(id)){
			id = new ObjectID(id);
			
			if(req.user && User.isAdmin(req.user)){
				fileUtils.saveFile([req.file],"image",function(files){
					if(files){
						return Banner.findOne({_id:id}, function(success, banner){
							if(success){
								return Banner.update({_id:id}, {"$set": {file: files[0]}}, {}, function(success, mes){
									if(success){
										fileUtils.deleteFile("routes/uploads/husk/"+banner.file);
										return res.json({e:0});
									}else{
										fileUtils.deleteFile("routes/uploads/husk/"+files[0]);
										return res.json({e:3, mes:mes});
									}
								});
							}else{
								fileUtils.deleteFile("routes/uploads/husk/"+files[0]);
								return res.json({e:3, mes:banner});
							}
						});
					}else
						return res.json({e:3, mes:"Error al guardar archivo"});
				});
			}else
				return res.json({e:1,mes:"Token invalido"});
		}else
			return res.json({e:1, mes: "ID Invalido"});
	})
	.delete(function(req,res){
		var token = req.query.token,
			tokenSecret = req.query.tokensecret,
			id = req.params.id;
		
		if(ObjectID.isValid(id)){
			id = new ObjectID(id);
			if(req.user && User.isAdmin(req.user))
				return Banner.remove({_id:id}, function(success, mes){
					if(success){
						return res.json({e:0});
					}else
						return res.json({e:2, mes: mes});
				});
			else
				return res.json({e:1,mes:"Token invalido"});
		}else
			res.json({e:1, mes: "ID invalido"});
	});
	

router.route('/')
	.post(upload.single('file'),function(req,res){
		var body = req.body,
			token = body.token,
			tokenSecret = body.tokensecret;
			
		if(!req.file)
			return res.json({e:7, mes:"Debes enviar todos los campos"});
		if(req.user && User.isAdmin(req.user)){
			
			return fileUtils.saveFile([req.file],"image",function(files){
				if(files){
					var data = {
						file: files[0]
					};
					
					Banner.insert(data, function(success, mes){
						if(success)
							res.json({e:0});
						else
							res.json({e:1, mes: mes});
					});
				}else
					return res.json({e:3, mes:"Error al guardar archivo"});
				
			});
			
					
		}else	
			return res.json({e:1, mes: "Token invalido"});
	});
	 
module.exports = router;