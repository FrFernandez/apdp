var express = require('express'),
	router = express.Router(),
	ObjectID = require('mongoskin').ObjectID,
	sanetize = require('../middlewares/sanetize'),
	User = require('../objects/user'),
	Friendship = require('../objects/friendship'),
	Follow = require('../objects/follow');

	User = new User();
	Follow = new Follow();
	Friendship = new Friendship();

router.use(sanetize());	
	
router.route('/toMe')
	.get(function(req,res){
		var body = req.query,
			start = body.start || 0,
			limit = body.limit || 20,
			find = {};
			
		start = parseInt(start);
		limit = parseInt(limit);
		
		if(req.user){
			find.followed = new ObjectID(req.user._id);
			return Follow.count(find, function(success, count){
				if(success){
					return Follow.getMultiFull(find, {_id: -1}, start, limit, function(success, users){
						if(success){
							return res.json({e:0, items:users, total: count});
						}else
							return res.json({e:1, mes:users});
					});
				}else
					return res.json({e:0, items: [], total: 0});
			});
		}else
			return res.json({e:1,mes:"Token invalido"});
	});
	
router.route('/followers/:id')
	.get(function(req,res){
		var body = req.query,
			start = body.start || 0,
			limit = body.limit || 20,
			find = {};
			
		start = parseInt(start);
		limit = parseInt(limit);
		
		if(ObjectID.isValid(req.params.id)){
			find.followed = new ObjectID(req.params.id);
			return Follow.count(find, function(success, count){
				if(success){
					return Follow.getMultiFull(find, {_id: -1}, start, limit, function(success, users){
						if(success){
							return res.json({e:0, items:users, total: count});
						}else
							return res.json({e:1, mes:users});
					});
				}else
					return res.json({e:0, items: [], total: 0});
			});
		}else
			return res.json({e:1,mes:"Token invalido"});
	});
	
router.route('/fromMe')
	.get(function(req,res){
		var body = req.query,
			start = body.start || 0,
			limit = body.limit || 20,
			find = {};
			
		start = parseInt(start);
		limit = parseInt(limit);
		
		if(req.user){
			find.follower = new ObjectID(req.user._id);
			return Follow.count(find, function(success, count){
				if(success){
					return Follow.getMultiFull(find, {_id: -1}, start, limit, function(success, users){
						if(success){
							return res.json({e:0, items:users, total: count});
						}else
							return res.json({e:1, mes:users});
					});
				}else
					return res.json({e:0, items: [], total: 0});
			});
		}else
			return res.json({e:1,mes:"Token invalido"});
	});
		
router.route('/follows/:id')
	.get(function(req,res){
		var body = req.query,
			start = body.start || 0,
			limit = body.limit || 20,
			find = {};
			
		start = parseInt(start);
		limit = parseInt(limit);
		
		if(ObjectID.isValid(req.params.id)){
			find.follower = new ObjectID(req.params.id);
			return Follow.count(find, function(success, count){
				if(success){
					return Follow.getMultiFull(find, {_id: -1}, start, limit, function(success, users){
						if(success){
							return res.json({e:0, items:users, total: count});
						}else
							return res.json({e:1, mes:users});
					});
				}else
					return res.json({e:0, items: [], total: 0});
			});
		}else
			return res.json({e:1,mes:"ID invalido"});
	});
	
router.route('/unfollow/:id')
	.delete(function(req, res){
		
		if(ObjectID.isValid(req.params.id)){
			if(req.user){
				let body = {};
				body.follower = new ObjectID(req.user._id);
				body.followed = new ObjectID(req.params.id);
			
			
				return Follow.remove({follower: new ObjectID(body.follower), followed: new ObjectID(body.followed)}, function(success, mes){
					if(success)
						return res.json({e:0, id: mes});
					else
						return res.json({e:1, mes: mes});
				});
			}else
				return res.json({e:1,mes:"Token invalido"});
		
		}else
			return res.json({e:1,mes:"ID invalido"});
	});		

router.route('/follow/:id')
	.post(function(req, res){
		var body = req.body;
		
		if(ObjectID.isValid(req.params.id)){
			if(req.user){
				body.follower = new ObjectID(req.user._id);
				body.followed = new ObjectID(req.params.id);
			
			
				return Follow.insert(body, function(success, mes){
					if(success)
						return res.json({e:0, id: mes});
					else
						return res.json({e:1, mes: mes});
				});
			}else
				return res.json({e:1,mes:"Token invalido"});
		
		}else
			return res.json({e:1,mes:"ID invalido"});
	});	
module.exports = router;