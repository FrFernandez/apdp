var express = require('express'),
	router = express.Router(),
	fileUtils = require('../utils/fileUtils'),
	multer = require('multer'),
	request = require('request'),
	upload = multer({ dest: './temp/' }),
	ObjectID = require('mongoskin').ObjectID,
	sanetize = require('../middlewares/sanetize'),
	PanaPago = require('../objects/panaPago');

	PanaPago = new PanaPago();
		
router.use(sanetize());	
	
router.route('/search')
	.get(function(req,res){
		var body = req.query,
			start = body.start || 0,
			limit = body.limit || 20;
			
		start = parseInt(start);
		limit = parseInt(limit);
		
		if(req.user){
			
			if(!User.isAdmin(req.user) && !User.isMod(req.user)){
				if(body.byMe)
					body.buyer = new ObjectID(req.user._id);
				else if(body.toMe)
					body.seller = new ObjectID(req.user._id);
				else 
					return res.json({e:1, mes:"No puedes hacer una busqueda general"});
			}
			
			return PanaPago.count(body, function(success, count){
				if(success)
					if(count>0){
						return PanaPago.getMultiFull(body,{}, start, limit, function(success, users){
							if(success){
								return res.json({e:0, items: users, total: count});
							}else
								return res.json({e:1, mes: users});
						});
					}else
						return res.json({e:0, items: [], total: 0});
				else
					return res.json({e:1, mes:count});
			});
		}else
			return res.json({e:1, mes:"Token invalido"});
	});

router.route('/pay/:id')
	.post(upload.single('file'), function(req, res){
		
		if(!req.file)
			return next();
			
		return fileUtils.saveFile([req.file], "image", function(files){
			if(files){
				body.photo = files[0];
				
				if(req.user){
					body.buyer = req.user._id;
					body.transaction = req.params.id;
					
					return PanaPago.insert(body, function(success, mes){
						if(success)
							return res.json({e:1, id: mes});
						else
							return res.json({e:2, mes: 'Token invalido'});
					});
				
				}else
					return res.json({e:1, mes: 'Token invalido'});
			}else
				return res.json({e:3, mes:"Error al guardar archivo"});
		});		
		
	})
	.post(function(req, res){
		var body = req.body;
		if(req.user){
			body.buyer = req.user._id;
			body.transaction = req.params.id;
			
			return PanaPago.insert(body, function(success, mes){
				if(success)
					return res.json({e:1, id: mes});
				else
					return res.json({e:2, mes: 'Token invalido'});
			});
		
		}else
			return res.json({e:1, mes: 'Token invalido'});
	});	 
module.exports = router;