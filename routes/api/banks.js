var express = require('express'),
	router = express.Router(),
	fileUtils = require('../utils/fileUtils'),
	multer = require('multer'),
	upload = multer({ dest: './temp/' }),
	ObjectID = require('mongoskin').ObjectID,
	sanetize = require('../middlewares/sanetize'),
	Bank = require('../objects/bank'),
	User = require('../objects/user');
	
	
	User = new User();
	Bank = new Bank();
	
router.use(sanetize());	
	
router.route('/search')
	.get(function(req,res){
		var body = req.query,
			start = body.start || 0,
			limit = body.limit || 999999,
			term = body.term && body.term.length > 0 ? body.term : false,
			find = {};
			
		start = parseInt(start);	
		limit = parseInt(limit);	
			
		Bank.find(find, {name:1}, start, limit, function(success, banks){
			if(success){
				return res.json({e:0,items:banks});
			}else
				return res.json({e:1,mes:banks});
		});
		return;
	});
	
router.route('/id/:id')
	.get(function(req,res){
		if(ObjectID.isValid(req.params.id)){
			Bank.findOne({_id: new ObjectID(req.params.id)}, function(success, bank){
				if(success) 
					res.json({e:0, item: bank});
				else
					res.json({e:2, mes: bank});
			});
		}else
			res.json({e:1, mes:"ID invalido"});
		return;
	})
	.put(function(req,res){
		var body = req.body,
			id = req.params.id;
			
		if(req.user && User.isAdmin(req.user)){
			if(ObjectID.isValid(id)){
				id = new ObjectID(id);
				
				return Bank.findOne({_id:id}, function(success, bank){
					if(success){
						return Bank.doUpdate(id, body, function(success, result){
							if(success)
								return res.json({e:0,id:result});
							else
								return res.json({e:3,mes:result});
						});	
					}else
						return res.json({e:3, mes:bank});
				});
			}else
				return res.json({e:1,mes:"ID invalido"});	
		}else
			return res.json({e:2,mes:"Token invalido"});
	})
	.delete(function(req,res){
		var id = req.params.id;
		if(ObjectID.isValid(id)){
			if(req.user && User.isAdmin(req.user)){
				Bank.remove({_id:new ObjectID(id)}, function(success, mes){
					if(success){
						return res.json({e:0});
					}else return res.json({e:3, mes:mes});
				});
			}else
				return res.json({e:2,mes:"Token invalido"});
		}else
			return res.json({e:1,mes:"ID invalido"});
	});
	
router.route('/')
	.post(function(req,res){
		var body = req.body;
		
		console.log(body);
		console.log(req.user);
		
		if(req.user && User.isAdmin(req.user)){
			return Bank.insert(body, function(success, result){
				if(success)
					return res.json({e:0,id:result});
				else
					return res.json({e:3,mes:result});
			});
		}else
			return res.json({e:1,mes:"Token invalido"});
	});	
	 
module.exports = router;