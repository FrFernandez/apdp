var db = require('../utils/db'),
	collection = db.collection("follow"),
	ObjectID = require('mongoskin').ObjectID,
	User = require(__dirname +'/user'),
	Notification = require(__dirname +'/notification'),
	method = Follow.prototype;

	User = new User();
	Notification = new Notification();
	
	
function Follow(){

}

method.update = function(query, order, extra, cb){
	return collection.update(query, order, extra, function(err){
		if(err){
			console.log(err);
			return cb ? cb(false, "Error al actualizar") : false;
		}else{
			return cb ? cb(true) : false;
		}
	});
};

method.remove = function(filter, cb){
	return collection.remove(filter, function(err){
		if(err){
			console.log(err);
			return cb ? cb(false, "Error al eliminar") : false;
		}else
			return cb ? cb(true) : false;
	});
};

method.count = function(filter, cb){
	return collection.count(filter, function(err, count){
		if(err){
			console.log(err);
			cb(false, "Error al conseguir total de busqueda");
		}else
			cb(true, count);
		return;
	});
};

method.find = function(filter, order, skip, limit, cb){
	
	if(typeof order == 'function'){
		cb = order;
		order = false;
	}
	
	var promise = collection.find(filter);
	if(order)
		promise.sort(order);
	if(skip)
		promise.skip(skip);
	if(limit)
		promise.limit(limit);
		
	return promise.toArray(function(err, users){
		if(err){
			console.log(err);
			return cb ? cb(false, "Error al ejecutar busqueda") : false;
		}else
			return cb ? cb(true, users) : false;
	});
};

method.findOne = function(filter, cb){
	collection.findOne(filter, function(err, user){
		if(err){
			console.log(err);
			return cb ? cb(false, "Error al encontrar") : false;
		}else if(user)
			return cb ? cb(true, user) : false;
		else
			return cb ? cb(false, "Follow no encontrada") : false;
	});
};

method.getMultiFull = function(filter, order, skip, limit, cb){
	if(typeof order == 'function'){
		cb = order;
		order = false;
	}
	
	var promise = collection.find(filter);
	if(order)
		promise.sort(order);
	if(skip)
		promise.skip(skip);
	if(limit)
		promise.limit(limit);
		
	return promise.toArray(function(err, users){
		if(err){
			return cb ? cb(false, "Error al ejecutar busqueda") : false;
		}else if(users){
			var il = users.length, round = 1, i = 0, total = 0, newUsers = [];
			var handleLoop = function(i){
				this.getFull(users[i], function(success, user){
					if(success)
						newUsers[i] = user;
					else
						newUsers[i] = users[i];
					
					if(round == il)
						return cb ? cb(true, newUsers) : false;
					return round++;
				}.bind(this));
				return;
			}.bind(this);
			
			if(il>0)
				for(;i<il;i++)
					handleLoop(i);
			else
				return cb? cb(true, users) : false;
		}
		else return cb ? cb(false, "Follows no encontrados") : false;
	}.bind(this));
};

method.proccess = function(follow, cb){
	return User.getFull(new ObjectID(follow.follower), function(success, follower){
		if(success)
			follow.follower = follower;
		User.getFull(new ObjectID(follow.followed), function(success, followed){
			if(success)
				follow.followed = followed;
			return cb ? cb(true, follow) : false;
		});
	});
};

method.getFull = function(id, cb){
	if(ObjectID.isValid(id)){
		id = new ObjectID(id);
		this.findOne({_id:id}, function(success, follow){
			if(success){
				return this.proccess(follow, cb);
			}else
				return cb ? cb(false, "Follow no encontrado") : false;
		}.bind(this));
	}else if(id !== null && typeof id === 'object')
		return this.proccess(id, cb);
	else
		return cb ? cb(false, "ID Invalido") : false;
};

method.insert = function(body, cb){
	if(ObjectID.isValid(body.follower) &&
		ObjectID.isValid(body.followed)){
		
		var data = {};
		data.follower = new ObjectID(body.follower);
		data.followed = new ObjectID(body.followed);
		data.since = Date.now();
		
		return User.findOne({_id: data.follower}, function(success, user){
			if(success){
				return User.findOne({_id: data.followed}, function(success, house){
					if(success){
						return this.findOne({follower: data.follower, followed: data.followed}, function(success, follow){
							if(success){
								return cb ? cb(false, "Ya existe esta amistad") : false;
							}else{
								return collection.insert(data, function(err, result){
									if(err){
										Notification.insert({
											subject: "Nuevo seguidor",
											message: "Ahora "+user.username+" te sigue!",
											type: "follower",
											user: data.followed
										});
									
										return cb ? cb(false, "Error al insertar") : false;
									}else{
										return cb ? cb(true, new ObjectID(result.insertedIds[0])) : false;
									}
								}.bind(this));
							}
						}.bind(this));
					}else
						return cb ? cb(false, "Usuario no encontrada") : false;
				}.bind(this));
			}else
				return cb ? cb(false, "Usuario no encontrado") : false;
		}.bind(this));
	}else
		return cb ? cb(false, "Debes enviar todos los campos") : false;
};

module.exports = Follow;