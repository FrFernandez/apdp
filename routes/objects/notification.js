var db = require('../utils/db'),
	collection = db.collection("notifications"),
	ObjectID = require('mongoskin').ObjectID,
	method = Notification.prototype,
	User = require(__dirname +'/user'),
	fcm = require('../utils/myFCM'),
	mailer = require('../utils/mailer');
	
	User = new User();
	
function Notification(){

}

method.update = function(query, order, extra, cb){
	collection.update(query, order, extra,  function(err){
		if(err){
			console.log(err);
			return cb ? cb(false, "Error al actualizar") : false;
		}else{
			return cb ? cb(true) : false;
		}
	});
};

method.remove = function(filter, cb){
	collection.remove(filter, function(err){
		if(err){
			console.log(err);
			return cb ? cb(false, "Error al eliminar") : false;
		}else
			return cb ? cb(true) : false;
	});
}

method.count = function(filter, cb){
	collection.count(filter, function(err, count){
		if(err){
			console.log(err);
			cb(false, "Error al conseguir total de busqueda");
		}else
			cb(true, count);
		return;
	});
	return;
};

method.find = function(filter, order, skip, limit, cb){
	if(typeof order == 'function'){
		cb = order;
		order = false;
	}
	
	var promise = collection.find(filter);
	if(order)
		promise.sort(order);
	if(skip)
		promise.skip(skip);
	if(limit)
		promise.limit(limit);
		
	promise.toArray(function(err, users){
		if(err){
			console.log(err);
			return cb ? cb(false, "Error al ejecutar busqueda") : false;
		}else if(users) return cb ? cb(true, users) : false;
		else return cb ? cb(false, "No se han encontrado notificaciones") : false;
	});
};

/*method.planRequest(data, cb){
	
	
	return this.insert(data, cb);
}*/

method.insert = function(data, cb){
	if(data.message && 
	data.subject && 
	ObjectID.isValid(data.user)){
		
		data.user = new ObjectID(data.user);
		data.fromUser = new ObjectID(data.fromUser);
		data.status = 0;
        data.time = Date.now();
		
		console.log(data);
		User.findOne({_id:data.user}, function(success, user){
			if(success)
				return collection.insert(data, function(err, result){
					if(err){
						return cb ? cb(false, "Error al agregar notificacion") : false;
					}else{
						if(user && user.devices.length>0){
							var tokens = [];
							for(var i = 0 ; i < user.devices.length ; i++){
								tokens.push(user.devices[i].googleToken);
							}
							fcm.send(tokens,{
								"data.body":data.message,
								"data.title":data.subject
							});
						}
						
						/*var mailOptions = mailer.push(user, data);
							mailOptions.to = user.mail;
							mailer.sendMail(mailOptions);*/
						return cb ? cb(true, new ObjectID(result.insertedIds[0])) : false;
					}
				});
			else
				return cb ? cb(false, "Usuario no encontrado") : false;
		}.bind(this));
	}else
		return cb ? cb(false, "Debes enviar todos los campos") : false;
};

module.exports = Notification;
