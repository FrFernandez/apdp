var db = require('../utils/db'),
	collection = db.collection("transMessage"),
	ObjectID = require('mongoskin').ObjectID,
	User = require(__dirname +'/user'),
	Transaction = require(__dirname +'/transaction'),
	method = TransMessage.prototype;
	
	Transaction = new Transaction();
	User = new User();

function TransMessage(){
}

method.update = function(query, order, extra, cb){
	collection.update(query, order, extra, function(err){
		if(err){
			console.log(err);
			return cb ? cb(false, "Error al actualizar") : false;
		}else{
			return cb ? cb(true) : false;
		}
	});
};

method.remove = function(filter, cb){
	return collection.remove(filter, function(err){
		if(err){
			console.log(err);
			return cb ? cb(false, "Error al eliminar") : false;
		}else
			return cb ? cb(true) : false;
	});
};

method.count = function(filter, cb){
	collection.count(filter, function(err, count){
		if(err){
			console.log(err);
			cb(false, "Error al conseguir total de busqueda");
		}else
			cb(true, count);
		return;
	});
	return;
};

method.find = function(filter, order, skip, limit, cb){
	
	if(typeof order == 'function'){
		cb = order;
		order = false;
	}
	
	var promise = collection.find(filter);
	if(order)
		promise.sort(order);
	if(skip)
		promise.skip(skip);
	if(limit)
		promise.limit(limit);
		
	promise.toArray(function(err, users){
		if(err){
			console.log(err);
			return cb ? cb(false, "Error al ejecutar busqueda") : false;
		}else
			return cb ? cb(true, users) : false;
	});
};

method.findOne = function(filter, cb){
	collection.findOne(filter, function(err, user){
		if(err){
			console.log(err);
			return cb ? cb(false, "Error al encontrar") : false;
		}else if(user)
			return cb ? cb(true, user) : false;
		else
			return cb ? cb(false, "TransMessage no encontrada") : false;
	});
};

method.getMultiFull = function(filter, order, skip, limit, cb){
	if(typeof order == 'function'){
		cb = order;
		order = false;
	}
	
	var promise = collection.find(filter);
	if(order)
		promise.sort(order);
	if(skip)
		promise.skip(skip);
	if(limit)
		promise.limit(limit);
		
	promise.toArray(function(err, users){
		if(err){
			return cb ? cb(false, "Error al ejecutar busqueda") : false;
		}else if(users){
			var il = users.length, round = 1, i = 0, total = 0, newUsers = [];
			var handleLoop = function(i){
				this.getFull(users[i], function(success, user){
					if(success)
						newUsers[i] = user;
					else
						newUsers[i] = users[i];
					
					if(round == il)
						return cb ? cb(true, newUsers) : false;
					round++;
					return;
				}.bind(this));
				return;
			}.bind(this);
			
			if(il>0)
				for(;i<il;i++)
					handleLoop(i);
			else
				return cb? cb(true, users) : false;
		}
		else return cb ? cb(false, "Mensajes no encontrados") : false;
	}.bind(this));
};

method.proccess = function(transMessage, cb){
	
		switch(transMessage.type){
			case 1:
				return User.getFull(new ObjectID(transMessage.user), function(success, user){
					if(success){
						delete user.notifications;
						transMessage.user = user;
					}
					return cb ? cb(true, transMessage) : false;
				});
				break;
			case 2:
				return cb ? cb(true, transMessage) : false;
				break;
		}	
};

method.getFull = function(id, cb){
	if(ObjectID.isValid(id)){
		id = new ObjectID(id);
		this.findOne({_id:id}, function(success, transMessage){
			if(success){
				return this.proccess(transMessage, cb);
			}else
				return cb ? cb(false, "Mensaje no encontrado") : false;
		}.bind(this));
	}else if(id !== null && typeof id === 'object')
		return this.proccess(id, cb);
	else
		return cb ? cb(false, "ID Invalido") : false;
};


method.insert = function(body, cb){

	if(!ObjectID.isValid(body.transaction))
		return cb ? cb(false, "Debes enviar a que propuesta pertenece este mensaje") : false;
	
	if(body.type==1 && !ObjectID.isValid(body.user._id))
		return cb ? cb(false, "Debes enviar a que usuario pertenece este mensaje") : false;
	
	if(!body.transMessage)
		return cb ? cb(false, "Debes enviar el cuerpo del mensaje") : false;
		
	var data = {
		transaction : new ObjectID(body.transaction),
		transMessage : body.transMessage,
		type: parseInt(body.type),
		file: body.file || false,
		time: Date.now()
	};
	
	return Transaction.findOne({_id:data.transaction}, function(success, transaction){
		if(success){
			if(data.type == 1){
				data.user = new ObjectID(body.user._id);
				if(new ObjectID(transaction.messenger).equals(data.user)){
					data.by = 0;
				}else if(new ObjectID(transaction.solicitant).equals(data.user)){
					data.by = 1;
				}else if(ObjectID.isValid(transaction.emp) && new ObjectID(transaction.emp).equals(data.user)){
					data.by = 2;
				}else if(User.isAdmin(body.user)){
					data.by = 4;
				}else
					return cb ? cb(false, "Usuario no pertenece a esta propuesta") : false;
			}else
				data.by = 3;
			
			return collection.insert(data, function(err, result){
				if(err){
					console.log(err);
					return cb ? cb(false, "Error al insertar") : false;
				}else{
					data._id = new ObjectID(result.insertedIds[0]);
					
					Transaction.update({_id:data.transaction}, {"$set": {lastActive: Date.now()}});
					return this.getFull(data, function(success, data){
						return cb ? cb(true, data) : false;
					});
				}
			}.bind(this));	
			
		}else
			return cb ? cb(false, "Propuesta no encontrado") : false;
	}.bind(this));
};

module.exports = TransMessage;