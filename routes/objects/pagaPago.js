var db = require('../utils/db'),
	collection = db.collection("panaPagos"),
	ObjectID = require('mongoskin').ObjectID,
	method = PanaPago.prototype;
	
function PanaPago(){
	
}

method.dateToTime = function(date){
	date=date.split("-");
	return new Date(date[2]+"-"+date[1]+"-"+date[0]).getTime();
}

method.update = function(query, order, extra, cb){
	return collection.update(query, order, extra, function(err){
		if(err){
			console.log(err);
			return cb ? cb(false, "Error al actualizar") : false;
		}else{
			return cb ? cb(true) : false;
		}
	});
};

method.doUpdate = function(user, id, body, oldstatuss, oldstatusb, cb){
	var data = {};
	
	if(!ObjectID.isValid(id))
		return cb ? cb(false, "ID Invalido") : false;	
	id = new ObjectID(id);
	
	if(body.status && oldstatus == 1)
		data.status = parseInt(body.status);
		
	if(Object.keys(data).length > 0){
		return this.update({_id:id}, {"$set":data}, {}, function(success, mes){
			if(success){
				return cb ? cb(true, data.bitacora[0]) : false;
			}else{
				return cb ? cb(false, mes) : false;
			}
		});
	}else
		return cb ? cb(false, "Debes enviar al menos un campo para actualizar") : false;
};

method.remove = function(filter, cb){
	return collection.remove(filter, function(err){
		if(err){
			console.log(err);
			return cb ? cb(false, "Error al eliminar") : false;
		}else
			return cb ? cb(true) : false;
	});
};

method.count = function(filter, cb){
	filter = this.preparefind(filter);
	return collection.count(filter, function(err, count){
		if(err){
			console.log(err);
			return cb ? cb(false, "Error al conseguir total de busqueda") : false;
		}else
			return cb ? cb(true, count) : false;
		return;
	});
};

method.find = function(filter, order, skip, limit, cb){
	if(typeof order == 'function'){
		cb = order;
		order = false;
	}
	
	filter = this.preparefind(filter);
	var promise = collection.find(filter);
	if(order)
		promise.sort(order);
	if(skip)
		promise.skip(skip);
	if(limit)
		promise.limit(limit);
		
	return promise.toArray(function(err, panaPagos){
		if(err){
			console.log(err);
			return cb ? cb(false, "Error al ejecutar busqueda") : false;
		}else if(panaPagos) return cb ? cb(true, panaPagos) : false;
		else return cb ? cb(false, "Transacciones no encontradas") : false;
	});
};

method.preparefind = function(body){
	var find = {};

	if(body.buyer && ObjectID.isValid(body.buyer))
		find.buyer = new ObjectID(body.buyer)
	
	if(body.seller && ObjectID.isValid(body.seller))
		find.seller = new ObjectID(body.seller)
	
	if(body.transaction && ObjectID.isValid(body.transaction))
		find.transaction = new ObjectID(body.transaction)
	
	if(body.mintime){
		if(!find.time) find.time = {}
		find.time["$gte"] = this.dateToTime(body.mintime);
	}
	
	if(body.status)
		find.status = parseInt(body.status);
	
	if(body.maxtime){
		if(!find.time) find.time = {}
		find.time["$lte"] = this.dateToTime(body.maxtime);
	}
	return find;
};

method.findOne = function(filter, cb){
	return collection.findOne(filter, function(err, user){
		if(err){
			console.log(err);
			return cb ? cb(false, "Error al encontrar usuario") : false;
		}else if(user)
			return cb ? cb(true, user) : false;
		else return cb ? cb(false, "Usuario no encontrado") : false;
	});
};

method.getMultiFull = function(filter, order, skip, limit, cb){
	if(typeof order == 'function'){
		cb = order;
		order = false;
	}
	filter = this.preparefind(filter);
	var promise = collection.find(filter);
	if(order)
		promise.sort(order);
	if(skip)
		promise.skip(skip);
	if(limit)
		promise.limit(limit);
		
	return promise.toArray(function(err, items){
		if(err){
			return cb ? cb(false, "Error al ejecutar busqueda") : false;
		}else if(items){
			var il = items.length, round = 1, i = 0, total = 0, newitems = [];
			var handleLoop = function(i){
				this.getFull(items[i], function(success, user){
					if(success)
						newitems[i] = user;
					else
						newitems[i] = items[i];
					
					if(round == il)
						return cb ? cb(true, newitems) : false;
					round++;
					return;
				}.bind(this));
				return;
			}.bind(this);
			
			if(il>0)
				for(;i<il;i++)
					handleLoop(i);
			else
				return cb? cb(true, items) : false;
		}
		else return cb ? cb(false, "Usuarios no encontrados") : false;
	}.bind(this));
};

method.proccess = function(item, cb){
	return User.getFull({_id: new ObjectID(item.buyer)}, function(success, user){
		if(success)
			item.buyer = user;
			
		return User.getFull({_id: new ObjectID(item.seller)}, function(success, user){
			if(success)
				item.seller = user;
			
			return Transaction.getFull({_id: new ObjectID(item.transaction)}, function(success, transaction){
				if(success)
					item.transaction = transaction;
			
				return cb ? cb(true, user) : false;
			});
		});
	});
};

method.getFull = function(id, cb){
	if(ObjectID.isValid(id)){
		id = new ObjectID(id);
		return this.findOne({_id:id}, function(success, item){
			if(success){
				return this.proccess(item, cb);
			}else
				return cb ? cb(false, "Cuenta Bancaria no encontrada") : false;
		}.bind(this));
	}else if(id !== null && typeof id === 'object')
		return this.proccess(id, cb);
	else
		return cb ? cb(false, "ID Invalido") : false;
};

method.insert = function(body, cb){
	
	if(!body.transaction || !ObjectID.isValid(body.transaction))
		return cb ? cb(false, "Debes enviar a que transacción pertenece este pago") : false;
	
	if(!body.buyer || !ObjectID.isValid(body.buyer))
		return cb ? cb(false, "Debes enviar el comprador esta transacción") : false;
	
	if(!body.date)
		return cb ? cb(false, "Debes enviar la fecha de pago") : false;
	
	if(!body.way)
		return cb ? cb(false, "Debes enviar el tipo de pago") : false;
	
	if(!body.sum || isNaN(body.sum))
		return cb ? cb(false, "Debes enviar el monto del pago") : false;
	
	if(!body.bank)
		return cb ? cb(false, "Debes enviar el banco del pago") : false;
		
	if(!body.ref)
		return cb ? cb(false, "Debes enviar la referencia del pago") : false;
		
	var data = {
		transaction: new ObjectID(body.transaction),
		date: this.dateToTime(body.date),
		way: body.way,
		bank: body.bank,
		ref: body.ref,
		sum: parseFloat(body.sum),
		status: 1
	};	
	
	if(body.photo)
		data.photo = body.photo;
		
	return User.findOne({_id: data.buyer}, function(success, user){
		if(success){
			Transaction.findOne({_id: data.transaction}, function(success, transaction){
				if(success){
					
					data.seller = new ObjectID(transaction.user);
					data.publication = new ObjectID(transaction.publication);
					
					if(!data.buyer.equals(transaction.buyer))
						return cb ? cb(false, "No puedes comprar tu propia publicación") : false;
				
					return collection.insert(data, function(err, result){
						if(err){
							console.log(err);
							return cb ? cb(false, "Error al registrar transacción") : false;
						}else{
							return cb ? cb(true, new ObjectID(result.insertedIds[0])) : false;
						}
					});
				
				}else
					return cb ? cb(false, transaction) : false;
			});
		}else
			return cb ? cb(false, user) : false;
	});
};

module.exports = PanaPago;



var Transaction = require(__dirname +'/transaction'),
	User = require(__dirname +'/user');

	User = new User();
	Transaction = new Transaction();

