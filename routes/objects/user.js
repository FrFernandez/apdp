var db = require('../utils/db'),
	collection = db.collection("users"),
	ObjectID = require('mongoskin').ObjectID,
	method = User.prototype,
	fileUtils = require('../utils/fileUtils'),
	bcrypt = require('bcrypt'),
	SALT_WORK_FACTOR = 10,
	mailer = require('../utils/mailer'),
	uuid = require('node-uuid'),
	Hashids = require("hashids"),
    hashids = new Hashids("apreciodepana");

function User(){
	
}

method.getSalt = function(cb){
	bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt) {
        if(err) {
        	console.error(err);
			return cb ? cb(false) : false;
   		}
		return cb ? cb(salt) : false;
	});	
};

method.hashPass = function(salt,pass,cb){
	bcrypt.hash(pass, salt, function(err, hash) {
		if(err) {
			console.error(err);
			return cb ? cb(false) : false;
		}else
			return cb ? cb(hash) : false;
	});
};

method.compareHashPass = function(hash,pass,cb){
	bcrypt.compare(pass, hash, function(err, isMatch) {
		if(err) {
			console.error(err);
			return cb ? cb(false) : false;
		}else
			return cb ? cb(isMatch) : false;
    });
};

method.getBitacora = function(days){
	var daysSum = days || 30;
	daysSum = daysSum * 24 * 60 * 60 * 1000;
	
	return {
		token : hashids.encode(Date.now()),
		tokenSecret : uuid.v4(),
		expires: Date.now() + daysSum,
        active: true
	};
};

method.isAdmin = function(user){
	return user.priv == 1;	
}

method.isMod = function(user){
	return user.priv == 2;	
}

method.isUser = function(user){
	return user.priv == 3;	
}

method.checkHash = function(token, tokenSecret, cb){
	if(token && tokenSecret){
		return collection.findOne({"bitacora.token":token,"bitacora.tokenSecret":tokenSecret,"bitacora.active":true},function(err,user){
			if (err){
				console.log(err);
				setTimeout(function(){
					return cb ? cb(false) : false;
				}, 2000);
			}else if(user){
				var delegate = false;
				for(var i = 0; i < user.bitacora.length; i++)
					if(user.bitacora[i].token == token && 
					   user.bitacora[i].tokenSecret == tokenSecret &&
					   user.bitacora[i].delegate){
						delegate = true;
						break;
					}
				user.delegate = delegate;
				this.update({_id: new ObjectID(user._id)},{"$set":{lastActivity:Date.now()}},function(){return;});
				return cb ? cb(user) : false;
			}else{
				setTimeout(function(){
					return cb ? cb(false) : false;
				}, 2000);
			}
		}.bind(this));
	}else{
		setTimeout(function(){
			return cb ? cb(false) : false;
		}, 2000);
	}
};

method.checkPassword = function(data, id, olduser, cb){
	if(data.password){
		this.getSalt(function(salt){
			if(salt){
				this.hashPass(salt, data.password, function(password){
					if(password){
						data.password = password;
						data.bitacora = [this.getBitacora()];
						return this.update({_id:id}, {"$set":data}, {}, function(success, mes){
							if(success){
								if(data.photo)
									fileUtils.deleteFile("routes/uploads/husk/"+olduser.photo);
								if(data.cover)
									fileUtils.deleteFile("routes/uploads/husk/"+olduser.cover);
								return cb ? cb(true, data.bitacora[0]) : false;
							}else{
								if(data.photo)
									fileUtils.deleteFile("routes/uploads/husk/"+data.photo);
								if(data.cover)
									fileUtils.deleteFile("routes/uploads/husk/"+data.cover);
								return cb ? cb(false, mes) : false;
							}
						});
					}else{
						return cb ? cb(false, "Error al encriptar contraseña") : false;
					}
				}.bind(this));
			}else{
				if(data.photo)
					fileUtils.deleteFile("routes/uploads/husk/"+data.photo);
				if(data.cover)
					fileUtils.deleteFile("routes/uploads/husk/"+data.cover);
				return cb ? cb(false, "Error al encriptar contraseña") : false;
			}
		}.bind(this));
	}else
		return this.update({_id:id}, {"$set":data}, {}, function(success, mes){
			if(success){
				if(data.photo)
					fileUtils.deleteFile("routes/uploads/husk/"+olduser.photo);
				return cb ? cb(true) : false;
			}else{
				if(data.photo)
					fileUtils.deleteFile("routes/uploads/husk/"+data.photo);
				if(data.cover)
					fileUtils.deleteFile("routes/uploads/husk/"+data.cover);
				return cb ? cb(false, mes) : false;
			}
		});
};

method.checkFacebook = function(data, id, olduser, cb){
	if(data.facebook && data.facebook.id){
		return this.findOne({"facebook.id": data.facebook.id, _id: {"$ne": id}}, function(success){
			if(success){
				if(data.photo)
					fileUtils.deleteFile("routes/uploads/husk/"+data.photo);
				if(data.cover)
					fileUtils.deleteFile("routes/uploads/husk/"+data.cover);
				return cb ? cb(false, "Ya existe otro pana con este facebook") : false;
			}else{
				return this.checkPassword(data, id, olduser, cb);
			}
		}.bind(this));
	}else
		return this.checkPassword(data, id, olduser, cb);
};

method.checkTwitter = function(data, id, olduser, cb){
	if(data.twitter && data.twitter.id){
		return this.findOne({"twitter.id": data.twitter.id, _id: {"$ne": id}}, function(success){
			if(success){
				if(data.photo)
					fileUtils.deleteFile("routes/uploads/husk/"+data.photo);
				if(data.cover)
					fileUtils.deleteFile("routes/uploads/husk/"+data.cover);
				return cb ? cb(false, "Ya existe otro pana con este twitter") : false;
			}else{
				return this.checkFacebook(data, id, olduser, cb);
			}
		}.bind(this));
	}else
		return this.checkFacebook(data, id, olduser, cb);
};

method.checkMail = function(data, id, olduser, cb){
	if(data.mail){
		return this.findOne({mail: data.mail, _id: {"$ne": id}}, function(success){
			if(success){
				if(data.photo)
					fileUtils.deleteFile("routes/uploads/husk/"+data.photo);
				if(data.cover)
					fileUtils.deleteFile("routes/uploads/husk/"+data.cover);
				return cb ? cb(false, "Ya existe otro pana con este nombre de E-mail") : false;
			}else{
				return this.checkTwitter(data, id, olduser, cb);
			}
		}.bind(this));
	}else
		return this.checkTwitter(data, id, olduser, cb);
};

method.checkUsername = function(data, id, olduser, cb){
	if(data.username){
		return this.findOne({username: data.username, _id: {"$ne": id}}, function(success){
			if(success){
				if(data.photo)
					fileUtils.deleteFile("routes/uploads/husk/"+data.photo);
				if(data.cover)
					fileUtils.deleteFile("routes/uploads/husk/"+data.cover);
				return cb ? cb(false, "Ya existe otro pana con este nombre de usuario") : false;
			}else{
				return this.checkMail(data, id, olduser, cb);
			}
		}.bind(this));
	}else
		return this.checkMail(data, id, olduser, cb);
};

method.checkLocality = function(data, id, olduser, cb){
	if(data.localities){
		return Locality.validateArray(data.localities, function(success, mes){
			if(success){
				return this.checkUsername(data, id, olduser, cb);
			}else{
				if(data.photo)
					fileUtils.deleteFile("routes/uploads/husk/"+data.photo);
				if(data.cover)
					fileUtils.deleteFile("routes/uploads/husk/"+data.cover);
				return cb ? cb(false, mes) : false;
			}
		}.bind(this));
	}else
		return this.checkUsername(data, id, olduser, cb);
};

method.doUpdate = function(user, body, olduser, cb){
	var data = {};
	
	if(!ObjectID.isValid(olduser._id))
		return cb ? cb(false, "ID Invalido") : false;	
	id = new ObjectID(olduser._id);
	
	if(this.isAdmin(user) && body.priv)
		data.priv = parseInt(body.priv);

	if(body.username)
		data.username = this.clean(body.username).toUpperCase();
		
	if(body.dob)
		data.dob = body.dob;
		
	if(body.gender && parseInt(body.gender) >= 1 && parseInt(body.gender) <= 2)
		data.gender = parseInt(body.gender);
	
	if(body.name)
		data.name = body.name;
		
	if(body.description)
		data.description = body.description;
			
	if(body.lastname)
		data.lastname = body.lastname;
				
	if(body.website)
		data.website = body.website;
		
	if(body.document)
		data.document = body.document;
		
	if(body.phone)
		data.phone = body.phone;
			
	if(body.address)
		data.address = body.address;
				
	if(body.sharedaddress)
		data.sharedaddress = body.sharedaddress == 'true';
		
	if(body.mail)
		data.mail = this.clean(body.mail);
		
	if(body.localities){
		if(Array.isArray(body.localities)){
			let locs = [];
			for(let key in body.localities)
				if(ObjectID.isValid(body.localities[key]))
					locs.push(new ObjectID(body.localities[key]));
			data.localities = locs;
		}else if(ObjectID.isValid(body.localities))
			data.localities = [new ObjectID(body.localities)];
	}
	
	
	if(body.password)
		data.password = body.password;
		
	if(body.photo && body.photo != olduser.photo)
		data.photo = body.photo;
		
	if(body.cover && body.cover != olduser.cover)
		data.cover = body.cover;
		
	if(body.facebook && body.facebook.id)
		data.facebook = body.facebook;
		
	if(body.twitter && body.twitter.id)
		data.twitter = body.twitter;
	
	if(this.isAdmin(user)){
		if(body.status)
			data.status = parseInt(body.status);
		
		if(body.priv)
			data.priv = parseInt(body.priv);
	}
	
	if(Object.keys(data).length > 0){
		return this.checkLocality(data, id, olduser, cb);
	}else
		return cb ? cb(false, "Debes enviar al menos un campo para actualizar") : false;	
};

method.update = function(query, order, extra, cb){
	collection.update(query, order, extra, function(err){
		if(err){
			console.log(err);
			return cb ? cb(false, "Error al actualizar") : false;
		}else{
			return cb ? cb(true) : false;
		}
	});
};

method.remove = function(filter, cb){
	this.find(filter, false, false, false, function(items){
		if(items){
			var handleLoop = function(i){
				if(items[i].photo)
					fileUtils.deleteFile("routes/uploads/husk/"+items[i].photo);
			}
			
			for(var i = 0 ; i < items.length ; i++)
				handleLoop(i);
			
			collection.remove(filter, function(err){
				if(err){
					console.log(err);
					return cb ? cb(false, "Error al eliminar") : false;
				}else
					return cb ? cb(true) : false;
			});
		}else
			return cb ? cb(false, "Nada por eliminar") : false;
	});
	return;
};

method.count = function(filter, cb){
	var find = this.prepareFind(filter);
	
	return collection.count(find, function(err, count){
		if(err){
			console.log(err);
			return cb ? cb(false, "Error al conseguir total de busqueda") : false;
		}else
			return cb ? cb(true, count) : false;
	});
};

method.dateToTime = function(date){
	date=date.split("-");
	return new Date(date[2]+"-"+date[1]+"-"+date[0]).getTime();
};

method.prepareFind = function(body){
	var find = {};
	
	if(ObjectID.isValid(body._id))
		find._id = new ObjectID(body._id);
	
	if(body.name)
		find.name = {"$regex":new RegExp(this.clean(body.name),'i')};
	
	if(body.mail)
		find.mail = {"$regex":new RegExp(body.mail,'i')};
	
	if(body.username)
		find.username = {"$regex":new RegExp(this.clean(body.username),'i')};
	
	if(body.lastname)
		find.lastname = {"$regex":new RegExp(body.lastname,'i')};
	
	if(body.phone)
		find.phone = {"$regex":new RegExp(body.phone,'i')};
	
	if(body.description)
		find.description = {"$regex":new RegExp(body.description,'i')};
	
	if(body.type)
		find.type = parseInt(body.type);
	
	if(body.priv)
		find.priv = parseInt(body.priv);
	
	if(body.document)
		find.document = body.document;
		
	if(body.localities){
		if(Array.isArray(body.localities)){
			let locs = [];
			for(let key in body.localities)
				if(ObjectID.isValid(body.localities[key]))
					locs.push(new ObjectID(body.localities[key]));
			find.localities = locs;
		}else if(ObjectID.isValid(body.localities))
			find.localities = [new ObjectID(body.localities)];
	}
	
	if(body["facebook.id"])
		find.facebook = {id: body["facebook.id"]};
	
	if(body["twitter.id"])
		find.twitter = {id: body["twitter.id"]};
	
	if(body.joinedstart){
		if(!find.joined) find.joined = {};
		find.joined["$gte"] = this.dateToTime(body.joinedstart);
	}
	
	if(body.joinedend){
		if(!find.joined) find.joined = {};
		find.joined["$lte"] = this.dateToTime(body.joinedend);
	}
	
	if(body.onlinestart){
		if(!find.lastactivity) find.lastactivity = {};
		find.lastactivity["$gte"] = this.dateToTime(body.onlinestart);
	}
	
	if(body.onlineend){
		if(!find.lastactivity) find.lastactivity = {};
		find.lastactivity["$lte"] = this.dateToTime(body.onlineend);
	}
	return find;
};

method.find = function(body, order, skip, limit, cb){
	if(typeof order == 'function'){
		cb = order;
		order = false;
	}
	
	var find = this.prepareFind(body),
		promise = collection.find(find);
		
	if(order)
		promise.sort(order);
	if(skip)
		promise.skip(skip);
	if(limit)
		promise.limit(limit);
		
	promise.toArray(function(err, users){
		if(err){
			console.log(err);
			return cb ? cb(false, "Error al ejecutar busqueda") : false;
		}else if(users) return cb ? cb(true, users) : false;
		else return cb ? cb(false, "Usuarios no encontrados") : false;
	});
};

method.findOne = function(filter, cb){
	collection.findOne(filter, function(err, user){
		if(err){
			console.log(err);
			return cb ? cb(false, "Error al encontrar usuario") : false;
		}else if(user)
			return cb ? cb(true, user) : false;
		else return cb ? cb(false, "Usuario no encontrado") : false;
	});
};

method.getMultiFull = function(body, order, skip, limit, cb){
	if(typeof order == 'function'){
		cb = order;
		order = false;
	}
	
	var find = this.prepareFind(body),
		promise = collection.find(find);
		
	if(order)
		promise.sort(order);
	if(skip)
		promise.skip(skip);
	if(limit)
		promise.limit(limit);
		
	promise.toArray(function(err, users){
		if(err){
			return cb ? cb(false, "Error al ejecutar busqueda") : false;
		}else if(users){
			var il = users.length, round = 1, i = 0, total = 0, newUsers = [];
			var handleLoop = function(i){
				this.getFull(users[i], function(success, user){
					if(success)
						newUsers[i] = user;
					else
						newUsers[i] = users[i];
					
					if(round == il)
						return cb ? cb(true, newUsers) : false;
					round++;
					return;
				}.bind(this));
				return;
			}.bind(this);
			
			if(il>0)
				for(;i<il;i++)
					handleLoop(i);
			else
				return cb? cb(true, users) : false;
		}
		else return cb ? cb(false, "Usuarios no encontrados") : false;
	}.bind(this));
};

method.proccess = function(user, cb){
	delete user.bitacora;
	delete user.password;
	delete user.devices;
	return Locality.validateArray(user.localities, function(success, localities){
		if(success)
			user.localities = localities;
		
		return Follow.count({follower: new ObjectID(user._id)}, function(success, follows){
			if(success)
				user.follows = follows;
			return Follow.count({followed: new ObjectID(user._id)}, function(success, followers){
				if(success)
					user.followers = followers;
				return Friendship.count({users: new ObjectID(user._id)}, function(success, friends){
					if(success)
						user.friends = friends;
					return cb ? cb(true, user) : false;
				});
			});
		});
	});
};

method.getFull = function(id, cb){
	if(ObjectID.isValid(id)){
		id = new ObjectID(id);
		return this.findOne({_id:id}, function(success, user){
			if(success){
				return this.proccess(user, cb);
			}else
				return cb ? cb(false, "Usuario no encontrado") : false;
		}.bind(this));
	}else if(id !== null && typeof id === 'object')
		return this.proccess(id, cb);
	else
		return cb ? cb(false, "ID Invalido") : false;
};

method.str_replace = function($f, $r, $s){
    return $s.replace(new RegExp("(" + (typeof($f) == "string" ? $f.replace(/[.?*+^$[\]\\(){}|-]/g, "\\$&") : $f.map(function(i){return i.replace(/[.?*+^$[\]\\(){}|-]/g, "\\$&")}).join("|")) + ")", "g"), typeof($r) == "string" ? $r : typeof($f) == "string" ? $r[0] : function(i){ return $r[$f.indexOf(i)]});
};

method.clean = function(name){
	var f=[' ','Š','Œ','Ž','š','œ','ž','Ÿ','¥','µ','À','Á','Â','Ã','Ä','Å','Æ','Ç','È','É','Ê','Ë','?','Ì','Í','Î','Ï','I','Ð','Ñ','Ò','Ó','Ô','Õ','Ö','Ø','Ù','Ú','Û','Ü','Ý','ß','à','á','â','ã','ä','å','æ','ç','è','é','ê','ë','?','ì','í','î','ï','i','ð','ñ','ò','ó','ô','õ','ö','ø','ù','ú','û','ü','ý','ÿ'],
		r=['','S','O','Z','s','o','z','Y','Y','u','A','A','A','A','A','A','A','C','E','E','E','E','E','I','I','I','I','I','D','N','O','O','O','O','O','O','U','U','U','U','Y','s','a','a','a','a','a','a','a','c','e','e','e','e','e','i','i','i','i','i','o','n','o','o','o','o','o','o','u','u','u','u','y','y'];
	return this.str_replace(f,r,name).toLowerCase();		
};

method.insert = function(body, cb){
	if(!body.username)
		return cb ? cb(false, "Debes enviar un nombre de usuario") : false;
		
	if(!body.name)
		return cb ? cb(false, "Debes enviar tu nombre") : false;
		
	if(!body.photo)
		return cb ? cb(false, "Debes enviar tu foto") : false;
			
	if(!body.lastname)
		return cb ? cb(false, "Debes enviar tu apellido") : false;
		
	if(!body.document)
		return cb ? cb(false, "Debes enviar tu documento de identificación") : false;
		
	if(!body.phone)
		return cb ? cb(false, "Debes enviar tu número de teléfono") : false;
			
	if(!body.address)
		return cb ? cb(false, "Debes enviar tu dirección") : false;
		
	if(!body.mail)
		return cb ? cb(false, "Debes enviar tu E-Mail") : false;
		
	if(!body.localities)
		return cb ? cb(false, "Debes enviar tu ubicación") : false;
		
	if(!body.dob)
		return cb ? cb(false, "Debes enviar tu fecha de nacimiento") : false;
		
	if(!body.gender || parseInt(body.gender) < 1 || parseInt(body.gender) > 2)
		return cb ? cb(false, "Debes enviar tu genero") : false;
	
	if(!body.password || body.password.length < 8)
		return cb ? cb(false, "Debes enviar una contraseña de 8 o más caracteres") : false;
	/*
	if(!body.facebook && !body.twitter)
		return cb ? cb(false, "Debes vincular al menos una red social") : false;
	*/
	if(Array.isArray(body.localities)){
		let locs = [];
		for(let key in body.localities)
			if(ObjectID.isValid(body.localities[key]))
				locs.push(new ObjectID(body.localities[key]));
		body.localities = locs;
	}else if(ObjectID.isValid(body.localities))
		body.localities = [new ObjectID(body.localities)];
		
	var data = {
		username: this.clean(body.username).toUpperCase(),
		name: body.name,
		photo: body.photo,
		cover: body.cover || '',
		description: '',
		dob: body.dob,
		gender: body.gender,
		facebook: body.facebook || false,
		twitter: body.twitter || false,
		website: body.website,
		lastname: body.lastname,
		document: body.document,
		phone: body.phone,
		address: body.address,
		sharedaddress: body.sharedaddress == 'true',
		mail: body.mail.toLowerCase(),
		password: body.password,
		localities: body.localities,
		bitacora: [],
		devices: [],
		priv: parseInt(body.priv),
		joined: Date.now(),
		lastactivity: 0,
		valid: true,
		status: 1,
		validateToken: hashids.encode(Date.now()),
		rating : {
			seller: {
				total: 0,
				totalTallied: 0,
				tallied: {
					zero: 0,
					one: 0,
					two: 0
				}
			},
			buyer: {
				total: 0,
				totalTallied: 0,
				tallied: {
					zero: 0,
					one: 0,
					two: 0
				}
			}
		}
	};	
		
	return this.findOne({mail:data.mail}, function(success){
		if(success)
			return cb ? cb(false, "Este correo ya esta registrado") : false;
		else
			return this.findOne({username: data.username}, function(success){
				if(success)
					return cb ? cb(false, "Este usuario ya esta registrado") : false;
				else
					return Locality.validateArray(data.localities, function(success, mes){
						if(success){
							return this.getSalt(function(salt){
								if(salt){
									return this.hashPass(salt, data.password, function(password){
										if(password){
											data.password = password;
											return collection.insert(data, function(err, result){
												if(err){
													console.log(err);
													fileUtils.deleteFile("routes/uploads/husk/"+data.photo);
													if(data.cover)
														fileUtils.deleteFile("routes/uploads/husk/"+data.cover);
													return cb ? cb(false, "Error al agregar usuario") : false;
												}else{
													return cb ? cb(true, data.validateToken) : false;
												}
											}.bind(this));
										}else{
											fileUtils.deleteFile("routes/uploads/husk/"+data.photo);
											if(data.cover)
												fileUtils.deleteFile("routes/uploads/husk/"+data.cover);
											return cb ? cb(false, "Error al encriptar contraseña") : false;
										}
									}.bind(this));
								}else{
									fileUtils.deleteFile("routes/uploads/husk/"+data.photo);
									if(data.cover)
										fileUtils.deleteFile("routes/uploads/husk/"+data.cover);
									return cb ? cb(false, "Error al encriptar contraseña") : false;
								}
							}.bind(this));
						}else{
							fileUtils.deleteFile("routes/uploads/husk/"+data.photo);
							if(data.cover)
								fileUtils.deleteFile("routes/uploads/husk/"+data.cover);
							return cb ? cb(false, mes) : false;
						}
					}.bind(this));
			}.bind(this));
	}.bind(this));
};

module.exports = User;

var Locality = require(__dirname +'/locality'),
	Friendship = require(__dirname +'/friendship'),
	Follow = require(__dirname +'/follow');
	
	Locality = new Locality();
	Friendship = new Friendship();
	Follow = new Follow();