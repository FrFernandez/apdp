var db = require('../utils/db'),
	collection = db.collection("reviews"),
	ObjectID = require('mongoskin').ObjectID,
	method = Review.prototype,
	Transaction = require(__dirname +'/transaction'),
	User = require(__dirname +'/user');

	User = new User();
	Transaction = new Transaction();

function Review(){
	
}

method.count = function(filter, cb){
	filter = this.preparefind(filter);
	return collection.count(filter, function(err, count){
		if(err){
			console.log(err);
			return cb ? cb(false, "Error al conseguir total de busqueda") : false;
		}else
			return cb ? cb(true, count) : false;
		return;
	});
};

method.find = function(filter, order, skip, limit, cb){
	if(typeof order == 'function'){
		cb = order;
		order = false;
	}
	
	filter = this.preparefind(filter);
	var promise = collection.find(filter);
	if(order)
		promise.sort(order);
	if(skip)
		promise.skip(skip);
	if(limit)
		promise.limit(limit);
		
	return promise.toArray(function(err, reviews){
		if(err){
			console.log(err);
			return cb ? cb(false, "Error al ejecutar busqueda") : false;
		}else if(reviews) return cb ? cb(true, reviews) : false;
		else return cb ? cb(false, "Transacciones no encontradas") : false;
	});
};

method.findOne = function(filter, cb){
	return collection.findOne(filter, function(err, user){
		if(err){
			console.log(err);
			return cb ? cb(false, "Error al encontrar usuario") : false;
		}else if(user)
			return cb ? cb(true, user) : false;
		else return cb ? cb(false, "Usuario no encontrado") : false;
	});
};

method.getMultiFull = function(filter, order, skip, limit, cb){
	if(typeof order == 'function'){
		cb = order;
		order = false;
	}
	
	filter = this.preparefind(filter);
	var promise = collection.find(filter);
	if(order)
		promise.sort(order);
	if(skip)
		promise.skip(skip);
	if(limit)
		promise.limit(limit);
		
	return promise.toArray(function(err, items){
		if(err){
			return cb ? cb(false, "Error al ejecutar busqueda") : false;
		}else if(items){
			var il = items.length, round = 1, i = 0, total = 0, newitems = [];
			var handleLoop = function(i){
				this.getFull(items[i], function(success, user){
					if(success)
						newitems[i] = user;
					else
						newitems[i] = items[i];
					
					if(round == il)
						return cb ? cb(true, newitems) : false;
					round++;
					return;
				}.bind(this));
				return;
			}.bind(this);
			
			if(il>0)
				for(;i<il;i++)
					handleLoop(i);
			else
				return cb? cb(true, items) : false;
		}
		else return cb ? cb(false, "Usuarios no encontrados") : false;
	}.bind(this));
};

method.proccess = function(item, cb){
	return User.getFull({_id: new ObjectID(item.to)}, function(success, user){
		if(success)
			item.to = user;
			
		return User.getFull({_id: new ObjectID(item.by)}, function(success, user){
			if(success)
				item.by = user;
			
			return Transaction.getFull({_id: new ObjectID(item.transaction)}, function(success, transaction){
				if(success)
					item.transaction = transaction;
			
				return cb ? cb(true, user) : false;
			});
		});
	});
};

method.getFull = function(id, cb){
	if(ObjectID.isValid(id)){
		id = new ObjectID(id);
		return this.findOne({_id:id}, function(success, item){
			if(success){
				return this.proccess(item, cb);
			}else
				return cb ? cb(false, "Cuenta Bancaria no encontrada") : false;
		}.bind(this));
	}else if(id !== null && typeof id === 'object')
		return this.proccess(id, cb);
	else
		return cb ? cb(false, "ID Invalido") : false;
};

method.preparefind = function(body){
	var find = {};

	if(body._id && ObjectID.isValid(body._id))
		find._id = new ObjectID(body._id);
	
	if(body.transaction && ObjectID.isValid(body.transaction))
		find.transaction = new ObjectID(body.transaction);
	
	if(body.by && ObjectID.isValid(body.by))
		find.by = new ObjectID(body.by);
	
	if(body.to && ObjectID.isValid(body.to))
		find.to = new ObjectID(body.to);
	
	if(body.rate)
		find.rate = parseInt(body.rate);
	
	if(body.publication && ObjectID.isValid(body.publication))
		find.publication = new ObjectID(body.publication);
	
	if(body.mintime){
		if(!find.time) find.time = {}
		find.time["$gte"] = this.dateToTime(body.mintime);
	}
	
	if(body.maxtime){
		if(!find.time) find.time = {}
		find.time["$lte"] = this.dateToTime(body.maxtime);
	}
	return find;
};

method.insert = function(body, cb){
	
	if(!body.transaction || !ObjectID.isValid(body.transaction))
		return cb ? cb(false, "Debes enviar a que transacción pertenece esta reseña") : false;
	
	if(!body.by || !ObjectID.isValid(body.by))
		return cb ? cb(false, "Debes enviar el comprador esta transacción") : false;
		
	if(!body.comment)
		return cb ? cb(false, "Debes enviar un comentario asociado a esta reseña") : false;
		
	if(!body.rate || parseInt(body.rate) < 0 || parseInt(body.rate) > 2)
		return cb ? cb(false, "Debes enviar un puntaje valido") : false;
	
	var data = {
		transaction: new ObjectID(body.transaction),
		comment: comment,
		rate: parseInt(body.rate),
		time: Date.now(),
		by: new ObjectID(body.by)
	};	
	
	return User.findOne({_id: data.by}, function(success, user){
		if(success){
			Transaction.findOne({_id: data.transaction}, function(success, transaction){
				if(success){
				
					var buyerRatingSeller = true;
				
					if(new ObjectID(transaction.buyer).equals(data.by)){
						if(transaction.statusb !== 1)
							return cb ? cb(false, "Ya has cerrado esta transacción") : false;
					
						data.to = new ObjectID(transaction.seller);
						data.publication = new ObjectID(transaction.publication);
					}else if(new ObjectID(transaction.seller).equals(data.by)){
						if(transaction.statuss !== 1)
							return cb ? cb(false, "Ya has cerrado esta transacción") : false;
					
						buyerRatingSeller = false;
						data.to = new ObjectID(transaction.buyer);
					}else
						return cb ? cb(false, "No puedes clasificar esta transacción") : false;
						
						
					return User.findOne({_id: data.to}, function(success, user){
						if(success){
							var rt, set, tset;
							
							if(buyerRatingSeller){
								tset = {"$set": {statusb: 2 }};
								rt = user.rating.seller;
							}else{
								tset = {"$set": {statuss: 2}};
								rt = user.rating.buyer;
							}
							rt.totalTallied++;
							switch(data.rate){
								case 0:
									rt.tallied.zero++;
									break;
								case 1:
									rt.tallied.one++;
									break;
								case 2:
									rt.tallied.two++;
									break;
							}	
							
							rt.total = Math.round((( (rt.tallied.one) + (rt.tallied.two * 2)) / rt.totalTallied) * 10) / 10;	
							
							if(buyerRatingSeller)
								set = {"$set":{"user.rating.seller": rt}};
							else
								set = {"$set":{"user.rating.buyer": rt}};
						
							return User.update({_id: data.to}, set, {}, function(success, mes){
								if(success){
									return Transaction.update({_id: data.transaction}, tset, {}, function(success, mes){
										if(success){
											return collection.insert(data, function(err, result){
												if(err){
													console.log(err);
													return cb ? cb(false, "Error al agregar Reseña") : false;
												}else{
													return cb ? cb(true) : false;
												}
											});
										}else
											return cb ? cb(false, "Error al actualizar Transacción") : false;
									});
								}else
									return cb ? cb(false, "Error al clasificar usuario") : false;
							});
						
						}else
							return cb ? cb(false, user) : false;
					});
				}else
					return cb ? cb(false, publication) : false;
			});
		}else
			return cb ? cb(false, user) : false;
	});
};

module.exports = Review;