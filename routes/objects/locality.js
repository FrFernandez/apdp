var db = require('../utils/db'),
	collection = db.collection("localities"),
	area = require('area-polygon'),
	ObjectID = require('mongoskin').ObjectID,
	method = Locality.prototype;

function Locality(){	
}

method.findSmallest = function(items){
	for(let key in items){
		items[k].geometry.coordinates[0].pop();
		items[k].area = area(items[k].geometry.coordinates[0]);
	}
	
	let sortable = items.splice(0);
	sortable.sort(function(a,b) {
		var x = a.area;
		var y = b.area;
		return x < y ? -1 : x > y ? 1 : 0;
	});
	
	return sortable[0];
};

method.doUpdate = function(id, body, cb){
	var data = {};
	
	if(body.name)
		data.name = body.name;
	
	if(ObjectID.isValid(body.parent))
		data.parent = new ObjectID(body.parent);
		
	if(body.coordinates && Array.isArray(body.coordinates)){
		let newCoordinates = [], calcA = 0;
		
		for(let i = 0; i < body.coordinates.length; i++){
			newCoordinates.push([body.coordinates[i].lng, body.coordinates[i].lat]);
		}
		data.area = area(newCoordinates);
		
		newCoordinates.push([coordinates[0].lng,coordinates[0].lat]);
		data.geometry = {
			type:'Polygon',
			coordinates:[newCoordinates]
		};
	}
	if(Object.keys(data).length > 0){
		if(data.parent)
			return this.findOne({_id: data.parent}, function(success, mes){
				if(success)
					return this.update({_id: id}, {"$set":data}, {}, function(success, mes){
						if(success)
							return cb ? cb(true) : false;
						else
							return cb ? cb(false, mes) : false;
					});
				else
					return cb ? cb(false, "No se ha encontrado la localidad padre") : false;
			}.bind(this));
		else
			return this.update({_id: id}, {"$set":data}, {}, function(success, mes){
				if(success)
					return cb ? cb(true) : false;
				else
					return cb ? cb(false, mes) : false;
			});
	}else
		return cb ? cb(false, "Debes enviar al menos un campo para actualizar") : false;	
};

method.validateArray = function(items, cb){
	var round = 1, cl = items.length, bodies = [], failed = 0;
	let loop = function(i){
		if(ObjectID.isValid(items[i])){
			items[i] = new ObjectID(items[i]); 
			let find = {_id: new ObjectID(items[i])};
			if(i>0)
				find.parent = new ObjectID(items[i-1]);
			this.findOne(find, function(success, mes){
				if(success){
					bodies[i] = mes;
					if(round == cl){
						if(failed == 0)
							return cb ? cb(true, bodies) : false;
						else
							return cb ? cb(false, "Error al validar una o mas localidades") : false;
					}
					round++;
				}else{
					if(round == cl){
						if(failed == 0)
							return cb ? cb(true, bodies) : false;
						else
							return cb ? cb(false, "Error al validar una o mas localidades") : false;
					}
					round++;
					failed++;
				}
				return;
			}.bind(this));
		}else{
			if(round == cl){
				if(failed == 0)
					return cb ? cb(true, bodies) : false;
				else
					return cb ? cb(false, "Error al validar uno o más localidades") : false;
			}
			round++;
			failed++
		}
	}.bind(this);
	
	if(cl > 0)
		for(let i in items){
			loop(i);
		}
	else
		return cb ? cb(false, "Debes enviar al menos una localidad") : false;
};

method.update = function(query, order, extra, cb){
	if(!extra)
		extra = {};

	collection.update(query, order, extra, function(err){
		if(err){
			console.log(err);
			return cb ? cb(false, "Error al actualizar") : false;
		}else{
			return cb ? cb(true) : false;
		}
	});
};

method.doRemove = function(id){
	id = new ObjectID(id);
	User.update({localities: id}, {"$pull": {localities:id}}, {multi: true});
	Publication.update({localities: id}, {"$pull": {localities:id}}, {multi: true});
	
	return this.find({parent: id}, function(success, items){
		if(success){
			while(items.length > 0)
				this.doRemove(items.pop()._id);
		}
	}.bind(this));
};

method.remove = function(filter, cb){
	this.find(filter, function(success, items){
		if(success){
			return collection.remove(filter, function(err){
				if(err){
					while(items.length>0)
						this.doRemove(items.pop()._id);
					return cb ? cb(false, "Error al eliminar") : false;
				}else
					return cb ? cb(true) : false;
			}.bind(this));
		}else
			return cb ? cb(false, "Nada for eliminar") : false;
	}.bind(this));
};

method.count = function(filter, cb){
	collection.count(filter, function(err, count){
		if(err){
			console.log(err);
			cb(false, "Error al conseguir total de busqueda");
		}else
			cb(true, count);
		return;
	});
	return;
};

method.find = function(filter, order, skip, limit, cb){
	
	if(typeof order == 'function'){
		cb = order;
		order = false;
	}
	
	var promise = collection.find(filter);
	if(order)
		promise.sort(order);
	if(skip)
		promise.skip(skip);
	if(limit)
		promise.limit(limit);
		
	promise.toArray(function(err, users){
		if(err){
			console.log(err);
			return cb ? cb(false) : false;
		}else if(users)
			return cb ? cb(true, users) : false;
		else
			return cb ? cb(false, "Localidad no encontrada") : false;
	});
};

method.findOne = function(filter, cb){
	collection.findOne(filter, function(err, user){
		if(err){
			console.log(err);
			return cb ? cb(false, "Error al encontrar") : false;
		}else if(user)
			return cb ? cb(true, user) : false;
		else return cb ? cb(false, "Localidad no encontrada") : false;
	});
};

method.insert = function(body, cb){
	if(!body.name)
		return cb ? cb(false, "Debes enviar todos los campos") : false;
	
	let newCoordinates = [], calcA = 0;
    if(Array.isArray(body.coordinates)){
		for(let i = 0; i < body.coordinates.length; i++){
			newCoordinates.push([parseFloat(body.coordinates[i].lng), parseFloat(body.coordinates[i].lat)]);
		}
		calcA = area(newCoordinates);
		
		newCoordinates.push([parseFloat(body.coordinates[0].lng), parseFloat(body.coordinates[0].lat)]);
	}else{
		newCoordinates.push([0,0]);
		newCoordinates.push([0.1,0.1]);	
		newCoordinates.push([-0.1,-0.1]);	
		newCoordinates.push([0,0]);
		calcA = 0.1;
	}
	
	
	let data = {
		name : body.name,
		parent: ObjectID.isValid(body.parent) ? new ObjectID(body.parent) : false,
		geometry:{
			type:'Polygon',
			coordinates:[newCoordinates]
		},
		area: calcA
	};
	
	if(isNaN(calcA) || calcA == 0)
		return cb ? cb(false, "Debes enviar un poligono valido") : false;

	if(data.parent){
			console.log("parent search");
		this.findOne({_id:data.parent}, function(success, loc){
			console.log("parent found");
			if(success){
				return collection.insert(data, function(err, result){
					if(err){
						console.log(err);
						return cb ? cb(false, "Error al insertar") : false;
					}else{
						console.log("inserted");
						return cb ? cb(true, new ObjectID(result.insertedIds[0])) : false;
					}
				});
			}else
				return cb ? cb(false, "Localidd padre no encontrada") : false;
		});
	}else{
		data.parent = false;
		return collection.insert(data, function(err, result){
			if(err){
				console.log(err);
				return cb ? cb(false, "Error al insertar") : false;
			}else{
				return cb ? cb(true, new ObjectID(result.insertedIds[0])) : false;
			}
		});
	}
};

module.exports = Locality;

var User = require(__dirname +'/user'),
	Publication = require(__dirname +'/publication');
	
	User = new User();
	Publication = new Publication();