var db = require('../utils/db'),
	collection = db.collection("comments"),
	ObjectID = require('mongoskin').ObjectID,
	method = Comment.prototype;

function Comment(){
	
}

method.update = function(query, order, extra, cb){
	return collection.update(query, order, extra, function(err){
		if(err){
			console.log(err);
			return cb ? cb(false, "Error al actualizar") : false;
		}else{
			return cb ? cb(true) : false;
		}
	});
};

method.doUpdate = function(id, body, cb){
	var data = {};
	
	if(!ObjectID.isValid(id))
		return cb ? cb(false, "ID Invalido") : false;	
	id = new ObjectID(id);
	
	if(body.comment)
		data.comment = body.comment;
		
	if(Object.keys(data).length > 0){
		return this.update({_id:id}, {"$set":data}, {}, function(success, mes){
			if(success){
				return cb ? cb(true, data.bitacora[0]) : false;
			}else{
				return cb ? cb(false, mes) : false;
			}
		});
	}else
		return cb ? cb(false, "Debes enviar al menos un campo para actualizar") : false;
};

method.remove = function(filter, cb){
	return collection.remove(filter, function(err){
		if(err){
			console.log(err);
			return cb ? cb(false, "Error al eliminar") : false;
		}else
			return cb ? cb(true) : false;
	});
};

method.count = function(filter, cb){
	return collection.count(filter, function(err, count){
		if(err){
			console.log(err);
			return cb ? cb(false, "Error al conseguir total de busqueda") : false;
		}else
			return cb ? cb(true, count) : false;
		return;
	});
};

method.find = function(filter, order, skip, limit, cb){
	if(typeof order == 'function'){
		cb = order;
		order = false;
	}
	
	var promise = collection.find(filter);
	if(order)
		promise.sort(order);
	if(skip)
		promise.skip(skip);
	if(limit)
		promise.limit(limit);
		
	return promise.toArray(function(err, comments){
		if(err){
			console.log(err);
			return cb ? cb(false, "Error al ejecutar busqueda") : false;
		}else if(comments) return cb ? cb(true, comments) : false;
		else return cb ? cb(false, "Transacciones no encontradas") : false;
	});
};

method.findOne = function(filter, cb){
	return collection.findOne(filter, function(err, user){
		if(err){
			console.log(err);
			return cb ? cb(false, "Error al encontrar usuario") : false;
		}else if(user)
			return cb ? cb(true, user) : false;
		else return cb ? cb(false, "Usuario no encontrado") : false;
	});
};

method.getMultiFull = function(filter, order, skip, limit, cb){
	if(typeof order == 'function'){
		cb = order;
		order = false;
	}
	
	var promise = collection.find(filter);
	if(order)
		promise.sort(order);
	if(skip)
		promise.skip(skip);
	if(limit)
		promise.limit(limit);
		
	return promise.toArray(function(err, items){
		if(err){
			return cb ? cb(false, "Error al ejecutar busqueda") : false;
		}else if(items){
			var il = items.length, round = 1, i = 0, total = 0, newitems = [];
			var handleLoop = function(i){
				this.getFull(items[i], function(success, user){
					if(success)
						newitems[i] = user;
					else
						newitems[i] = items[i];
					
					if(round == il)
						return cb ? cb(true, newitems) : false;
					round++;
					return;
				}.bind(this));
				return;
			}.bind(this);
			
			if(il>0)
				for(;i<il;i++)
					handleLoop(i);
			else
				return cb? cb(true, items) : false;
		}
		else return cb ? cb(false, "Usuarios no encontrados") : false;
	}.bind(this));
};

method.proccess = function(item, cb){
	return Publication.getFull(new ObjectID(item.publication), function(success, publication){
		
		if(success)
			item.publication = publication;
		
		return User.getFull(new ObjectID(item.user), function(success, user){
			if(success)
				item.user = user;
				
			if(item.parent === false)
				return this.getMultiFull({parent: new ObjectID(item._id)}, function(success, comments){
					if(success)
						item.comments = comments;
					else
						item.comments = [];
						
					return cb ? cb(true, item) : false;
				});
			else
				return cb ? cb(true, item) : false;
		}.bind(this));
	}.bind(this));
};

method.getFull = function(id, cb){
	if(ObjectID.isValid(id)){
		id = new ObjectID(id);
		return this.findOne({_id:id}, function(success, item){
			if(success){
				return this.proccess(item, cb);
			}else
				return cb ? cb(false, "Cuenta Bancaria no encontrada") : false;
		}.bind(this));
	}else if(id !== null && typeof id === 'object')
		return this.proccess(id, cb);
	else
		return cb ? cb(false, "ID Invalido") : false;
};

method.insert = function(body, cb){
	
	if(!body.publication || !ObjectID.isValid(body.publication))
		return cb ? cb(false, "Debes enviar a que publicación pertenece esta comentario") : false;
	
	if(!body.user || !ObjectID.isValid(body.user))
		return cb ? cb(false, "Debes enviar el usuario que comenta") : false;
	
	if(!body.comment)
		return cb ? cb(false, "Debes enviar el comentario") : false;	
		
	var data = {
		publication: new ObjectID(body.publication),
		user: new ObjectID(body.user),
		comment: body.comment,
		status: 1,
		time: Date.now()
	};	
	
		
	return User.findOne({_id: data.user}, function(success, user){
		if(success){
			Publication.findOne({_id: data.publication, status:1}, function(success, publication){
				if(success){
					if(User.isAdmin(user))
						data.bytype = 4;
					else if(User.isMod(user))
						data.bytype = 3;
					else if(data.user.equals(new ObjectID(publication.user)))
						data.bytype = 2;
					else{
						data.by = 1;
					}
					
					if(!data.user.equals(new ObjectID(publication.user))){
						data.to = new ObjectID(publication.user);
					}
					
					if(ObjectID.isValid(body.parent)){
						data.parent = new ObjectID(body.parent);
						return this.findOne({_id: data.parent, parent: false, publication: data.publication}, function(success, mes){
							if(success){
								
								this.update({_id: data.parent},{"$set":{answered: data.user.equals(new ObjectID(publication.user))}});
								
								return collection.insert(data, function(err, result){
									if(err){
										console.log(err);
										return cb ? cb(false, "Error al registrar comentario") : false;
									}else{
										if(!new ObjectID(mes.user).equals(data.user))
											Notification.insert({
												subject: "Han respondido tu comentario",
												message: user.username+" ha respondido tu comentario",
												type: "comment",
												meta: new ObjectID(data.publication),
												user: new ObjectID(mes.user)
											});
										else if(!data.user.equals(new ObjectID(publication.user)))
											Notification.insert({
												subject: "Han comentado tu publicación",
												message: user.username+" ha comentado en tu publicación",
												type: "comment",
												meta: new ObjectID(data.publication),
												user: new ObjectID(publication.user)
											});
										return cb ? cb(true, new ObjectID(result.insertedIds[0])) : false;
									}
								});
							}else
								return cb ? cb(false, "Comentario padre no encontrado") : false;
						});
					}else{
						data.parent = false;
						if(!data.user.equals(new ObjectID(publication.user))){
							this.update({_id: data.parent},{"$set":{answered: true}});
						}
						
						return collection.insert(data, function(err, result){
							if(err){
								console.log(err);
								return cb ? cb(false, "Error al registrar comentario") : false;
							}else{
								Notification.insert({
									subject: "Han comentado tu publicación",
									message: user.username+" ha comentado en tu publicación",
									type: "comment",
									meta: new ObjectID(data.publication),
									user: new ObjectID(publication.user)
								});
							
								return cb ? cb(true, new ObjectID(result.insertedIds[0])) : false;
							}
						});
					}
				
				}else
					return cb ? cb(false, publication) : false;
			}.bind(this));
		}else
			return cb ? cb(false, user) : false;
	}.bind(this));
};

module.exports = Comment;



var	Publication = require(__dirname +'/publication'),
	Notification = require(__dirname +'/notification'),
	User = require(__dirname +'/user');

	User = new User();
	Publication = new Publication();
	Notification = new Notification();
