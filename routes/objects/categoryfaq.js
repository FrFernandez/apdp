var db = require('../utils/db'),
	collection = db.collection("categoryfaq"),
	ObjectID = require('mongoskin').ObjectID,
	method = CategoryFaq.prototype,
	fileUtils = require('../utils/fileUtils');

function CategoryFaq(){
}

method.doUpdate = function(id, body, cb){
	var data = {};
	
	if(body.name){
		data.name = body.name;
		data.searchName = this.clean(data.name);
	}

	if(body.sinonims)
		data.sinonims = this.clean(body.sinonims);
		
	if(ObjectID.isValid(body.parent))
		data.parent = new ObjectID(body.parent);
		
	if(Object.keys(data).length > 0){
		if(data.parent)
			return this.findOne({_id: data.parent}, function(success, mes){
				if(success)
					return this.update({_id: id}, {"$set":data}, {}, function(success, mes){
						if(success)
							return cb ? cb(true) : false;
						else
							return cb ? cb(false, mes) : false;
					});
				else
					return cb ? cb(false, "No se ha encontrado la localidad padre") : false;
			}.bind(this));
		else
			return this.update({_id: id}, {"$set":data}, {}, function(success, mes){
				if(success)
					return cb ? cb(true) : false;
				else
					return cb ? cb(false, mes) : false;
			});
	}else
		return cb ? cb(false, "Debes enviar al menos un campo para actualizar") : false;	
};

method.update = function(query, order, extra, cb){
	
	
	collection.update(query, order, extra, function(err){
		if(err){
			console.log(err);
			return cb ? cb(false, "Error al actualizar") : false;
		}else{
			return cb ? cb(true) : false;
		}
	});
};

method.remove = function(filter, cb){
	this.find(filter, false, false, false, function(items){
		if(items){
			var handleLoop = function(i){
				for(var k = 0 ; k < items[i].banners.length ; k++)
					fileUtils.deleteFile("routes/uploads/husk/"+items[i].banners[k]);
			}
			
			for(var i = 0 ; i < items.length ; i++)
				handleLoop(i);
			
			collection.remove(filter, function(err){
				if(err){
					console.log(err);
					return cb ? cb(false, "Error al eliminar") : false;
				}else
					return cb ? cb(true) : false;
			});
		}else
			return cb ? cb(false, "Nada por eliminar") : false;
	});
	return;
};

method.count = function(filter, cb){
	
	if(filter.name){
		filter.searchName = {"$regex":new RegExp(this.clean(filter.name),'i')};
		delete filter.name;
	}
	
	
	collection.count(filter, function(err, count){
		if(err){
			console.log(err);
			cb(false, "Error al conseguir total de busqueda");
		}else
			cb(true, count);
		return;
	});
	return;
};

method.validateArray = function(items, cb){
	var round = 1, cl = items.length, bodies = [], failed = 0;
	let loop = function(i){
		if(ObjectID.isValid(items[i])){
			items[i] = new ObjectID(items[i]); 
			let find = {_id: new ObjectID(items[i])};
			if(i>0)
				find.parent = new ObjectID(items[i-1]);
			this.findOne(find, function(success, mes){
				if(success){
					bodies[i] = mes;
					if(round == cl){
						if(failed == 0)
							return cb ? cb(true, bodies) : false;
						else
							return cb ? cb(false, "Error al validar una o mas categorias") : false;
					}
					round++;
				}else{
					if(round == cl){
						if(failed == 0)
							return cb ? cb(true, bodies) : false;
						else
							return cb ? cb(false, "Error al validar una o mas categorias") : false;
					}
					round++;
					failed++;
				}
				return;
			}.bind(this));
		}else{
			if(round == cl){
				if(failed == 0)
					return cb ? cb(true, bodies) : false;
				else
					return cb ? cb(false, "Error al validar uno o más categorias") : false;
			}
			round++;
			failed++
		}
	}.bind(this);
	
	if(cl > 0)
		for(let i = 0 ; i < cl ; i++)
			loop(i);
	else
		return cb ? cb(false, "Debes enviar al menos una categoria") : false;
};

method.find = function(filter, order, skip, limit, cb){
	
	if(typeof order == 'function'){
		cb = order;
		order = false;
	}
	
	if(filter.name){
		filter.searchName = {"$regex":new RegExp(this.clean(filter.name),'i')};
		delete filter.name;
	}
	
	var promise = collection.find(filter);
	
	if(order)
		promise.sort(order);
	if(skip)
		promise.skip(skip);
	if(limit)
		promise.limit(limit);
		
	promise.toArray(function(err, users){
		if(err){
			console.log(err);
			return cb ? cb(false, "Error al ejecutar busqueda") : false;
		}else
			return cb ? cb(true, users) : false;
	});
};

method.findOne = function(filter, cb){
	
	if(filter.name){
		filter.searchName = {"$regex":new RegExp(this.clean(filter.name),'i')};
		delete filter.name;
	}
	
	collection.findOne(filter, function(err, user){
		if(err){
			console.log(err);
			return cb ? cb(false, "Error al encontrar") : false;
		}else if(user)
			return cb ? cb(true, user) : false;
		else
			return cb ? cb(false, "Categoria no encontrada") : false;
	});
};

method.str_replace = function($f, $r, $s){
    return $s.replace(new RegExp("(" + (typeof($f) == "string" ? $f.replace(/[.?*+^$[\]\\(){}|-]/g, "\\$&") : $f.map(function(i){return i.replace(/[.?*+^$[\]\\(){}|-]/g, "\\$&")}).join("|")) + ")", "g"), typeof($r) == "string" ? $r : typeof($f) == "string" ? $r[0] : function(i){ return $r[$f.indexOf(i)]});
};

method.clean = function(name){
	var f=['Š','Œ','Ž','š','œ','ž','Ÿ','¥','µ','À','Á','Â','Ã','Ä','Å','Æ','Ç','È','É','Ê','Ë','?','Ì','Í','Î','Ï','I','Ð','Ñ','Ò','Ó','Ô','Õ','Ö','Ø','Ù','Ú','Û','Ü','Ý','ß','à','á','â','ã','ä','å','æ','ç','è','é','ê','ë','?','ì','í','î','ï','i','ð','ñ','ò','ó','ô','õ','ö','ø','ù','ú','û','ü','ý','ÿ'],
		r=['S','O','Z','s','o','z','Y','Y','u','A','A','A','A','A','A','A','C','E','E','E','E','E','I','I','I','I','I','D','N','O','O','O','O','O','O','U','U','U','U','Y','s','a','a','a','a','a','a','a','c','e','e','e','e','e','i','i','i','i','i','o','n','o','o','o','o','o','o','u','u','u','u','y','y'];
	return this.str_replace(f,r,name).toLowerCase();		
};

method.insert = function(body, cb){
	if(!body.name)
		return cb ? cb(false, "Debes enviar todos los campos") : false;
	
	var data = {
		name : body.name,
		searchName: this.clean(body.name),
		sinonims : body.sinonims ? this.clean(body.sinonims) : '',
		parent: ObjectID.isValid(body.parent) ? new ObjectID(body.parent) : false,
		picture: body.picture || false
	};
	
	if(data.parent){
		this.findOne({_id:data.parent}, function(cat){
			if(cat)
				collection.insert(data, function(err, result){
					if(err){
						console.log(err);
						return cb ? cb(false, "Error al insertart") : false;
					}else{
						return cb ? cb(true, new ObjectID(result.insertedIds[0])) : false;
					}
				});
			else
				return cb ? cb(false, "Categoria padre invalida") : false;
		}.bind(this));
	}else{
		data.parent = false;
		collection.insert(data, function(err, result){
			if(err){
				console.log(err);
				return cb ? cb(false, "Error al insertart") : false;
			}else{
				return cb ? cb(true, new ObjectID(result.insertedIds[0])) : false;
			}
		});
	}
};

module.exports = CategoryFaq;