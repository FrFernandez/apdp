var db = require('../utils/db');
var collection = db.collection("valuepair");
var ObjectID = require('mongoskin').ObjectID;
var method = Categorydt.prototype;

function Categorydt(id, cb){
	if(id && ObjectID.isValid(id)){
		this.findOne({_id:new ObjectID(id)}, function(categorydt){
			if(categorydt){
				this._id = new ObjectID(id);
				this._categorydt = categorydt;
				cb();
			}else
				throw "Invalid categorydt";
		});
	}
}

method.setID = function(id){
	if(ObjectID.isValid(id))
		this._id = new ObjectID(id);
	else
		throw id + " must be an instance of OBJECTID";
	return;
};

method.update = function(query, order, cb){
	collection.update(query, order, function(err){
		if(err){
			console.log(err);
			return cb ? cb(false, "Error al actualizar") : false;
		}else{
			return cb ? cb(true) : false;
		}
	});
};

method.remove = function(filter, cb){
	return collection.remove(filter, function(err){
		if(err){
			console.log(err);
			return cb ? cb(false, "Error al eliminar") : false;
		}else
			return cb ? cb(true) : false;
	});
};

method.count = function(filter, cb){
	collection.count(filter, function(err, count){
		if(err){
			console.log(err);
			return cb ? cb(false, "Error al conseguir total de busqueda") : false;
		}else
			return cb ? cb(true, count) : false;
	});
};

method.find = function(filter, order, skip, limit, cb){
	if(typeof order == 'function'){
		cb = order;
		order = false;
	}
	
	var promise = collection.find(filter);
	if(order)
		promise.sort(order);
	if(skip)
		promise.skip(skip);
	if(limit)
		promise.limit(limit);
		
	promise.toArray(function(err, users){
		if(err){
			console.log(err);
			return cb ? cb(false, "Error al encontrar elementos") : false;
		}else if(users)
			return cb ? cb(true, users) : false;
		else return cb ? cb(false, "Elementos no encontradas") : false;
	});
};

method.findOne = function(filter, cb){
	collection.findOne(filter, function(err, user){
		if(err){
			console.log(err);
			return cb ? cb(false, "Error al encontrar elemento") : false;
		}else if(user)
			return cb ? cb(true, user) : false;
		else
			return cb ? cb(false, "Elemento no encontrada") : false;
	});
};


method.insert = function(data, cb){
	if(data.key && data.value){
		collection.insert(data, function(err, result){
			if(err){
				console.log(err);
				return cb ? cb(false, "Error al insertar") : false;
			}else{
				return cb ? cb(new ObjectID(result.insertedIds[0])) : false;
			}
		});
	}else
		return cb ? cb(false, "Debes enviar todos los campos") : false;
};

module.exports = Categorydt;