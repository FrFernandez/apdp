var mailer = require('nodemailer'),
	transporter = mailer.createTransport({
		host: 'localhost',
		port: 465,
		secure: true, // use SSL
		tls: {rejectUnauthorized: false},
		auth: {
			user: 'no-responder@okapp.website',
			pass: 'Token.01'
		}
	});


/*transporter.verify(function(error, success) {
   if (error) {
        console.log(error);
   } else {
        console.log('Server is ready to take our messages');
   }
});*/
var footer = 'PIE DE PÁGINA';

var wrapBody = function(body){
	return 'CABECERA<br><br>'+body+'<br><br>'+footer+'';
};

var forgotMail = function(name,token,tokenSecret){
	return {
		from: '"OKApp" <no-responder@okapp.website>',
		subject: 'Recupera tu contraseña', // Subject line
		text: 'Hola '+name+'.\n\nSe ha solicitado restaurar la contraseña de tu cuenta. Para  realizar el cambio da click en el siguiente enlace:\n\n http://okapp.website/forgotpassword/'+token+'/'+tokenSecret, // plaintext body
		html: wrapBody('<h3>Hola '+name+'.</h3><br><br>Se ha solicitado restaurar la contraseña de tu cuenta. Para  realizar el cambio da click en el siguiente enlace.<br><br><a href="http://okapp.website/forgotpassword/'+token+'/'+tokenSecret+'">Restaurar Contraseña</a>') // html body					
	};
};

var confirmMail = function(name, mail, mailToken){
		return {
			from: '"OKApp" <no-responder@okapp.website>',
			subject: 'Confirma tu E-mail', // Subject line
			text: 'Hola '+name+'.\n\nPara comenzar a usar OKApp como usuario es necesario que confirmes tu cuenta.\n\n Por favor haz click en el seguiente enlance:\n\n http://okapp.website/register/mail/'+mail+'/'+mailToken+'\n\n', // plaintext body
			html: wrapBody('<h3>Hola '+name+'.</h3><br><br>Para comenzar a usar OKApp como usuario es necesario que confirmes tu cuenta.<br><br> Por favor haz click en el seguiente enlance: <a href="http://okapp.website/register/mail/'+mail+'/'+mailToken+'">http://okapp.website/register/mail/'+mail+'/'+mailToken+'</a><br><br>') // html body					
		};
	};

	
var youWon = function(name, raffleName, link){
	return {
		from: '"OKApp" <no-responder@okapp.website>',
		subject: 'Felicidades al ganar '+raffleName, // Subject line
		text: 'Hola '+name+'.\n\n:\n\n Felicidades por ganar la rifa '+raffleName+', pronto nos pondremos en contacto contigo\n\n', // plaintext body
		html: wrapBody('<h3>Hola '+name+'.</h3><br><br>Felicidades por ganar la rifa <b>'+raffleName+'</b>, pronto nos pondremos en contacto contigo') // html body					
	};
};	
	
var sendMail = 	function(mailOptions) {

		mailOptions.encoding = 'utf-8';
		transporter.sendMail(mailOptions, function(error, info){
			if(error){
				return console.log(error);
			}
			console.log('Message sent: ' + info.response);
		});
	};
	

	
var exports = {
	wrapBody: wrapBody,
	sendMail: sendMail,
	confirmMail: confirmMail,
	forgotMail: forgotMail,
	youWon: youWon
};

module.exports = exports;
