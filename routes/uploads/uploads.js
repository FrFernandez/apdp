var express = require('express');
var sharp = require('sharp');
var fs = require('fs');
var router = express.Router();
//sharp.concurrency(0);
//sharp.cache(0);

router.get('/', function(req, res) {
	res.status(404).send('Not found');
});

router.get('/scaled/:maxwidth/:name',function(req,res){
	var path = __dirname+'/husk/'+decodeURIComponent(req.params.name);
	fs.exists(path, function(exists){
		if(exists){
			sharp(path)
				.resize(parseInt(req.params.maxwidth),parseInt(req.params.maxwidth))
				.max()
				.withoutEnlargement()
				.toBuffer()
				.then(function(data) {
					res.setHeader("Cache-Control", "public, max-age=2592000");
					res.setHeader("Expires", new Date(Date.now() + 2592000000).toUTCString());
					res.write(data);
					res.end();
				}).catch(function(e) {
					console.log(e);
					res.status(404).send('Not found');
				});
		}else{
			res.status(404).send('Not found');
		}
	});
});

router.get('/square/:maxwidth/:name', function(req,res){
	var path = __dirname+'/husk/'+decodeURIComponent(req.params.name);
	fs.exists(path, function(exists){
		if(exists){
			
			sharp(path)
			.max()
			.resize(parseInt(req.params.maxwidth),parseInt(req.params.maxwidth))
			.toBuffer()
			.then(function(data){
				sharp({
				  create: {
					width: parseInt(req.params.maxwidth),
					height: parseInt(req.params.maxwidth),
					channels: 4,
					background: { r: 0, g: 0, b: 0, alpha: 0 }
				  }
				})
				.png()
				.max()
				.overlayWith( data, { gravity: sharp.gravity.center } )
				.sharpen()
				.toBuffer()
				.then(function(data) {
					res.setHeader("Cache-Control", "public, max-age=2592000");
					res.setHeader("Expires", new Date(Date.now() + 2592000000).toUTCString());
					res.write(data);
					res.end();
				}).catch(function(e) {
					console.log(e);
					res.status(404).send('Not found');
				});
			}).catch(function(e) {
				console.log(e);
				res.status(404).send('Not found');
			});
		}else{
			res.status(404).send('Not found');
		}
	});
});

router.get('/:name',function(req,res){
	var path = __dirname+'/husk/'+decodeURIComponent(req.params.name);
	fs.exists(path, function(exists){
		if(exists){
			sharp(path)
				.toBuffer()
				.then(function(data) {
					res.setHeader("Cache-Control", "public, max-age=2592000");
					res.setHeader("Expires", new Date(Date.now() + 2592000000).toUTCString());
					res.write(data);
					res.end();
				}).catch(function(e) {
					console.log(e);
					res.status(404).send('Not found');
				});
		}else{
			res.status(404).send('Not found');
		}
	});
});


module.exports = router;
