function LayoutController(GeoPosition,toastr,$rootScope,$state,$scope,$http,$location,$stateParams,$cookies,$cookieStore,$document,$window,$timeout,Auth,Publications,Users,Locality,SweetAlert) {
	$rootScope.title = $state.current.title;

	$rootScope.open = false;

	$scope.publications = angular.copy(Publications);
	$scope.users = angular.copy(Users);
	$scope.Locality = angular.copy(Locality);

  $scope.auth = Auth;
	if($scope.auth.isLogged()){
    $scope.auth.getUser(function(success){});
  }

  $scope.flag=false;
  $scope.index = null;
  $scope.indexx = null;

  $scope.publications.s = {
    start: 0,
    limit: 7,
    status: 1
  }
  $scope.users.s = {
    start: 0,
    limit: 7,
    status: 1
  }

  $scope.search = function(){
    $state.go('base.search',{search: $scope.searchText,page: 1})
  }
  $scope.searchChange = function(){
    if($scope.searchText.length > 0){
      $scope.publications.items = [];
      $scope.users.items = [];
      $scope.publications.s.name = $scope.searchText;
      $scope.users.s.username = $scope.searchText;
      $scope.users.get(function(success){
        if(success){
          $scope.found=false;
          $scope.flag=true;
        }else {
          $scope.found=true;
          splice();
        }
      })
      $scope.publications.get(function(success){
        if(success){
          $scope.found=false;
          $scope.flag=true;
          splice();
        }else {
          $scope.found=true;
        }
      })
    }else {
      $scope.flag = false;
    }
    function splice(){
      if(($scope.publications.items.length > 0) && ($scope.users.items.length > 0)){
        $scope.publications.items = $scope.publications.items.splice(0,2);
        $scope.users.items = $scope.users.items.splice(0,2);
      }
    }
  }
  $scope.HoverSearch = function(Item,index,flag){
      $scope.searchText = Item;
      if(flag){
        $scope.index = index;
        $scope.indexx = null;
      }else{
        $scope.indexx = index;
        $scope.index = null;
      }
  }

  $scope.Load = false;
  $scope.send = {
    form: {},
    login: function(){
      let form = this.form;
      $scope.auth.login(form,function(success){
        if(success)
          this.OnForm();
					$('#LoginResponsive').modal('hide')
          console.log(success);
      }.bind(this))
    },
    contact: function(){
      $('.close').click();
    },
    register: function(){
      $scope.Load = true;
      $scope.users.a = this.form;
      console.log(this.form)
      if(!this.form.photo){
				$scope.Load = false;
				return SweetAlert.swal({
					title:'No has incluido una imagen de perfil',
					text: 'Debes incluir un imagen de perfil para poder registrarte',
					type: 'error'
				},function(success){})
			}
      $scope.users.post(function(success){
        if(success){
          $scope.Load = true;
        	SweetAlert.swal({
						text: 'Registro Exitoso',
						imageUrl: 'http://50.21.179.35:8083/bill-ok.png',
					  imageWidth: 150,
					  imageHeight: 150,
					  animation: false,
						confirmButtonText:"Continuar",
						showCancelButton: true
					},function(success){
						if(success)
							console.log('OK')
							$('#SignIn').modal('hide');
							$scope.send.OnForm();
					})
        }else {
          $scope.Load = false;
        }
      });
    },
    AutoFormContact: function() {
      this.form.name = $scope.auth.user.name;
      this.form.mail = $scope.auth.user.mail;
    },
    forgotpassword: function(){},
    OnForm: function() {
      this.form = {
        username: '',
        name: '',
        mail: '',
        msj: '',
        password: '',
        lastname: '',
        phone: '',
        address: '',
        document: '',
        dob: '',
        gender: '',
        datasource: 'regular',
        priv: '3',
        photo: false
      };
    }
  }
  $scope.send.OnForm();

}

function IndexController($sce,GeoPosition,toastr,$rootScope,$state,$scope,$http,$location,$stateParams,$cookies,$cookieStore,$document,$window,$timeout,Auth,Banners,Category,Publications,Users,Spots) {
	$rootScope.title = $state.current.title;
  $scope.users = angular.copy(Users);
  $scope.banners = angular.copy(Banners);
  $scope.category = angular.copy(Category);
  $scope.publications = angular.copy(Publications);
  $scope.spots = angular.copy(Spots);

  $scope.flagRight = true;
  $scope.flagLeft = true;
  $scope.users.s = {
    start: 0,
    limit: 5,
    status: 1
  }
  $scope.banners.get(function(success){
    if(success){

    }
  })
  $scope.category.s = {
    parent: "false"
  }
  $scope.category.get(function(success){
    if(success){
      $scope.ParentCat = $scope.category.items;
      $scope.categorySingle($scope.ParentCat[0]._id);
    }
  })
  $scope.categorySingle = function(Id){
    if(Id){
      $scope.category.s = {
          parent: Id
      }
      GetCategory();

    }else {
      $scope.category.s = {
        parent: Id
      }
      GetCategory();
    }
    function GetCategory(){
      $scope.category.get(function(success){
        if(success){
          $scope.listCat = $scope.category.items;
          $scope.active = $scope.category.s.parent;
        }
      })
    }
  }
  $scope.publications.get(function(success){
    if(success){
    }
  })
  $scope.users.get(function(success) {
    if(success){

    }
  })
	$scope.trustSrc = function(src) {
		url = 'https://www.youtube.com/embed/'+src+'?rel=0';
    return $sce.trustAsResourceUrl(url);
  }
	$scope.array = [];
  $scope.spots.get(function(success){
		if(success){
			if($scope.spots.total>1){
				$scope.pages = (Math.ceil($scope.spots.total/3));
				for(var i=0;i<$scope.pages;i++){
					$scope.array[i] = [];

					var y = 0;
					y = i*3;

					if(i==0){
						$scope.array[i][0] = $scope.spots.items[y];
						$scope.array[i][1] = $scope.spots.items[y+1];
						$scope.array[i][2] = $scope.spots.items[y+2];
					}
					if(i>0){
						$scope.array[i][0] = $scope.spots.items[y];
						$scope.array[i][1] = $scope.spots.items[y+1];
						$scope.array[i][2] = $scope.spots.items[y+2];
					}
				}
			}else {
				$scope.pages = 1;
			}
		}
	})
}

function SummaryController(Transactions, Comments, GeoPosition,toastr,$rootScope,$state,$scope,$http,$location,$stateParams,$cookies,$cookieStore,$document,$window,$timeout,Auth,Banners,Category,Publications,Users,Spots){
  if(!Auth.isLogged())
		return $state.go('base.index');
  $rootScope.title = $state.current.title;

	$scope.toanswer = angular.copy(Comments);
	$scope.toanswer.s = {
		toMe: true,
		start: 0,
		limit: 1,
		answered: 'false'
	};
	$scope.toanswer.get();

	$scope.answered = angular.copy(Comments);
	$scope.answered.s = {
		byMe: true,
		start: 0,
		limit: 1,
		// answered: 'false'
	};
	$scope.answered.get();


	$scope.publiActive = angular.copy(Publications);
	$scope.publiActive.s = {
		byMe: true,
		start: 0,
		limit: 1,
		status: 1
	};
	$scope.publiActive.get();

	$scope.publiPaused = angular.copy(Publications);
	$scope.publiPaused.s = {
		byMe: true,
		start: 0,
		limit: 1,
		status: 2
	};
	$scope.publiPaused.get();
	$scope.publiEnded = angular.copy(Publications);
	$scope.publiEnded.s = {
		byMe: true,
		start: 0,
		limit: 1,
		status: 3
	};
	$scope.publiEnded.get();
	$scope.publiShared = angular.copy(Publications);
	$scope.publiShared.s = {
		byMe: true,
		start: 0,
		limit: 1,
		status: 31
	};
	$scope.publiShared.get();

	$scope.completeSales = angular.copy(Transactions);
	$scope.completeSales.s = {
		sales: true,
		start: 0,
		limit: 1,
		status: 2
	};
	$scope.completeSales.get();

	$scope.activesSales = angular.copy(Transactions);
	$scope.activesSales.s = {
		sales: true,
		start: 0,
		limit: 1,
		status: 1
	};
	$scope.activesSales.get();

	$scope.completePurchases = angular.copy(Transactions);
	$scope.completePurchases.s = {
		purchases: true,
		start: 0,
		limit: 1,
		status: 2
	};
	$scope.completePurchases.get();

	$scope.activePurchases = angular.copy(Transactions);
	$scope.activePurchases.s = {
		purchases: true,
		start: 0,
		limit: 1,
		status: 1
	};
	$scope.activePurchases.get();

}

function salesPublicationsController(Transactions, Comments, GeoPosition,toastr,$rootScope,$state,$scope,$http,$location,$stateParams,$cookies,$cookieStore,$document,$window,$timeout,Auth,Banners,Category,Publications,Users,Spots){
  if(!Auth.isLogged())
		return $state.go('base.index');
  $rootScope.title = $state.current.title;

	$rootScope.salesActive = true;

	$scope.itemsActive = [];
	$scope.itemsPaused = [];
	$scope.itemsEnded = [];
	$scope.totalActive = 1000;
	$scope.totalPaused = 1000;
	$scope.totalEnded = 1000;
	$scope.limit = 2;
	$scope.flag = false;

	$scope.publiActive = angular.copy(Publications);
	$scope.publiPaused = angular.copy(Publications);
	$scope.publiEnded = angular.copy(Publications);

	$scope.publiActive.s = {
		byMe: true,
		limit: $scope.limit,
		status: 1,
		// orderDate: 1
	};

	$scope.publiPaused.s = {
		byMe: true,
		limit: $scope.limit,
		status: 2,
		// orderDate: -1
	};

	$scope.publiEnded.s = {
		byMe: true,
		limit: $scope.limit,
		status: 3,
		// orderDate: -1
	};

	$scope.send = {
		form: {},
		search: function(){
			$state.go('.',{search: this.form.search,page: 1});
		},
		filter: function(options){
			if(options){
				this.form.filter = options;
				$state.go('.',{orderSales: this.form.filter});
			}else {
				$state.go('.',{orderDate: this.form.sort});
			}
		}
	}

	$scope.state = $stateParams.state;
	switch ($stateParams.state) {
		case 'enabled':
			$scope.options = 0;
			console.log('enable')
			$scope.publiActive.s.name 			= $stateParams.search ? $stateParams.search: '';
			$scope.currentPage 							= $stateParams.page ? $stateParams.page: 1;
			$scope.publiActive.s.start 			= $scope.limit*($scope.currentPage-1);
			if($stateParams.orderDate)
				$scope.publiActive.s.orderDate 	= $stateParams.orderDate ? $stateParams.orderDate: '';
				$scope.send.form.sort = $stateParams.orderDate ? $stateParams.orderDate: '';
			if($stateParams.orderSales)
				$scope.orderSales = $stateParams.orderSales ? $stateParams.orderSales: '';
				$scope.publiActive.s.orderSales = $stateParams.orderSales ? $stateParams.orderSales: '';
			$scope.publiActive.get(function(success){
				if(success){
					$scope.itemsActive = $scope.publiActive.items;
					$scope.totalActive = $scope.publiActive.total;
					if($scope.totalActive>0)
						$scope.flag = true;
				}
			});
			break;
		case 'paused':
			$scope.options = 1;
			console.log('paused')
			$scope.publiPaused.s.name 			= $stateParams.search ? $stateParams.search: '';
			$scope.currentPage 							= $stateParams.page ? $stateParams.page: 1;
			$scope.publiPaused.s.start 			= $scope.limit*($scope.currentPage-1);
			if($stateParams.orderDate)
				$scope.publiPaused.s.orderDate 	= $stateParams.orderDate ? $stateParams.orderDate: '';
				$scope.send.form.sort = $stateParams.orderDate ? $stateParams.orderDate: '';
			if($stateParams.orderSales)
				$scope.orderSales = $stateParams.orderSales ? $stateParams.orderSales: '';
				$scope.publiPaused.s.orderSales = $stateParams.orderSales ? $stateParams.orderSales: '';
			$scope.publiPaused.get(function(success){
				if(success){
					$scope.itemsPaused = $scope.publiPaused.items;
					$scope.totalPaused = $scope.publiPaused.total;
					if($scope.totalPaused>0)
						$scope.flag = true;
				}
			});
			break;
		case 'ended':
			$scope.options = 2;
			console.log('ended')
			$scope.publiEnded.s.name 				= $stateParams.search ? $stateParams.search: '';
			$scope.currentPage 							= $stateParams.page ? $stateParams.page: 1;
			$scope.publiEnded.s.start 			= $scope.limit*($scope.currentPage-1);
			if($stateParams.orderDate)
				$scope.publiEnded.s.orderDate 	= $stateParams.orderDate ? $stateParams.orderDate: '';
				$scope.send.form.sort = $stateParams.orderDate ? $stateParams.orderDate: '';
			if($stateParams.orderSales)
				$scope.orderSales = $stateParams.orderSales ? $stateParams.orderSales: '';
				$scope.publiEnded.s.orderSales = $stateParams.orderSales ? $stateParams.orderSales: '';
			$scope.publiEnded.get(function(success){
				if(success){
					$scope.itemsEnded = $scope.publiEnded.items;
					$scope.totalEnded = $scope.publiEnded.total;
					if($scope.totalEnded>0)
						$scope.flag = true;
				}
			});
			break;
		default:
			console.log('default')
			if($stateParams.search){
				$scope.publiActive.s.name = $stateParams.search;
			}
			$scope.currentPage = $stateParams.page ? $stateParams.page: 1;
			$scope.publiActive.s.start = $scope.limit*($scope.currentPage-1);
			$scope.publiActive.get(function(success){
				if(success){
					$scope.itemsActive = $scope.publiActive.items;
					$scope.totalActive = $scope.publiActive.total;
					if($scope.totalActive>0)
						$scope.flag = true;
				}
			});
	}

	$scope.setPage = function (pageNo) {
		$scope.currentPage = pageNo;
		console.log(pageNo)
	};
	$scope.pageChanged = function() {
		$state.go('.',{page: $scope.currentPage})
		console.log('Page changed to: ' + $scope.currentPage);
	}

	$scope.publiActiveTotal = angular.copy(Publications);
	$scope.publiActiveTotal.s = {
		byMe: true,
		status: 1
	};
	$scope.publiActiveTotal.get(function(success){
		$scope.activeTotal = $scope.publiActiveTotal.total;
	});
	$scope.publiPausedTotal = angular.copy(Publications);
	$scope.publiPausedTotal.s = {
		byMe: true,
		status: 2
	};
	$scope.publiPausedTotal.get(function(success){
		$scope.pauseTotal = $scope.publiPausedTotal.total;
	});
	$scope.publiEndedTotal = angular.copy(Publications);
	$scope.publiEndedTotal.s = {
		byMe: true,
		status: 3
	};
	$scope.publiEndedTotal.get(function(success){
		$scope.endedTotal = $scope.publiEndedTotal.total;
	});

	if($stateParams.search){
		$scope.send.form.search = $stateParams.search;
	}
}

function salesSalesController(Transactions, Comments, GeoPosition,toastr,$rootScope,$state,$scope,$http,$location,$stateParams,$cookies,$cookieStore,$document,$window,$timeout,Auth,Banners,Category,Publications,Users,Spots){
  if(!Auth.isLogged())
		return $state.go('base.index');
  $rootScope.title = $state.current.title;
	$rootScope.salesActive = true;

	$scope.itemsActive = [];
	$scope.itemsComplete = [];
	$scope.totalActive = 1000;
	$scope.totalComplete = 1000;
	$scope.limit = 2;
	$scope.flag = false;

	$scope.activesSales = angular.copy(Transactions);
	$scope.completeSales = angular.copy(Transactions);

	$scope.activesSales.s = {
		sales: true,
		start: 0,
		limit: $scope.limit,
		status: 1
	};

	$scope.completeSales.s = {
		sales: true,
		start: 0,
		limit: $scope.limit,
		status: 2
	};

	$scope.send = {
		form: {},
		search: function(){
			$state.go('.',{search: this.form.search,page: 1});
		},
		filter: function(options){
			if(options){
				this.form.filter = options;
				$state.go('.',{orderSales: this.form.filter});
			}else {
				$state.go('.',{orderDate: this.form.sort});
			}
		}
	}

	$scope.state = $stateParams.state;
	switch ($stateParams.state) {
		case 'enabled':
			$scope.options = 0;
			console.log('enable')
			$scope.activesSales.s.name = $stateParams.search ? $stateParams.search: '';
			$scope.currentPage = $stateParams.page ? $stateParams.page: 1;
			$scope.activesSales.s.start = $scope.limit*($scope.currentPage-1);
			if($stateParams.orderDate)
				$scope.activesSales.s.orderDate 	= $stateParams.orderDate ? $stateParams.orderDate: '';
				$scope.send.form.sort = $stateParams.orderDate ? $stateParams.orderDate: '';
			if($stateParams.orderSales)
				$scope.orderSales = $stateParams.orderSales ? $stateParams.orderSales: '';
				$scope.activesSales.s.orderSales = $stateParams.orderSales ? $stateParams.orderSales: '';
			$scope.activesSales.get(function(success){
				if(success){
					$scope.itemsActive = $scope.activesSales.items;
					$scope.totalActive = $scope.activesSales.total;
					if($scope.totalActive>0)
						$scope.flag = true;
				}
			});
			break;
		case 'complete':
			$scope.options = 1;
			console.log('complete')
			$scope.completeSales.s.name = $stateParams.search ? $stateParams.search: '';
			$scope.currentPage = $stateParams.page ? $stateParams.page: 1;
			$scope.completeSales.s.start = $scope.limit*($scope.currentPage-1);
			if($stateParams.orderDate)
				$scope.completeSales.s.orderDate 	= $stateParams.orderDate ? $stateParams.orderDate: '';
				$scope.send.form.sort = $stateParams.orderDate ? $stateParams.orderDate: '';
			if($stateParams.orderSales)
				$scope.orderSales = $stateParams.orderSales ? $stateParams.orderSales: '';
				$scope.completeSales.s.orderSales = $stateParams.orderSales ? $stateParams.orderSales: '';
			$scope.completeSales.get(function(success){
				if(success){
					$scope.itemsComplete = $scope.completeSales.items;
					$scope.totalComplete = $scope.completeSales.total;
					if($scope.totalComplete>0)
						$scope.flag = true;
				}
			});
			break;
		default:
			$scope.options = 0;
			console.log('enable')
			$scope.activesSales.s.name = $stateParams.search ? $stateParams.search: '';
			$scope.currentPage = $stateParams.page ? $stateParams.page: 1;
			$scope.activesSales.s.start = $scope.limit*($scope.currentPage-1);
			if($stateParams.orderDate)
				$scope.activesSales.s.orderDate 	= $stateParams.orderDate ? $stateParams.orderDate: '';
				$scope.send.form.sort = $stateParams.orderDate ? $stateParams.orderDate: '';
			if($stateParams.orderSales)
				$scope.orderSales = $stateParams.orderSales ? $stateParams.orderSales: '';
				$scope.activesSales.s.orderSales = $stateParams.orderSales ? $stateParams.orderSales: '';
			$scope.activesSales.get(function(success){
				if(success){
					$scope.itemsActive = $scope.activesSales.items;
					$scope.totalActive = $scope.activesSales.total;
					if($scope.totalActive>0)
						$scope.flag = true;
				}
			});
	}

	$scope.setPage = function (pageNo) {
		$scope.currentPage = pageNo;
		console.log(pageNo)
	};
	$scope.pageChanged = function() {
		$state.go('.',{page: $scope.currentPage})
		console.log('Page changed to: ' + $scope.currentPage);
	};

	$scope.activesSalesTotal = angular.copy(Transactions);
	$scope.activesSalesTotal.s = {
		sales: true,
		start: 0,
		limit: 1,
		status: 1
	};
	$scope.activesSalesTotal.get(function(success){
		$scope.activeTotal = $scope.activesSalesTotal.total;
	});
	$scope.completeSalesTotal = angular.copy(Transactions);
	$scope.completeSalesTotal.s = {
		sales: true,
		start: 0,
		limit: 1,
		status: 2
	};
	$scope.completeSalesTotal.get(function(success){
		$scope.endedTotal = $scope.completeSales.total;
	});

	$scope.active = 0;

	if($stateParams.search){
		$scope.send.form.search = $stateParams.search;
	}

}

function salesQuestionController(Transactions, Comments, GeoPosition,toastr,$rootScope,$state,$scope,$http,$location,$stateParams,$cookies,$cookieStore,$document,$window,$timeout,Auth,Banners,Category,Publications,Users,Spots){
  if(!Auth.isLogged())
		return $state.go('base.index');
  $rootScope.title = $state.current.title;
	$rootScope.salesActive = true;

	$scope.answered = angular.copy(Comments);
	$scope.comments = angular.copy(Comments);
	$scope.answered.s = {
		byMe: true,
		start: 0,
	};
	$scope.answered.get();

	$scope.send = {
		form: {},
		comments: function(id,publication){
			$scope.comments.a = this.form;
			$scope.comments.a.parent = id;
			$scope.comments.a.publication = publication;
			$scope.comments.post(function(success){
				if(success)
					$scope.send.form = {};
					$scope.answered.get();
			})
		}
	}

	let letter = 0;
	$scope.letter = {
		count: 240,
		on: function(){
			if($scope.send.form.comment.length>letter)
				this.count = this.count - 1;
			else
				this.count = this.count + 1;

			letter = $scope.send.form.comment.length;
		}
	}

	$scope.show = function(index){
		console.log(index)
		$('#'+index+' .show').addClass('hidden');
		$('#'+index+' .hiden').removeClass('hidden');
		$('#box-comments'+index).addClass('in');
	}
	$scope.hide = function(index){
		console.log(index)
		$('#'+index+' .show').removeClass('hidden');
		$('#'+index+' .hiden').addClass('hidden');
		$('#box-comments'+index).removeClass('in');
	}

}

function salesClaimsController(Transactions, Comments, GeoPosition,toastr,$rootScope,$state,$scope,$http,$location,$stateParams,$cookies,$cookieStore,$document,$window,$timeout,Auth,Banners,Category,Publications,Users,Spots){
  if(!Auth.isLogged())
		return $state.go('base.index');
  $rootScope.title = $state.current.title;
	$rootScope.salesActive = true;

	$scope.toanswer = angular.copy(Comments);
	$scope.toanswer.s = {
		toMe: true,
		start: 0,
		limit: 1,
		answered: 'false'
	};
	$scope.toanswer.get();

	$scope.answered = angular.copy(Comments);
	$scope.answered.s = {
		byMe: true,
		start: 0,
		limit: 1,
		answered: 'false'
	};
	$scope.answered.get();


	$scope.publiActive = angular.copy(Publications);
	$scope.publiActive.s = {
		byMe: true,
		start: 0,
		limit: 1,
		status: 1
	};
	$scope.publiActive.get();

	$scope.publiPaused = angular.copy(Publications);
	$scope.publiPaused.s = {
		byMe: true,
		start: 0,
		limit: 1,
		status: 2
	};
	$scope.publiPaused.get();
	$scope.publiEnded = angular.copy(Publications);
	$scope.publiEnded.s = {
		byMe: true,
		start: 0,
		limit: 1,
		status: 3
	};
	$scope.publiEnded.get();
	$scope.publiShared = angular.copy(Publications);
	$scope.publiShared.s = {
		byMe: true,
		start: 0,
		limit: 1,
		status: 31
	};
	$scope.publiShared.get();

	$scope.completeSales = angular.copy(Transactions);
	$scope.completeSales.s = {
		sales: true,
		start: 0,
		limit: 1,
		status: 2
	};
	$scope.completeSales.get();

	$scope.activesSales = angular.copy(Transactions);
	$scope.activesSales.s = {
		sales: true,
		start: 0,
		limit: 1,
		status: 1
	};
	$scope.activesSales.get();

	$scope.completePurchases = angular.copy(Transactions);
	$scope.completePurchases.s = {
		purchases: true,
		start: 0,
		limit: 1,
		status: 2
	};
	$scope.completePurchases.get();

	$scope.activePurchases = angular.copy(Transactions);
	$scope.activePurchases.s = {
		purchases: true,
		start: 0,
		limit: 1,
		status: 1
	};
	$scope.activePurchases.get();

}

function purchasesFavoritesController(Transactions, Comments, GeoPosition,toastr,$rootScope,$state,$scope,$http,$location,$stateParams,$cookies,$cookieStore,$document,$window,$timeout,Auth,Banners,Category,Publications,Users,Spots){
  if(!Auth.isLogged())
		return $state.go('base.index');
  $rootScope.title = $state.current.title;
	$scope.publications = angular.copy(Publications);

	$scope.items = [];
	$scope.total = 1000;
	$scope.limit = 2;
	$scope.flag = false;

	$scope.publications.s = {
		byMe: true,
		start: 0,
		limit: $scope.limit,
		status: 1
	};

	$scope.publications.s.search = $stateParams.search ? $stateParams.search: '';
	$scope.currentPage = $stateParams.page ? $stateParams.page: 1;
	$scope.publications.s.start = $scope.limit*($scope.currentPage-1);

	if($stateParams.orderDate)
		$scope.publications.s.orderDate = $stateParams.orderDate ? $stateParams.orderDate: '';
		$scope.send.form.sort = $stateParams.orderDate ? $stateParams.orderDate: '';
	if($stateParams.orderSales)
		$scope.send.form.sort = $stateParams.orderSales ? $stateParams.orderSales: '';
		$scope.publications.s.orderSales = $stateParams.orderSales ? $stateParams.orderSales: '';

	$scope.publications.get(function(success){
		if(success)
			$scope.items = $scope.publications.items;
			$scope.total = $scope.publications.total;
			if($scope.total>0)
				$scope.flag = true;
	});

	$scope.setPage = function (pageNo) {
		$scope.currentPage = pageNo;
		console.log(pageNo)
	};
	$scope.pageChanged = function() {
		$state.go('.',{page: $scope.currentPage})
		console.log('Page changed to: ' + $scope.currentPage);
	};

	$scope.send = {
		form: {},
		search: function(){
			$state.go('.',{search: this.form.search,page: 1});
		},
		filter: function(){
			console.log(this.form.sort)
			switch(this.form.sort){
				case '0':
						$state.go('.',{orderSales: -1,orderDate: undefined});
					break;
				case '1':
						$state.go('.',{orderSales: 1,orderDate: undefined});
					break;
				case '2':
						$state.go('.',{orderDate: -1,orderSales: undefined});
					break;
				case '3':
						$state.go('.',{orderDate: 1,orderSales: undefined});
					break;
			}
		}
	}

	if($stateParams.search){
		$scope.send.form.search = $stateParams.search;
	}

}

function purchasesQuestionController(Transactions, Comments, GeoPosition,toastr,$rootScope,$state,$scope,$http,$location,$stateParams,$cookies,$cookieStore,$document,$window,$timeout,Auth,Banners,Category,Publications,Users,Spots){
	if(!Auth.isLogged())
		return $state.go('base.index');
  $rootScope.title = $state.current.title;

	$scope.answered = angular.copy(Comments);
	$scope.comments = angular.copy(Comments);
	$scope.answered.s = {
		toMe: true,
		start: 0,
		limit: 1
	};
	$scope.answered.get();

	$scope.send = {
		form: {},
		comments: function(){
			$scope.comments.a = this.form;
			$scope.comments.a.publication = $scope.publications.item._id;
			$scope.comments.post(function(success){
				if(success)
					$scope.send.form = {};
					$scope.answered.get();
			})
		}
	}

	let letter = 0;
	$scope.letter = {
		count: 240,
		on: function(){
			if($scope.send.form.comment.length>letter)
				this.count = this.count - 1;
			else
				this.count = this.count + 1;

			letter = $scope.send.form.comment.length;
		}
	}

	$scope.show = function(index){
		console.log(index)
		$('#'+index+' .show').addClass('hidden');
		$('#'+index+' .hiden').removeClass('hidden');
		$('#box-comments'+index).addClass('in');
	}
	$scope.hide = function(index){
		console.log(index)
		$('#'+index+' .show').removeClass('hidden');
		$('#'+index+' .hiden').addClass('hidden');
		$('#box-comments'+index).removeClass('in');
	}

}

function purchasesPurchasesController(Transactions, Comments, GeoPosition,toastr,$rootScope,$state,$scope,$http,$location,$stateParams,$cookies,$cookieStore,$document,$window,$timeout,Auth,Banners,Category,Publications,Users,Spots){
	if(!Auth.isLogged())
		return $state.go('base.index');
  $rootScope.title = $state.current.title;

	$scope.itemsActive = [];
	$scope.itemsComplete = [];
	$scope.totalActive = 1000;
	$scope.totalComplete = 1000;
	$scope.limit = 2;
	$scope.flag = false;

	$scope.activesPurchases = angular.copy(Transactions);
	$scope.completePurchases = angular.copy(Transactions);

	$scope.activesPurchases.s = {
		purchases: true,
		start: 0,
		limit: $scope.limit,
		status: 1
	};

	$scope.completePurchases.s = {
		purchases: true,
		start: 0,
		limit: $scope.limit,
		status: 2
	};

	$scope.send = {
		form: {},
		search: function(){
			$state.go('.',{search: this.form.search,page: 1});
		},
		filter: function(options){
			if(options){
				this.form.filter = options;
				$state.go('.',{orderSales: this.form.filter});
			}else {
				$state.go('.',{orderDate: this.form.sort});
			}
		}
	}

	$scope.state = $stateParams.state;
	switch ($stateParams.state) {
		case 'enabled':
			$scope.options = 0;
			console.log('enable')
			$scope.activesPurchases.s.search = $stateParams.search ? $stateParams.search: '';
			$scope.currentPage = $stateParams.page ? $stateParams.page: 1;
			$scope.activesPurchases.s.start = $scope.limit*($scope.currentPage-1);
			if($stateParams.orderDate)
				$scope.activesPurchases.s.orderDate 	= $stateParams.orderDate ? $stateParams.orderDate: '';
				$scope.send.form.sort = $stateParams.orderDate ? $stateParams.orderDate: '';
			if($stateParams.orderSales)
				$scope.orderSales = $stateParams.orderSales ? $stateParams.orderSales: '';
				$scope.activesPurchases.s.orderSales = $stateParams.orderSales ? $stateParams.orderSales: '';
			$scope.activesPurchases.get(function(success){
				if(success){
					$scope.itemsActive = $scope.activesPurchases.items;
					$scope.totalActive = $scope.activesPurchases.total;
					if($scope.totalActive>0)
						$scope.flag = true;
				}
			});
			break;
		case 'complete':
			$scope.options = 1;
			console.log('complete')
			$scope.completePurchases.s.search = $stateParams.search ? $stateParams.search: '';
			$scope.currentPage = $stateParams.page ? $stateParams.page: 1;
			$scope.completePurchases.s.start = $scope.limit*($scope.currentPage-1);
			if($stateParams.orderDate)
				$scope.completePurchases.s.orderDate 	= $stateParams.orderDate ? $stateParams.orderDate: '';
				$scope.send.form.sort = $stateParams.orderDate ? $stateParams.orderDate: '';
			if($stateParams.orderSales)
				$scope.orderSales = $stateParams.orderSales ? $stateParams.orderSales: '';
				$scope.completePurchases.s.orderSales = $stateParams.orderSales ? $stateParams.orderSales: '';
			$scope.completePurchases.get(function(success){
				if(success){
					$scope.itemsComplete = $scope.completePurchases.items;
					$scope.totalComplete = $scope.completePurchases.total;
					if($scope.totalComplete>0)
						$scope.flag = true;
				}
			});
			break;
		default:
			$scope.options = 0;
			$scope.activesPurchases.s.search = $stateParams.search ? $stateParams.search: '';
			$scope.currentPage = $stateParams.page ? $stateParams.page: 1;
			$scope.activesPurchases.s.start = $scope.limit*($scope.currentPage-1);
			if($stateParams.orderDate)
				$scope.activesPurchases.s.orderDate 	= $stateParams.orderDate ? $stateParams.orderDate: '';
				$scope.send.form.sort = $stateParams.orderDate ? $stateParams.orderDate: '';
			if($stateParams.orderSales)
				$scope.orderSales = $stateParams.orderSales ? $stateParams.orderSales: '';
				$scope.activesPurchases.s.orderSales = $stateParams.orderSales ? $stateParams.orderSales: '';
			$scope.activesPurchases.get(function(success){
				if(success){
					$scope.itemsActive = $scope.activesPurchases.items;
					$scope.totalActive = $scope.activesPurchases.total;
					if($scope.totalActive)
						$scope.flag = true;
				}
			});
	}

	$scope.setPage = function (pageNo) {
		$scope.currentPage = pageNo;
		console.log(pageNo)
	};
	$scope.pageChanged = function() {
		$state.go('.',{page: $scope.currentPage})
		console.log('Page changed to: ' + $scope.currentPage);
	};

	$scope.activesPurchasesTotal = angular.copy(Transactions);
	$scope.activesPurchasesTotal.s = {
		purchases: true,
		status: 1
	};
	$scope.activesPurchasesTotal.get(function(success){
		$scope.activeTotal = $scope.activesPurchasesTotal.total;
	});
	$scope.completePurchasesTotal = angular.copy(Transactions);
	$scope.completePurchasesTotal.s = {
		purchases: true,
		status: 2
	};
	$scope.completePurchasesTotal.get(function(success){
		$scope.endedTotal = $scope.completePurchasesTotal.total;
	});

	$scope.active = 0;

	if($stateParams.search){
		$scope.send.form.search = $stateParams.search;
	}
}

function billingPayController(Transactions, Comments, GeoPosition,toastr,$rootScope,$state,$scope,$http,$location,$stateParams,$cookies,$cookieStore,$document,$window,$timeout,Auth,Banners,Category,Publications,Users,Spots){
  if(!Auth.isLogged())
		return $state.go('base.index');
  $rootScope.title = $state.current.title;

	$scope.toanswer = angular.copy(Comments);
	$scope.toanswer.s = {
		toMe: true,
		start: 0,
		limit: 1,
		answered: 'false'
	};
	$scope.toanswer.get();

	$scope.answered = angular.copy(Comments);
	$scope.answered.s = {
		byMe: true,
		start: 0,
		limit: 1,
		answered: 'false'
	};
	$scope.answered.get();


	$scope.publiActive = angular.copy(Publications);
	$scope.publiActive.s = {
		byMe: true,
		start: 0,
		limit: 1,
		status: 1
	};
	$scope.publiActive.get();

	$scope.publiPaused = angular.copy(Publications);
	$scope.publiPaused.s = {
		byMe: true,
		start: 0,
		limit: 1,
		status: 2
	};
	$scope.publiPaused.get();
	$scope.publiEnded = angular.copy(Publications);
	$scope.publiEnded.s = {
		byMe: true,
		start: 0,
		limit: 1,
		status: 3
	};
	$scope.publiEnded.get();
	$scope.publiShared = angular.copy(Publications);
	$scope.publiShared.s = {
		byMe: true,
		start: 0,
		limit: 1,
		status: 31
	};
	$scope.publiShared.get();

	$scope.completeSales = angular.copy(Transactions);
	$scope.completeSales.s = {
		sales: true,
		start: 0,
		limit: 1,
		status: 2
	};
	$scope.completeSales.get();

	$scope.activesSales = angular.copy(Transactions);
	$scope.activesSales.s = {
		sales: true,
		start: 0,
		limit: 1,
		status: 1
	};
	$scope.activesSales.get();

	$scope.completePurchases = angular.copy(Transactions);
	$scope.completePurchases.s = {
		purchases: true,
		start: 0,
		limit: 1,
		status: 2
	};
	$scope.completePurchases.get();

	$scope.activePurchases = angular.copy(Transactions);
	$scope.activePurchases.s = {
		purchases: true,
		start: 0,
		limit: 1,
		status: 1
	};
	$scope.activePurchases.get();

}

function managerNetworkLinkController(Transactions, Comments, GeoPosition,toastr,$rootScope,$state,$scope,$http,$location,$stateParams,$cookies,$cookieStore,$document,$window,$timeout,Auth,Banners,Category,Publications,Users,Spots){
  if(!Auth.isLogged())
		return $state.go('base.index');
  $rootScope.title = $state.current.title;

	$scope.toanswer = angular.copy(Comments);
	$scope.toanswer.s = {
		toMe: true,
		start: 0,
		limit: 1,
		answered: 'false'
	};
	$scope.toanswer.get();

	$scope.answered = angular.copy(Comments);
	$scope.answered.s = {
		byMe: true,
		start: 0,
		limit: 1,
		answered: 'false'
	};
	$scope.answered.get();


	$scope.publiActive = angular.copy(Publications);
	$scope.publiActive.s = {
		byMe: true,
		start: 0,
		limit: 1,
		status: 1
	};
	$scope.publiActive.get();

	$scope.publiPaused = angular.copy(Publications);
	$scope.publiPaused.s = {
		byMe: true,
		start: 0,
		limit: 1,
		status: 2
	};
	$scope.publiPaused.get();
	$scope.publiEnded = angular.copy(Publications);
	$scope.publiEnded.s = {
		byMe: true,
		start: 0,
		limit: 1,
		status: 3
	};
	$scope.publiEnded.get();
	$scope.publiShared = angular.copy(Publications);
	$scope.publiShared.s = {
		byMe: true,
		start: 0,
		limit: 1,
		status: 31
	};
	$scope.publiShared.get();

	$scope.completeSales = angular.copy(Transactions);
	$scope.completeSales.s = {
		sales: true,
		start: 0,
		limit: 1,
		status: 2
	};
	$scope.completeSales.get();

	$scope.activesSales = angular.copy(Transactions);
	$scope.activesSales.s = {
		sales: true,
		start: 0,
		limit: 1,
		status: 1
	};
	$scope.activesSales.get();

	$scope.completePurchases = angular.copy(Transactions);
	$scope.completePurchases.s = {
		purchases: true,
		start: 0,
		limit: 1,
		status: 2
	};
	$scope.completePurchases.get();

	$scope.activePurchases = angular.copy(Transactions);
	$scope.activePurchases.s = {
		purchases: true,
		start: 0,
		limit: 1,
		status: 1
	};
	$scope.activePurchases.get();

}

function managerNetworkPublicationAutoController(Transactions, Comments, GeoPosition,toastr,$rootScope,$state,$scope,$http,$location,$stateParams,$cookies,$cookieStore,$document,$window,$timeout,Auth,Banners,Category,Publications,Users,Spots){
  if(!Auth.isLogged())
		return $state.go('base.index');
  $rootScope.title = $state.current.title;

	$scope.toanswer = angular.copy(Comments);
	$scope.toanswer.s = {
		toMe: true,
		start: 0,
		limit: 1,
		answered: 'false'
	};
	$scope.toanswer.get();

	$scope.answered = angular.copy(Comments);
	$scope.answered.s = {
		byMe: true,
		start: 0,
		limit: 1,
		answered: 'false'
	};
	$scope.answered.get();


	$scope.publiActive = angular.copy(Publications);
	$scope.publiActive.s = {
		byMe: true,
		start: 0,
		limit: 1,
		status: 1
	};
	$scope.publiActive.get();

	$scope.publiPaused = angular.copy(Publications);
	$scope.publiPaused.s = {
		byMe: true,
		start: 0,
		limit: 1,
		status: 2
	};
	$scope.publiPaused.get();
	$scope.publiEnded = angular.copy(Publications);
	$scope.publiEnded.s = {
		byMe: true,
		start: 0,
		limit: 1,
		status: 3
	};
	$scope.publiEnded.get();
	$scope.publiShared = angular.copy(Publications);
	$scope.publiShared.s = {
		byMe: true,
		start: 0,
		limit: 1,
		status: 31
	};
	$scope.publiShared.get();

	$scope.completeSales = angular.copy(Transactions);
	$scope.completeSales.s = {
		sales: true,
		start: 0,
		limit: 1,
		status: 2
	};
	$scope.completeSales.get();

	$scope.activesSales = angular.copy(Transactions);
	$scope.activesSales.s = {
		sales: true,
		start: 0,
		limit: 1,
		status: 1
	};
	$scope.activesSales.get();

	$scope.completePurchases = angular.copy(Transactions);
	$scope.completePurchases.s = {
		purchases: true,
		start: 0,
		limit: 1,
		status: 2
	};
	$scope.completePurchases.get();

	$scope.activePurchases = angular.copy(Transactions);
	$scope.activePurchases.s = {
		purchases: true,
		start: 0,
		limit: 1,
		status: 1
	};
	$scope.activePurchases.get();

}

function managerNetworkCampaignsController(Transactions, Comments, GeoPosition,toastr,$rootScope,$state,$scope,$http,$location,$stateParams,$cookies,$cookieStore,$document,$window,$timeout,Auth,Banners,Category,Publications,Users,Spots){
  if(!Auth.isLogged())
		return $state.go('base.index');
  $rootScope.title = $state.current.title;

	$scope.Auth = Auth;

	$scope.send = {
		form: {},
		on: function(){

		},
		delete: function(index){
			console.log(index)
			console.log($scope.files)
		}
	}

	// $scope.days = ['Domingo','Lunes','Martes','Miercoles','Jueves','Sabado'];
	// $scope.dayShourt = ['Do','Lu','Ma','Mi','Ju','Vi','Sa'];
	// $scope.Arraydays = [];
  //
	// $scope.pushDays = function() {
	// 	$scope.send.form.days = [];
	// 	for(i=0;i<$scope.Arraydays.length;i++){
	// 		$scope.send.form.days[i] = $scope.dayShourt[$scope.Arraydays[i]];
	// 	}
	// }

	let letter = 0;
  $scope.letter = {
    count: 240,
    on: function(){
      if($scope.send.form.comment.length>letter)
        this.count = this.count - 1;
      else
        this.count = this.count + 1;

      letter = $scope.send.form.comment.length;
    }
  }

}

function configurationDataUserController(Transactions,GeoPosition,toastr,$rootScope,$state,$scope,$http,$location,$stateParams,$cookies,$cookieStore,$document,$window,$timeout,Auth,Users){
	$rootScope.title = $state.current.title;

	if(!Auth.isLogged())
		return $state.go('base.index');

	$scope.send = {
		form: {},
		EditInfoPerson: function(){
			let data= {};
			if(this.form.document != $scope.Auth.document)
				data.document = this.form.document;
			if(this.form.idp != $scope.Auth.document)
				data.idp = this.form.idp;
			if(this.form.phone != $scope.Auth.phone)
			 	data.phone = this.form.phone;
			if(this.form.address != $scope.Auth.address)
				data.address = this.form.address;

			console.log(data)
		},
		EditSeudominio: function(){
			let data = {};
			if(this.form.username != $scope.Auth.username)
				data.username = this.form.username;

			console.log(data)
		},
		EditEmail: function(){
			let data = {};
			if(this.form.mail != $scope.Auth.mail)
				data.mail = this.form.mail;
			console.log(data)
		},
		EditPassword: function(){
			let data = {};
			console.log(this.form)
		},
		EditStore: function(){

			console.log(this.form)
		},
		EditInfoBank: function(){
			console.log(this.form)
		}
	}

	function LoadInfo(){
		$scope.send.form.mail 			= $scope.Auth.mail;
		$scope.send.form.document 	= $scope.Auth.document;
		$scope.send.form.name 			= $scope.Auth.name;
		$scope.send.form.lastname 	= $scope.Auth.lastname;
		$scope.send.form.username 	= $scope.Auth.username;
		$scope.send.form.phone 			= $scope.Auth.phone;
		$scope.send.form.address 		= $scope.Auth.address;
	}


	if(!Auth.user)
		Auth.getUser();
	else {
		$scope.Auth = Auth.user;
		LoadInfo();
	}
	if(Auth.user){
		$scope.Auth = Auth.user;
		LoadInfo();
	}
	else {
		$rootScope.$on('gotUser',function(){
			$scope.Auth = Auth.user;
			LoadInfo();
		})
	}
}

function SearchController(GeoPosition,toastr,$rootScope,$state,$scope,$http,$location,$stateParams,$cookies,$cookieStore,$document,$window,$timeout,Auth,Publications,Users,Category,Views,SweetAlert,Transactions){
	if(Auth.user){
    $scope.auth = Auth.user;
  }else {
		$rootScope.$on('gotUser',function(){
      $scope.auth = Auth.user;
    })
  }
  $scope.users = angular.copy(Users);
  $scope.publications = angular.copy(Publications);
  $scope.category = angular.copy(Category);
  $scope.views = angular.copy(Views);
	$scope.transactions = angular.copy(Transactions);

  $scope.items = [];
	$scope.total = 1000;
	$scope.currentPage = $stateParams.page ? $stateParams.page: 1;
	$scope.limit = 2;
	$scope.limit2 = 2;

	if(!$scope.currentPage>1){
		$scope.publications.s = {
			start: 0,
			limit: $scope.limit,
			status: 1,
			// orderDate: -1
		}
		$scope.users.s = {
			start: 0,
			limit: $scope.limit,
			status: 1
		}
	}else {
		$scope.publications.s = {
			start: $scope.limit*($scope.currentPage-1),
			limit: $scope.limit,
			status: 1,
			// orderDate: -1
		}
		$scope.users.s = {
			start: $scope.limit*($scope.currentPage-1),
			limit: $scope.limit,
			status: 1,
		}
	}

	$scope.setPage = function (pageNo) {
    $scope.currentPage = pageNo;
		console.log(pageNo)
  };
	$scope.pageChanged = function() {
		$state.go('.',{page: $scope.currentPage})
		console.log('Page changed to: ' + $scope.currentPage);
	};

  $scope.options = $state.current.options;

  switch($state.current.options){
    case 'categories':
      $scope.flag = true;
      $scope._Id = $stateParams.id;

      $scope.publications.s.categories = [$scope._Id];
      $scope.category.s._id = $scope._Id;

      $scope.publications.getSearch(function(success){
				if(success)
					console.log($scope.publications.itemSearch);
			})
      $scope.category.getSingle(function(success){ })
      $scope.publications.get(function(success) {
        if(success)
          $scope.items = $scope.publications.items;
					$scope.total = $scope.publications.total;
      })
    break;
    case 'search':
      $scope.flag = false;
      $scope.search = $stateParams.search;

      $scope.publications.s.name = $scope.search;
      $scope.users.s.username = $scope.search;
      $scope.category.s.name = $scope.search;

      $scope.category.get(function(success){})
      $scope.publications.get(function(success) {
          if(success){
						if($scope.publications.total>0){
							$scope.items = $scope.publications.items;
						}
						$scope.users.get(function(success){
							if(success){
								if($scope.users.total>0 && $scope.publications.total===0){
									$scope.items = $scope.users.items;
									if($scope.publications.total===0){
										$scope.total = $scope.users.total;
									}else {
										$scope.total += $scope.users.total;
									}
								}
								if($scope.users.total>0 && $scope.publications.total>0){
									$scope.limit2 = 4;
									$scope.items.splice(2,0,$scope.users.items[0])
									if($scope.users.total>1){
										$scope.items.splice(3,0,$scope.users.items[1])
									}
									$scope.total = $scope.publications.total;
									$scope.total += $scope.users.total;
									console.log($scope.total)
								}
								if($scope.publications.total>0 && $scope.users.total===0){
									$scope.total = $scope.publications.total;
									$scope.total += $scope.users.total;
								}
							}
						})
					}
      })
			$scope.publications.getSearch(function(success){
				if(success)
					$scope.categories = $scope.publications.itemSearch.categories;
					$scope.localities = $scope.publications.itemSearch.localities;
					$scope.states 		= $scope.publications.itemSearch.states;
			})
    break;
		case 'list':
	 		$scope.flag = true;
			$scope.publications.getSearch(function(success){
				if(success)
					$scope.categories = $scope.publications.itemSearch.categories;
					$scope.localities = $scope.publications.itemSearch.localities;
					$scope.states 		= $scope.publications.itemSearch.states;
			})

			$scope.publications.get(function(success) {
				$scope.items = $scope.publications.items;
				$scope.total = $scope.publications.total;
      })
		break;
  }

	$scope.publications.s.categories = [];
	$scope.publications.s.localities = [];
	$scope.publications.s.states = [];

	$scope.addFilter = function(type,id){
		switch (type) {
			case 0:
				$scope.publications.s.categories.push(id)
				$scope.publications.get(function(success) {
					if(success)
						$scope.items = $scope.publications.items;
						$scope.total = $scope.publications.total;
				})
				break;
			case 1:
				$scope.publications.s.localities.push(id)
				$scope.publications.get(function(success) {
					if(success)
						$scope.items = $scope.publications.items;
						$scope.total = $scope.publications.total;
				})
				break;
			case 2:
				$scope.publications.s.states.push(id)
				$scope.publications.get(function(success) {
					if(success)
						$scope.items = $scope.publications.items;
						$scope.total = $scope.publications.total;
				})
				break;
		}
	}

	$scope.OrderSort = {
		sort: {},
		submit: function(){
			switch(this.sort){
				case '0':
					console.log(this.sort)
					$scope.publications.s.orderDate = -1;
					$scope.publications.get(function(success) {
						if(success)
							$scope.items = $scope.publications.items;
							$scope.total = $scope.publications.total;
					})
					break;
				case '1':
					console.log(this.sort)
					$scope.publications.s.orderDate = 1;
					$scope.publications.get(function(success) {
						if(success)
							$scope.items = $scope.publications.items;
							$scope.total = $scope.publications.total;
					})
					break;
				case '2':
					console.log(this.sort)
					$scope.publications.s.orderSales = -1;
					$scope.publications.get(function(success) {
						if(success)
							$scope.items = $scope.publications.items;
							$scope.total = $scope.publications.total;
					})
					break;
				case '3':
					console.log(this.sort)
					$scope.publications.s.orderSales = 1;
					$scope.publications.get(function(success) {
						if(success)
							$scope.items = $scope.publications.items;
							$scope.total = $scope.publications.total;
					})
					break;
			}
		}
	}

	$scope.purchase = {};
	$scope.purchase = function($index){
		$scope.purchase.item = $scope.items[$index];
	}
	$scope.quantity = 1;
	$scope.quantityF = function(params){
		console.log(params)
		if(params)
			if($scope.quantity < $scope.purchase.item.stock)
				$scope.quantity+= 1;
		else
			if($scope.quantity > 1)
				$scope.quantity-= 1;
	}
	$scope.purchaseConfirm = function(){
		SweetAlert.swal({
			text: '¿Estas seguro de tu compra?',
			type: 'info',
			confirmButtonText:"Continuar",
			showCancelButton: true,
		},function(success){
			if(success){
				$scope.transactions.a._id = $scope.purchase.item._id;
				$scope.transactions.a.user = {
					_id: $scope.auth._id
				};
				$scope.transactions.a.quantity = $scope.quantity;
				$scope.transactions.post(function(success,id = null){
				if(success){
					$scope.id = id;
					$('#purchaseSuccess').modal('show');
				}
			})
			}
		})
	}
  // $scope.category.get(function(success){})
}

function PublicationController(GeoPosition,toastr,$rootScope,$state,$scope,$http,$location,$stateParams,$cookies,$cookieStore,$document,$window,$timeout,Auth,Publications,Users,Category,Views,Comments,SweetAlert,Transactions){
  if(Auth.isLogged())
    $scope.follow = true;
	if(Auth.user){
    $scope.auth = Auth.user;
  }else {
		$rootScope.$on('gotUser',function(){
      $scope.auth = Auth.user;
    })
  }
  $scope.id = $stateParams.id;

  $scope.publications = angular.copy(Publications);
  $scope.comments = angular.copy(Comments);
	$scope.transactions = angular.copy(Transactions);

  $scope.images = [];
  $scope.index = null;

  $scope.publications.getSingle($scope.id,function(success){
    if(success){
      $rootScope.title = $scope.publications.item.user.username+' - '+$scope.publications.item.name;
      $scope.average = ($scope.publications.item.user.rating.seller.total/$scope.publications.item.user.rating.seller.totalTallied);
      if(isNaN($scope.average))
        $scope.average = 0;
      $scope.comments.s.publication = $scope.publications.item._id;
      $scope.comments.get(function(success){})
    }
  })

  $scope.send = {
    form: {},
    comments: function(){
      $scope.comments.a = this.form;
      $scope.comments.a.publication = $scope.publications.item._id;
      $scope.comments.post(function(success){
        if(success)
          $scope.send.form = {};
		      $scope.comments.get(function(success){})
      })
    }
  }


  let letter = 0;
  $scope.letter = {
    count: 240,
    on: function(){
      if($scope.send.form.comment.length>letter)
        this.count = this.count - 1;
      else
        this.count = this.count + 1;

      letter = $scope.send.form.comment.length;
    }
  }

	$scope.quantity = 1;
	$scope.quantityF = function(params){
		console.log(params)
		if(params)
			if($scope.quantity < $scope.publications.item.stock)
				$scope.quantity+= 1;
		else
			if($scope.quantity > 1)
				$scope.quantity-= 1;
	}
	$scope.purchaseConfirm = function(){
		SweetAlert.swal({
			title: '',
			text: '¿Estas seguro de tu compra?',
			type: 'info',
			confirmButtonText:"Continuar",
			showCancelButton: true,
		},function(success){
			if(success){
				$scope.transactions.a._id = $scope.publications.item._id;
				$scope.transactions.a.user = {
					_id: $scope.auth._id
				};
				$scope.transactions.a.quantity = $scope.quantity;
				$scope.transactions.post(function(success,id = null){
					if(success){
						$scope.id = id;
						$('#purchaseSuccess').modal('show');
					}
				})
			}
		});
	}
}

function UserController(GeoPosition,toastr,$rootScope,$state,$scope,$http,$location,$stateParams,$cookies,$cookieStore,$document,$window,$timeout,Auth,Publications,Users,Category,Views,Follows) {
  if(Auth.isLogged())
    $scope.follow = true;
  $rootScope.id = $stateParams.id;
  $scope.self = false;
  $scope._id = $rootScope.id;

  $scope.users = angular.copy(Users);
  $scope.users.getSingle($rootScope.id,function(success){
		if(success){
			$scope.info = $scope.users.item;
			console.log($scope.info);
		}
  })

	$scope.follows = angular.copy(Follows);
	$scope.follows.s = {
	fromMe: true,
	follows: true,
	_id: $rootScope.id
  }
  $scope.follows.get(function(success){})
}

function ProfileController(GeoPosition,toastr,$rootScope,$state,$scope,$http,$location,$stateParams,$cookies,$cookieStore,$document,$window,$timeout,Auth,Publications,Users,Category,Views,Upload,Follows) {
  if(!Auth.isLogged())
    return $state.go('base.index');

	$scope.users = angular.copy(Users);

	$rootScope.id = false;
  $scope.self = true;

  if(Auth.user){
    $scope.info = Auth.user;
		$scope._id = $scope.info._id;
    loadMe();
  }
  else {
    $rootScope.$on('gotUser',function(){
      $scope.info = Auth.user;
			$scope._id = $scope.info._id;
      loadMe();
    })
  }

  function loadMe(){

    $scope.outputImageCover = null;
    $scope.inputImageCover = null;
    $rootScope.title = Auth.user.username;
    $scope.outputImagePhoto = null;
    $scope.inputImagePhoto = null;

    $scope.onUpdate = function(data) {};

    $scope.send = {
      form: {},
      ModalEdit: function(){
        if(this.form.cover)
          $('#ModalEdit').click();
          Upload.dataUrl(this.form.cover,false)
          .then(function(urls){
            $scope.inputImageCover = urls;
						console.log(urls)
          });
        if(this.form.photo)
          $('#ModalEdit').click();
          Upload.dataUrl(this.form.photo,false)
          .then(function(urls){
            $scope.inputImagePhoto = urls;
						console.log(urls)
          });
      },
      send: function(){
        if($scope.inputImageCover)
          $scope.users.p = {
            _id: Auth.user._id,
            cover64: $scope.outputImageCover
          }
        if($scope.inputImagePhoto)
          $scope.users.p = {
            _id: Auth.user._id,
            photo64: $scope.outputImagePhoto
          }
        $scope.users.putSelf($scope.users.p,function(success){
          if(success)
            Auth.getUser();
            if($scope.inputImageCover)
              console.log($scope.outputImageCover)
            if($scope.inputImagePhoto)
              console.log($scope.outputImagePhoto)

            $('#Preview .close').click();
        })
      }
    }

    $('#Preview .close').on('click',function(){
      $scope.send.form = {};

      $scope.outputImageCover = null;
      $scope.inputImageCover = null;

      $scope.outputImagePhoto = null;
      $scope.inputImagePhoto = null;
    })
  }
	$scope.follows = angular.copy(Follows);
	$scope.follows.s = {
		fromMe: true,
		follows: true
	}
	$scope.follows.getSelf(function(success){})
}

function ProfilePublicationController(GeoPosition,toastr,$rootScope,$state,$scope,$http,$location,$stateParams,$cookies,$cookieStore,$document,$window,$timeout,Auth,Publications,Users,Category,Views) {
	$scope.publications = angular.copy(Publications);

	$scope.items = [];
	$scope.total = 1000;
	$scope.currentPage = $stateParams.page ? $stateParams.page: 1;
	$scope.limit = 10;

	$scope.setPage = function (pageNo) {
    $scope.currentPage = pageNo;
		console.log(pageNo)
  };

	$scope.pageChanged = function() {
		$state.go('.',{page: $scope.currentPage})
		console.log('Page changed to: ' + $scope.currentPage);
	};

	$scope.orderColum = {
    list: function(){},
    grid: function(){}
  }
	if(!$scope.currentPage>1){
		$scope.publications.s = {
			start: 0,
			limit: $scope.limit
		}
	}else {
		$scope.publications.s = {
			start: $scope.limit*($scope.currentPage-1),
			limit: $scope.limit
		}
	}

  if($rootScope.id){
		$scope.publications.s._id = $stateParams.id;
	}else {
		$scope.publications.s.byMe = true;
	}

  $scope.publications.get(function(success){
		if(success){
			$scope.items = $scope.publications.items;
			$scope.total = $scope.publications.total;
		}
    // if(success)
    //   $scope.publications.s = {};
    // else
    //   $scope.publications.s = {};
  })
  $scope.send = {
    form: {},
    search: function(){
			$scope.publications.s.name = this.form.search;
			$scope.publications.get(function(success){
				if(success){
					$scope.items = $scope.publications.items;
					$scope.total = $scope.publications.total;
				}
			})
			console.log(this.form)
		}
  }
}

function ProfileInfoController(GeoPosition,toastr,$rootScope,$state,$scope,$http,$location,$stateParams,$cookies,$cookieStore,$document,$window,$timeout,Auth,Publications,Users,Category,Views) {
  $scope.users = angular.copy(Users)

  if(!$rootScope.id){
    $scope.actionOn = true;
    $scope.action = {
      form: {},
      edit: function(cb){
        $scope.users.putSelf(this.form,function(success){
          if(success){
            if(cb) cb(true);
            Auth.getUser(loadMe);
          }
        })
      }
    }
    if(Auth.user){
      loadMe();
    }
    else {
      $rootScope.$on('gotUser',function(){
        loadMe();
      })
    }
  }else {
    $scope.actionOn = false;
    $scope.users.getSingle($stateParams.id,function(success){
      loadMe();
    })
  }

  function loadMe(){
    if(!$rootScope.id){
      $scope.average = (Auth.user.rating.seller.total/Auth.user.rating.seller.totalTallied);
      if(Number.isNaN($scope.average))
        $scope.average = 0;

      if(Auth.user.website){
        $scope.action.form.website = angular.copy(Auth.user.website);
      }else {
        $scope.action.form.website = 'No posee sitio web';
      }
      $scope.action.form.phone        = angular.copy(Auth.user.phone);
      $scope.action.form.mail         = angular.copy(Auth.user.mail);
      $scope.action.form.address      = angular.copy(Auth.user.address);
      $scope.action.form.description  = angular.copy(Auth.user.description);
      $scope.action.form._id          = angular.copy(Auth.user._id);
      $scope.action.form.nomb         = angular.copy(Auth.user.name)+' '+angular.copy(Auth.user.lastname);
    }else {
      $scope.action = {
        form: {}
      };
      $scope.average = ($scope.users.item.rating.seller.total/$scope.users.item.rating.seller.totalTallied);
      if(Number.isNaN($scope.average))
        $scope.average = 0;

      if($scope.users.item.website){
        $scope.action.form.website = angular.copy($scope.users.item.website);
      }else {
        $scope.action.form.website = 'No posee sitio web';
      }
      $scope.action.form.phone        = angular.copy($scope.users.item.phone);
      $scope.action.form.mail         = angular.copy($scope.users.item.mail);
      $scope.action.form.address      = angular.copy($scope.users.item.address);
      $scope.action.form.description  = angular.copy($scope.users.item.description);
      $scope.action.form._id          = angular.copy($scope.users.item._id);
      $scope.action.form.nomb         = angular.copy($scope.users.item.name)+' '+angular.copy($scope.users.item.lastname);
    }
  }
}

function ProfileFriendController(GeoPosition,toastr,$rootScope,$state,$scope,$http,$location,$stateParams,$cookies,$cookieStore,$document,$window,$timeout,Auth,Publications,Users,Category,Views,Friendships) {
  $scope.friendships = angular.copy(Friendships);

	$scope.items = [];
	$scope.total = 1000;
	$scope.currentPage = $stateParams.page ? $stateParams.page: 1;
	$scope.limit = 10;

	$scope.setPage = function (pageNo) {
    $scope.currentPage = pageNo;
		console.log(pageNo)
  };

	$scope.pageChanged = function() {
		$state.go('.',{page: $scope.currentPage})
		console.log('Page changed to: ' + $scope.currentPage);
	};

	if(!$scope.currentPage>1){
		$scope.friendships.s = {
			start: 0,
			limit: $scope.limit
		}
	}else {
		$scope.friendships.s = {
			start: $scope.limit*($scope.currentPage-1),
			limit: $scope.limit
		}
	}

  $scope.orderColum = {
    list: function(){},
    grid: function(){}
  }

  if($scope.$parent.self){
		$scope.friendships.s.toMe = true;
		$scope.friendships.getSelf(function(success){
			if(success){
				$scope.items = $scope.friendships.items;
				$scope.total = $scope.friendships.total;
			}
		})
	} else{
		$scope.friendships.s._id = $rootScope.id;
		$scope.friendships.get(function(success){
			if(success){
				$scope.items = $scope.friendships.items;
				$scope.total = $scope.friendships.total;
			}
		})
	}
  $scope.send = {
    form: {},
    search: function(){
			$scope.friendships.s.name = this.form.search;
			if($scope.$parent.self){
				$scope.friendships.s.toMe = true;
				$scope.friendships.getSelf(function(success){
					if(success){
						$scope.items = $scope.friendships.items;
						$scope.total = $scope.friendships.total;
					}
				})
			} else{
				$scope.friendships.s._id = $rootScope.id;
				$scope.friendships.get(function(success){
					if(success){
						$scope.items = $scope.friendships.items;
						$scope.total = $scope.friendships.total;
					}
				})
			}
			console.log(this.form)
		},
  }
}

function ProfileFollowingController(GeoPosition,toastr,$rootScope,$state,$scope,$http,$location,$stateParams,$cookies,$cookieStore,$document,$window,$timeout,Auth,Publications,Users,Category,Views,Follows) {
  $scope.follows = angular.copy(Follows);

	$scope.items = [];
	$scope.total = 1000;
	$scope.currentPage = $stateParams.page ? $stateParams.page: 1;
	$scope.limit = 12;

	$scope.setPage = function (pageNo) {
		$scope.currentPage = pageNo;
		console.log(pageNo)
	};

	$scope.pageChanged = function() {
		$state.go('.',{page: $scope.currentPage})
		console.log('Page changed to: ' + $scope.currentPage);
	};

	if(!$scope.currentPage>1){
		$scope.follows.s = {
			start: 0,
			limit: $scope.limit,
			fromMe: true,
			follows: true
		}
	}else {
		$scope.follows.s = {
			start: $scope.limit*($scope.currentPage-1),
			limit: $scope.limit,
			fromMe: true,
			follows: true
		}
	}

  $scope.orderColum = {
    list: function(){},
    grid: function(){}
  }

	if($scope.$parent.self)
		$scope.follows.getSelf(function(success){
			if(success){
				$scope.items = $scope.follows.items;
				$scope.total = $scope.follows.total;
			}
		})
	else{
		$scope.follows.s._id = $scope.$parent.id;
		$scope.follows.get(function(success){
			if(success){
				$scope.items = $scope.follows.items;
				$scope.total = $scope.follows.total;
			}
		})
	}

  $scope.send = {
    form: {},
    search: function(){
			$scope.follows.s.search = this.form.search;
			if($scope.$parent.self)
				$scope.follows.getSelf(function(success){
					if(success){
						$scope.items = $scope.follows.items;
						$scope.total = $scope.follows.total;
					}
				})
			else{
				$scope.follows.s._id = $scope.$parent.id;
				$scope.follows.get(function(success){
					if(success){
						$scope.items = $scope.follows.items;
						$scope.total = $scope.follows.total;
					}
				})
			}
			console.log(this.form)
		},
  }
}

function ReputationController(Auth, $scope, $rootScope, $state){
	if(!Auth.isLogged())
		return $state.go('base.index');
	$rootScope.title = $state.current.title;
  if(Auth.user){
    loadMe();
  }
  else {
    $rootScope.$on('gotUser',function(){
      loadMe();
    })
  }
  $scope.Auth = Auth;

  function loadMe(){

		$scope.average = (Auth.user.rating.seller.total/Auth.user.rating.seller.totalTallied);
      if(isNaN($scope.average))
        $scope.average = 0;
  }


}

function ProfileFollowerController(GeoPosition,toastr,$rootScope,$state,$scope,$http,$location,$stateParams,$cookies,$cookieStore,$document,$window,$timeout,Auth,Publications,Users,Category,Views,Follows) {
	$scope.follows = angular.copy(Follows);

	$scope.items = [];
	$scope.total = 1000;
	$scope.currentPage = $stateParams.page ? $stateParams.page: 1;
	$scope.limit = 12;

	$scope.setPage = function (pageNo) {
		$scope.currentPage = pageNo;
		console.log(pageNo)
	};

	$scope.pageChanged = function() {
		$state.go('.',{page: $scope.currentPage})
		console.log('Page changed to: ' + $scope.currentPage);
	};

	if(!$scope.currentPage>1){
		$scope.follows.s = {
			start: 0,
			limit: $scope.limit,
			toMe: true,
			followers: true
		}
	}else {
		$scope.follows.s = {
			start: $scope.limit*($scope.currentPage-1),
			limit: $scope.limit,
			toMe: true,
			followers: true
		}
	}

	$scope.orderColum = {
		list: function(){},
		grid: function(){}
	}

	if($scope.$parent.self)
		$scope.follows.getSelf(function(success){
			if(success){
				$scope.items = $scope.follows.items;
				$scope.total = $scope.follows.total;
			}
		})
	else{
		$scope.follows.s._id = $scope.$parent.id;
		$scope.follows.get(function(success){
			if(success){
				$scope.items = $scope.follows.items;
				$scope.total = $scope.follows.total;
			}
		})
	}
	$scope.send = {
		form: {},
		search: function(){
			$scope.follows.s.name = this.form.search;
			if($scope.$parent.self)
				$scope.follows.getSelf(function(success){
					if(success){
						$scope.items = $scope.follows.items;
						$scope.total = $scope.follows.total;
					}
				})
			else{
				$scope.follows.s._id = $scope.$parent.id;
				$scope.follows.get(function(success){
					if(success){
						$scope.items = $scope.follows.items;
						$scope.total = $scope.follows.total;
					}
				})
			}
			console.log(this.form)
		},
	}
}

function PublishController(GeoPosition,toastr,$rootScope,$state,$scope,$http,$location,$stateParams,$cookies,$cookieStore,$document,$window,$timeout,Auth){
	$rootScope.title = $state.current.title;
}

function PublishBaseController(GeoPosition,toastr,$rootScope,$state,$scope,$http,$location,$stateParams,$cookies,$cookieStore,$document,$window,$timeout,Auth,Category){
	$rootScope.title = $state.current.title;

	$rootScope.activeOne = true;
	$rootScope.activeTwo = false;
	$rootScope.activeThird = false;

	$scope.category = angular.copy(Category);
	$scope.category.s = {
		parent: "false"
	}
	$scope.category.get();

	$rootScope.disabledOne   = false;
	$rootScope.disabledTwo   = true;
	$rootScope.disabledThird = true;

	// $rootScope.redirection = function(){
	// 	$state.go('publish.base');
	// }
}

function PublishOneController(GeoPosition,toastr,$rootScope,$state,$scope,$http,$location,$stateParams,$cookies,$cookieStore,$document,$window,$timeout,Auth,Category){
	$rootScope.title = $state.current.title;
	$scope.categories = [];
	$scope.data = [];

	$rootScope.activeOne = true;
	$rootScope.activeTwo = false;
	$rootScope.activeThird = false;

	$scope.category = angular.copy(Category);
	$scope.category.s._id = $stateParams.selector[0];
	$scope.category.getSingle(function(success){
		if(success)
			if($stateParams.selector.length == 1){
				$scope.data.push($scope.category.item._id)
				$scope.category.s = {
					parent: $stateParams.selector[0]
				}
				$scope.category.get(function(success){
					if(success){
						if($scope.category.items.length){
							$scope.categories.push($scope.category.items)
						}
						else{
							$scope.next = true;
						}
					}
				});
			}
	});

	$scope.active = [];
	$scope.next = false;

	delete $rootScope.data;

	$scope.GetCategory = function(Id,index){
		$scope.active[index] = Id;
		$scope.data[index+1] = Id;


		$scope.category.s = {
			parent: Id
		}
		$scope.category.get(function(success){
			if(success)
				if($scope.category.items.length){
					$scope.categories[index+1] = $scope.category.items;
					$scope.next = false;
				}else{
					$scope.categories.splice(index+1,1);
					$scope.data.splice(index+2,1);
					$scope.next = true;
				}
		});
	}

	function GetCategory(Id,count){
		$scope.active[count] = Id;
		$scope.category.s = {
			parent: Id
		}
		$scope.category.get(function(success){
			if(success)
				if($scope.category.items.length){
					$scope.categories[count] = $scope.category.items;
					$scope.next = false;
				}else{
					$scope.next = true;
				}
				$scope.data[count] = Id;
		});
	}

	if($stateParams.selector.length > 1){
		for(i=0;i<$stateParams.selector.length;i++){
			GetCategory($stateParams.selector[i],i)
		}
	}

	$rootScope.disabledOne   = false;
	$rootScope.disabledTwo   = true;
	$rootScope.disabledThird = true;

	$rootScope.redirection = function(){
		$state.go('publish.base');
	}
}

function PublishTwoController(GeoPosition,toastr,$rootScope,$state,$scope,$http,$location,$stateParams,$cookies,$cookieStore,$document,$window,$timeout,Auth,Category,textAngularManager,Upload){
	$rootScope.title = $state.current.title;

	$rootScope.activeOne = false;
	$rootScope.activeTwo = true;
	$rootScope.activeThird = false;
	$scope.flag = false;

	$scope.category = angular.copy(Category);

	$scope.selector = $stateParams.selector;

	if(!$stateParams.selector)
		$state.go('publish.base');

	$scope.photos = [];
	$scope.inputPhotos = [];
	$scope.outPhotos = [];
	$scope.i = 0;

	$scope.categories = [];
	$scope.disabled = [];

	function GetCategories(i){
		$scope.category.s._id = $scope.selector[i];
		$scope.category.getSingle(function(success){
			if(success)
				$scope.categories[i] = $scope.category.item;
		})
	}
	for(i=0;i<$scope.selector.length;i++)
		GetCategories(i);

	// Upload.dataUrl(this.photos[i],false)
	// .then(function(urls){
	// 	photos[i] = urls;
	// });

	$scope.PhotoEdit = function(index){
		$scope.i = index;
		$('#Preview').modal('show');
		Upload.dataUrl($scope.photos[index],false)
		.then(function(urls){
			if(urls)
				$scope.inputPhotos[index] = urls;
				// $scope.disabled[index] = true;
		});
	}
	$scope.disabled = function(index){
		$scope.disabled[index] = false;
		$scope.photos[index] = false;
		$scope.outPhotos[index] = false;
		console.log('asdasd')
	}

	$scope.on = {
		form: {},
		submit: function(){
			this.form.files64 = [];
			for (var i = 0; i < $scope.outPhotos.length; i++) {
				this.form.files64.push($scope.outPhotos[i]);
			}
			$rootScope.data = this.form;
			console.log($rootScope.data)
			console.log($scope.selector)
			$state.go('publish.third',{selector: $scope.selector})
		}
	}

	if($rootScope.data){
		$scope.$watch('outPhotos[0]',function(){
			$scope.photos 			= angular.copy($rootScope.data.files64);
			$scope.inputPhotos 	= angular.copy($rootScope.data.files64);
			$scope.outPhotos 		= angular.copy($rootScope.data.files64);
			$scope.on.form 			= angular.copy($rootScope.data);
		})
	}

	$rootScope.disabledOne   = false;
	$rootScope.disabledTwo   = false;
	$rootScope.disabledThird = true;
	$rootScope.redirection = function(id){
		console.log(id)
		if(id==undefined){
			$state.go('publish.one',{selector: $scope.selector});
		}else{
			// $state.go('publish.two',{selector: $scope.selector});
		}
	}
}

function PublishThirdController(GeoPosition,toastr,$rootScope,$state,$scope,$http,$location,$stateParams,$cookies,$cookieStore,$document,$window,$timeout,Auth,Publications,SweetAlert){
	$rootScope.title = $state.current.title;

	$rootScope.activeOne = false;
	$rootScope.activeTwo = false;
	$rootScope.activeThird = true;

	console.log($rootScope.data)
	console.log($stateParams.selector)

	$scope.publication = angular.copy(Publications);

	if(!$stateParams.selector){
		$state.go('publish.base');
		console.log('!params');
	}


	$scope.on = {
		form: {},
		submit: function(){
			this.form.categories = $stateParams.selector;
			$scope.publication.a = this.form;
			let startdate = this.form.startdate;
			$scope.publication.post(function(success,id){
				if(success)
					SweetAlert.swal({
						title: 'Exito',
						text: 'Tu publicacion estara disponible en nuestros listados el'+startdate,
						imageUrl: 'http://50.21.179.35:8083/bill-ok.png',
						imageWidth: 150,
						imageHeight: 150,
						animation: false,
						confirmButtonText:"Continuar",
						showCancelButton: true,
					},function(success){
						if(success){
							$state.go('base.publication',{id: id});
						}
					});
					delete $rootScope.data;
			})
		}
	}

	$scope.on.form = $rootScope.data;

	$rootScope.disabledOne   = false;
	$rootScope.disabledTwo   = false;
	$rootScope.disabledThird = false;
	$rootScope.redirection = function(id){
		console.log(id)
		if(id==undefined){
			$state.go('publish.one',{selector: $scope.on.form.categories});
		}else if(id==1){
			console.log($rootScope.data)
			$state.go('publish.two',{selector: $stateParams.selector});
		}
	}
	if(!$rootScope.data){
		$rootScope.redirection(1);
		console.log('!data');
	}
}

function checkoutController(GeoPosition,toastr,$rootScope,$state,$scope,$http,$location,$stateParams,$cookies,$cookieStore,$document,$window,$timeout,Auth,Publications,BanksAccount){
	$rootScope.title = $state.current.title;

	$scope.banksaccount = angular.copy(BanksAccount);

	$scope.publish 	= $stateParams.publish;
	$scope.type 		= $stateParams.type;
	$scope.quantity = $stateParams.quantity;
	$scope.id   		= $stateParams.id;

	if($scope.type)
		$scope.banksaccount.s.me = true;
		$scope.banksaccount.get(function(success){})

	$scope.publications = angular.copy(Publications);
	$scope.publications.getSingle($scope.publish,function(success){});
}

function bankcheckout(GeoPosition,toastr,$rootScope,$state,$scope,$http,$location,$stateParams,$cookies,$cookieStore,$document,$window,$timeout,Auth,BanksAccount){
	$rootScope.title = $state.current.title;
}

function bankcheckoutOne(GeoPosition,toastr,$rootScope,$state,$scope,$http,$location,$stateParams,$cookies,$cookieStore,$document,$window,$timeout,Auth,Transactions,BanksAccount){
	$rootScope.title = $state.current.title;

	$scope.transactions = angular.copy(Transactions);
	$scope.banksaccount = angular.copy(BanksAccount);

	$rootScope.disabledOne = false;
	$rootScope.disabledTwo = true;
	$rootScope.disabledThird = true;

	$rootScope.activeOne = true;
	$rootScope.activeTwo = false;
	$rootScope.activeThird = false;

	$scope.type = $stateParams.type;

	$scope.on = {
		form: {},
		submit: function(){
			$scope.transactions.p = this.form;
			$scope.transactions.p._id = $stateParams.id;
			$scope.transactions.put(0,function(success){
				if(success){
					$rootScope.disabledBtn = true;
					$rootScope.dataOne = this.form;
					$rootScope.redirection(1);
				}
			})
		}
	}

	if($rootScope.dataOne)
		$scope.on.form = $rootScope.dataOne;
	if($scope.type)
		$scope.banksaccount.s.me = true;
		$scope.banksaccount.get(function(success){})

	$scope.transactions.s._id = $stateParams.id;
	$scope.transactions.getSingle(function(success){
		if(success){
			$scope.on.form.sum = $scope.transactions.item.price;
		}
	})

	$rootScope.redirection = function(i) {
		switch(i){
			case undefined:
				break;
			case 1:
				$state.go('bankcheckout.bankcheckoutTwo',{id: $stateParams.id});
				break;
			case 2:
				$state.go('bankcheckout.bankcheckoutThird',{id: $stateParams.id});
				break;
		}
	}

}

function bankcheckoutTwo(GeoPosition,toastr,$rootScope,$state,$scope,$http,$location,$stateParams,$cookies,$cookieStore,$document,$window,$timeout,Auth,Transactions){
	$rootScope.title = $state.current.title;

	$scope.transactions = angular.copy(Transactions);

	$scope.id = $stateParams.id;

	$rootScope.disabledOne = false;
	$rootScope.disabledTwo = false;
	$rootScope.disabledThird = true;

	$rootScope.activeOne = false;
	$rootScope.activeTwo = true;
	$rootScope.activeThird = false;

	$scope.disabled = true;

	$scope.on = {
		form: {},
		submit: function(){
			console.log(this.form);
			$scope.transactions.p = this.form;
			$scope.transactions.p._id = $stateParams.id;

			$scope.transactions.put(1,function(success){
				if(success){
					$rootScope.redirection(2);
				}
			})
		}
	}

	$scope.transactions.p.idp = 'v';

	$scope.change = function(){
		$scope.disabled = false;
	}

	$scope.transactions.s._id = $stateParams.id;
	$scope.transactions.getSingle(function(success){
		if(success){
			$scope.on.form.name = $scope.transactions.item.buyer.name+' '+$scope.transactions.item.buyer.lastname;
			$scope.on.form.id = $scope.transactions.item.buyer.document;
			$scope.on.form.idp = 'v';
			if($scope.transactions.item.buyer.localities.length>1)
				$scope.on.form.address = $scope.transactions.item.buyer.localities.toString();
		}
	})

	$rootScope.redirection = function(i) {
		switch(i){
			case undefined:
				$state.go('bankcheckout.bankcheckoutOne',{id: $stateParams.id});
				break;
			case 1:
				$state.go('bankcheckout.bankcheckoutTwo',{id: $stateParams.id});
				break;
			case 2:
				$state.go('bankcheckout.bankcheckoutThird',{id: $stateParams.id});
				break;
		}
	}

}

function bankcheckoutThird(GeoPosition,toastr,$rootScope,$state,$scope,$http,$location,$stateParams,$cookies,$cookieStore,$document,$window,$timeout,Auth,Transactions,SweetAlert){
	$rootScope.title = $state.current.title;

	$scope.transactions = angular.copy(Transactions);

	$rootScope.disabledOne = false;
	$rootScope.disabledTwo = false;
	$rootScope.disabledThird = false;

	$rootScope.activeOne = false;
	$rootScope.activeTwo = false;
	$rootScope.activeThird = true;

	$scope.on = {
		form: {},
		submit: function(){
			console.log(this.form);
			$scope.transactions.p = this.form;
			$scope.transactions.p._id = $stateParams.id;

			$scope.transactions.put(2,function(success){
				if(success){
					SweetAlert.swal({
					  title: '',
					  text: 'Tu compra ha sido concretada con exito',
					  imageUrl: 'http://50.21.179.35:8083/bill-ok.png',
					  imageWidth: 150,
					  imageHeight: 150,
					  animation: false,
						confirmButtonText:"Continuar",
						showCancelButton: true,
					},function(success){
						if(success){
							$state.go('base.index');
						}
					});
				}
			})
		}
	}

	$scope.transactions.s._id = $stateParams.id;
	$scope.transactions.getSingle(function(success){
		if(success){
			$scope.on.form.name = $scope.transactions.item.buyer.name+' '+$scope.transactions.item.buyer.lastname;
			$scope.on.form.id = $scope.transactions.item.buyer.document;
			$scope.on.form.idp = 'v';
			if($scope.transactions.item.buyer.localities.length>1)
				$scope.on.form.address = $scope.transactions.item.buyer.localities.toString();
		}
	})

	$rootScope.redirection = function(i) {
		switch(i){
			case undefined:
				$state.go('bankcheckout.bankcheckoutOne',{id: $stateParams.id});
				break;
			case 1:
				$state.go('bankcheckout.bankcheckoutTwo',{id: $stateParams.id});
				break;
			case 2:
				$state.go('bankcheckout.bankcheckoutThird',{id: $stateParams.id});
				break;
		}
	}
}

function PublicationLikes(GeoPosition,toastr,$rootScope,$state,$scope,$http,$location,$stateParams,$cookies,$cookieStore,$document,$window,$timeout,Auth,SweetAlert,Likes){
	$rootScope.title = $state.current.title;

	$scope.likes = angular.copy(Likes);

	$scope.items = [];
	$scope.total = 1000;
	$scope.currentPage = $stateParams.page ? $stateParams.page: 1;
	$scope.limit = 12;

	$scope.setPage = function (pageNo) {
		$scope.currentPage = pageNo;
		console.log(pageNo)
	};

	$scope.pageChanged = function() {
		$state.go('.',{page: $scope.currentPage})
		console.log('Page changed to: ' + $scope.currentPage);
	};

	if(!$scope.currentPage>1){
		$scope.likes.s = {
			start: 0,
			limit: $scope.limit,
			byMe : true
		}
	}else {
		$scope.likes.s = {
			start: $scope.limit*($scope.currentPage-1),
			limit: $scope.limit,
			byMe : true
		}
	}

	$scope.orderColum = {
    list: function(){},
    grid: function(){}
  }
  $scope.likes.get(function(success){
		if(success){
			$scope.items = $scope.likes.items;
			$scope.total = $scope.likes.total;
		}
	})
  $scope.send = {
    form: {},
    search: function(){
			$scope.likes.s.search = this.form.search;
			$scope.likes.get(function(success){
				if(success){
					$scope.items = $scope.likes.items;
					$scope.total = $scope.likes.total;
				}
			})
		}
  }
}
