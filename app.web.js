var express = require('express'),
	compression = require('compression'),
	minify = require('express-minify'),
	path = require('path'),
	logger = require('morgan'),
	cookieParser = require('cookie-parser'),
	useragent = require('express-useragent'),
	ObjectID = require('mongoskin').ObjectID,
	bodyParser = require('body-parser'),
	User = require('./routes/objects/user'),
	Notification = require('./routes/objects/notification');
	
	
	Notification = new Notification();
	User = new User();
	
var app = express();
app.use(compression());
app.use(useragent.express());
app.use(minify({
  cache: __dirname+'/minCache',
  uglifyJS: undefined,
  cssmin: undefined,
  onerror: undefined,
  js_match: /javascript/,
  css_match: /css/,
  sass_match: /scss/,
  less_match: /less/,
  stylus_match: /stylus/,
  json_match: /json/,
}));

// view engine setup
app.set('views', path.join(__dirname, 'views/client'));
app.set('view engine', 'pug');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

app.use(function(req,res,next){
    res._uglifyMangle = false;
    next();
});

app.use(express.static(path.join(__dirname, 'public')));

var client_index = require('./routes/web/index'),
	bot_index = require('./routes/web/bot');

var openConnections = [];

function expell(token, tokenSecret, res){
	if(res)
		res.json({e:0});
    for (let j = 0 ; j < openConnections.length ; j++) {
        for(let o = 0 ; o < openConnections[j].sets.length ; o++){
            if (openConnections[j].sets[o].token == token &&
                openConnections[j].sets[o].tokenSecret == tokenSecret) {
                for(let k = 0 ; k < openConnections[j].sets[o].res.length ; k++){
                    openConnections[j].sets[o].res[k].write('id: ' + Date.now() + '\n');
                    openConnections[j].sets[o].res[k].write('data:' + JSON.stringify({logout:true}) +   '\n\n');
                    openConnections[j].sets[o].res[k].flush();
                }
                openConnections[j].sets.splice(o,1);
                if(openConnections[j].sets.length === 0)
                    openConnections.splice(j,1);
                return;
            }
        }
    }
}

function sendMessage(sets,mes){
    for(let k = 0 ; k < sets.length ; k++)
        for(let i = 0 ; i < sets[k].res.length ; i++){
            sets[k].res[i].write('id: ' + mes.id + '\n');
            sets[k].res[i].write('data:' + JSON.stringify(mes) +   '\n\n');
            sets[k].res[i].flush();
        }
}

function prepareMessages(resp){
        var user = resp.user,
            sets = resp.sets;   
        Notification.find({user:user,status:0},function(success,arr){
            if(success){
              for(let i = 0 ; i < arr.length ; i++){
                    var mes = {
                        id  : arr[i]._id,
                        mes : arr[i].mes,
						status : 1,
						time: arr[i].time,
                        url : arr[i].url
                    };
                    sendMessage(sets,mes);
                }
                Notification.update({user:user,status:0},{"$set":{status:1}},{multi: true});
            }else{
				console.log(arr);
			}
        });
        
    }

setInterval(function() {
    for(var j = 0 ; j < openConnections.length ; j++){
        prepareMessages(openConnections[j]);
    }
}, 1000);	
	
setInterval(function() {
	Notification.update({time:{"$lt":(Date.now()-600000)},status:0},{"$set":{status:1}},{multi: true});
}, 600000);


app.use(function(req, res, next){
	if(req.useragent.isBot){
		req.fullurl = req.protocol + '://' + req.get('host') + req.originalUrl;
		req.url = "/bot"+req.url;
		next("route");
	}else
		next();
});

app.use('/bot', bot_index);
app.use('/events/:token/:tokenSecret',function(req,res){
    var token = req.params.token,
        tokenSecret = req.params.tokenSecret;
    
    User.checkHash(token, tokenSecret, function(user){
        if(user){
          
            var userOID = new ObjectID(user._id);
            req.socket.setTimeout(Number.MAX_VALUE);
            res.writeHead(200,
              {"Content-Type":"text/event-stream",
              "Cache-Control":"no-cache",
              "Connection":"keep-alive"});
            res.write("\n");
            res.flush();
            var notFound = true;
            for (let j = 0 ; j < openConnections.length ; j++) {
                if (openConnections[j].user.equals(userOID)) {
                    for(let o = 0 ; o < openConnections[j].sets.length ; o++){
                        if(openConnections[j].sets[o].token == token &&
                           openConnections[j].sets[o].tokenSecret == tokenSecret){
                            openConnections[j].sets[o].res.push(res);
                            notFound = false;
                            break;
                        }
                    }
                    break;
                }
            }
            if(notFound){
                openConnections.push(
                    {
                        user:   userOID,
                        sets:[
                            {
                                token: token,
                                tokenSecret: tokenSecret,
                                res: [res]
                            }
                        ]
                        
                    }                         
                );
            }
            
            req.on("close", function() {
                for (let j = 0 ; j < openConnections.length ; j++) {
                    if (openConnections[j].user.equals(userOID)) {
                        for(let p = 0 ; p < openConnections[j].sets.length ; p++){
                            if(openConnections[j].sets[p].token == token && openConnections[j].sets[p].tokenSecret == tokenSecret){
                                for(let k = 0 ; k < openConnections[j].sets[p].res.length ; k++){
                                    if(openConnections[j].sets[p].res[k] == res){
                                        openConnections[j].sets[p].res.splice(k,1);
                                        if(openConnections[j].sets[p].res.length === 0){
                                            openConnections[j].sets.splice(p,1);
                                            if(openConnections[j].sets.length === 0){
                                                openConnections.splice(j,1);
                                            }
                                        }
                                        break;
                                    }
                                }
                                break;
                            }
                        }
                        break;
                    }
                }
            });
        }else{
            res.json({e:1,mes:"Error"});
        }
    });
});
app.use('/logout/:token/:tokensecret',function(req,res){
	var token = req.params.token,
		tokenSecret = req.params.tokensecret;
	User.update({"bitacora.token": token, "bitacora.tokenSecret": tokenSecret},
		{"$set":{"bitacora.$.active":false}},
		{},
		function(){
			//res.json({e:0});
			expell(token, tokenSecret, res);
		});
});
app.use('/', client_index);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  console.log(err);
  // render the error page
  res.status(err.status || 500);
  res.json(err);
});

module.exports = app;
